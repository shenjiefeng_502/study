package com.study.LeetCode.mid;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/26  10:07
 * @description:
 */
public class T22 {
    public static void main(String[] args) {
        List<String> strings = generateParenthesis(2);
    }
    public static List<String> generateParenthesis(int n) {
        List<String> res = new ArrayList<String>();
        generate(res, "", 0, 0, n);

        return res;
    }

    /**
     * (
     * ((
     * (()
     * @param res
     * @param ans
     * @param count1
     * @param count2
     * @param n
     */
    //count1统计“(”的个数，count2统计“)”的个数
    public static void generate(List<String> res, String ans, int count1, int count2, int n){

        if(count1 > n || count2 > n) return;

        if(count1 == n && count2 == n)  res.add(ans);


        if(count1 >= count2){
            //todo
            String ans1 = new String(ans);
            generate(res, ans+"(", count1+1, count2, n);
            //左括号到达上限时指向右括号
            generate(res, ans1+")", count1, count2+1, n);
        }
    }
}
