package com.study.LeetCode.mid;

import java.util.ArrayList;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/25  15:49
 * @description: Z字符变换
 */
public class T6 {
    /**
     * 输入：s = "PAYPALISHIRING", numRows = 4
     * 输出："PINALSIGYAHRPI"
     * 解释：
     * P     I    N
     * A   L S  I G
     * Y A   H R
     * P     I
     * <p>
     * 生成行数个数的数组,遍历填入不同行,根据index区分下一个是填入向上行还是向下行
     *
     * @param s
     * @param numRows
     * @return
     */
    public String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        StringBuilder[] ress = new StringBuilder[numRows];
        for (int i = 0; i < Math.min(s.length(), numRows); i++) {
            ress[i] = (new StringBuilder());
        }
        boolean isGoingDown = false;
        char[] chars = s.toCharArray();
        int curRow = 0;

        for (int i = 0; i < chars.length; i++) {
            /**
             * 第几行为老行数
             * 向上or向下一行
             * isGoingDown=i/nowRows%2==0
             */
            ress[curRow].append(chars[i]);
            if (curRow == 0 || curRow == numRows - 1) isGoingDown = !isGoingDown;
            curRow += isGoingDown ? 1 : -1;
        }
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < Math.min(s.length(), numRows); i++) {
            res.append(ress[i]);
        }
        return res.toString();
    }
}
