package com.study.LeetCode.mid;

public class T50 {
    public static void main(String[] args) {
        double v = myPow(2.0, 10);
        System.out.println(v);
    }
    public static double myPow(double x, int n) {
        long N = n;
        return N > 0 ? quickMul(x, N) : 1.0 / quickMul(x, N);
    }

    private static double quickMul(double x, long n) {
        if (n == 0) {
            return 1.0;
        }
        double y = quickMul(x, n / 2);
        return n % 2 == 0 ? y * y : y * y * x;
    }
}
