package com.study.LeetCode.kmp;

public class T28 {
    public static void main(String[] args) {
        int[] abcabds = getNextTest("ABAB");
//        int kmp = KMP("AGAAGAAABAAABD", "AAABD");
        int[] right = getNext("ABAB");
        int[] rightPro = getNextPro("ABAB");
        System.out.println(1);
    }

    public int[] getNextT(String s) {
        char[] p = s.toCharArray();
        int k = -1;
        int j = 0;
        int[] next = new int[p.length - 1];
        next[0] = -1;
        while (j < p.length) {
            if (k == -1 || p[j] == p[k]) {
                next[++j] = ++k;
            } else {
                k = next[k];
            }
        }
        return next;
    }

    /**
     * Kmp
     *
     * @param ts the ts
     * @param ps the ps
     * @return int the int
     * @author sjf
     * @date 2022-01-05 16:38:43
     */
    public static int KMP(String ts, String ps) {
        char[] t = ts.toCharArray();
        char[] p = ps.toCharArray();
        // 主串的位置
        int i = 0;
        // 模式串的位置
        int j = 0;
        int[] next = getNext(ps);
        while (i < t.length && j < p.length) {
            // 当j为-1时，要移动的是i，当然j也要归0
            if (j == -1 || t[i] == p[j]) {
                i++;
                j++;
            } else {
                // i不需要回溯了
                // i = i - j + 1;
                // j回到指定位置
                j = next[j];
            }
        }
        if (j == p.length) {
            return i - j;
        } else {
            return -1;
        }
    }


    public static int[] getNextTest(String s) {
        char[] p = s.toCharArray();
        int length = s.length();
        int[] next = new int[length];
        next[0] = -1;
        int j = 0;
        int k = -1;
        while (j < p.length - 1) {
            if (k == -1 || p[j] == p[k]) {
                if (p[++j] == p[++k]) {
                    next[j]=next[k];
                } else {
                    next[j] = k;
                }
            } else {
                k = next[k];
            }
        }
        return next;
    }

    /**
     * j表示数组当前下标
     * k值表示该位置上对应的回退下标
     * j==-1时j不动了,i去后退
     *
     * @param ps
     * @return
     */
    public static int[] getNext(String ps) {
        char[] p = ps.toCharArray();
        int[] next = new int[p.length];
        next[0] = -1;
        int j = 0;
        int k = -1;
        while (j < p.length - 1) {
            //当出现第一个p[j]=k(0)时
            if (k == -1 || p[j] == p[k]) {
                //当前字符串的下一个字符串如果不对应则,从next[k]开始
                //祖上积德让你能够不用从0开始
                //p[j+1]=k+1
                next[++j] = ++k;
            } else {
                //当pk不相等时,
                //k指针指向的还是p数组的第一个位置(因为next数组默认为0就不需要操作next数组了),
                //但是k的值需要更改为-1,下一个字符串如果对应上了'k'也还是从0开始
                k = next[k];
            }
        }
        return next;
    }

    public static int[] getNextPro(String ps) {
        char[] p = ps.toCharArray();
        int[] next = new int[p.length];
        next[0] = -1;
        int j = 0;
        int k = -1;
        while (j < p.length - 1) {
            if (k == -1 || p[j] == p[k]) {
                // 当两个字符相等时要跳过
                if (p[++j] == p[++k]) {
                    next[j] = next[k];
                } else {
                    next[j] = k;
                }
            } else {
                k = next[k];
            }
        }
        return next;
    }
}
