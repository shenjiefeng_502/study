package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.List;

public class T22 {
    public static void main(String[] args) {
        List<String> strings = generateParenthesis(3);
        System.out.println("23");
    }

    /**
     * 总递归长度为n的两倍(1个前括号,一个后括号)
     * 隐藏条件,括号必须有前括号,如果前面没有足够的前括号,则这个是前括号
     * 如果前括号数量为三个则不再增加前括号
     * 定义两个条件,一个是现有可用前括号数量,可以加减,一个是现有总前括号数量,只能增加
     * @param n
     * @return
     */
    public static List<String> generateParenthesis(int n) {
        ArrayList<String> res = new ArrayList<>();
        StringBuilder temp = new StringBuilder();
        if (n <= 0) {
            return res;
        }
        int length = n * 2;
        dfs(0, 0, n, length, temp, res);
        return res;
    }

    /**
     * Dfs
     *
     * @param leftBracket 可用前括号数量
     * @param leftCount   前括号总数
     * @param n           the n
     * @param length      the length
     * @param temp        the temp
     * @param res         the res
     * @author sjf
     * @date 2021-12-13 17:15:17
     */
    private static void dfs(int leftBracket, int leftCount, int n, int length, StringBuilder temp, ArrayList<String> res) {
        if (temp.length() == length) {
            res.add(new String(temp));
            return;
        }
        char s = '(';
        for (int i = 0; i < 2; i++) {
            //如果前面没有前括号可用,则必须是前括号
            if (leftBracket == 0 && i == 1) {
                continue;
            }
            //如果前括号数量等于括号数,则不再添加前括号
            if (leftCount == n && i == 0) {
                continue;
            }
            char bra = (char) (s + i);
            temp.append(bra);
            //只判端当括号为前括号时添加一次前括号计数
            int leftBracketNum = leftBracket + (i == 0 ? 1 : -1);
            int count = leftCount + (i == 0 ? 1 : 0);
            dfs(leftBracketNum, count, n, length, temp, res);
            temp.deleteCharAt(temp.length() - 1);
        }

    }

}
