package com.study.LeetCode.bfs_dfs;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/24  15:24
 * @description: 第二短时间
 */
public class T2045 {
    public static void main(String[] args) {
        /**
         * 绿灯的时候通行
         */
        int secondMinimum = secondMinimum(3, new int[][]{{1, 2}, {2, 3}}, 792, 822);
//        int secondMinimum = secondMinimum(12, new int[][]{{1, 2}, {1, 3}, {3, 4}, {1, 5}, {3, 6}, {4, 7}, {7, 8}, {8, 9}, {5, 10}, {8, 11}, {3, 12}}, 792, 822);
        System.out.println(secondMinimum);
//        checkRedGreen(792,792,822);
    }

    public static int secondMinimum(int n, int[][] edges, int time, int change) {
        //将二维数组转为mapkey为起始点,value为结束点
        int nowTime = 0;
        List<Integer> temp = new ArrayList<>();
        temp.add(Integer.MAX_VALUE);
        temp.add(Integer.MAX_VALUE);
        temp.add(Integer.MAX_VALUE);
        boolean secondChance = false;
        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < edges.length; i++) {
            //记录双向道路
            putValue(map, edges[i][0], edges[i][1]);
            if (edges[i][1] == n) {
                putValue(map, edges[i][1], edges[i][0]);
            }
        }
        /**
         * 循环条件 1的每一条路径:例1则是 1-2,1-3,1-4
         */
        List<Integer> list = map.get(1);
        for (Integer target : list) {
            temp = dfs(1, target, secondChance, nowTime, temp, time, change, n, map);
        }
        return temp.get(1);
    }

    /**
     * 每次到一个地点要做的事情
     * 1.添加 time 时间到当前统计时间nowTime中
     * 2.判断是否是目的地,如果是目的地则比较时间
     * 3.判断是否可以继续通行
     * 4.判断红绿灯 (time/change%2!=0)
     * 5.如果是红灯继续添加等待时间到当前统计时间中(nowTime+=(change-nowTime%change))
     * 6.继续循环该节点的其他节点路径
     *
     * @return
     */
    private static List<Integer> dfs(int comeFrom, Integer nowPosition, boolean secondChance, int nowTime, List<Integer> temp, int time, int change, int n, HashMap<Integer, List<Integer>> map) {
        //1.添加时间
        nowTime += time;
        //4.判断红绿灯 (time/change%2==0)
        nowTime = checkRedGreen(nowTime, time, change);
        //2.判断是否到达目的地
        if (nowPosition == n) {
            //校验时间
            //绝对值, 2,2,4值该为4
            //也就是说当前时间等于最小时间时跳过更改第二小值
            if (!temp.contains(nowTime)) {
                temp.add(nowTime);
                temp = temp.stream().sorted().collect(Collectors.toList());
                temp.remove(3);
            }
            //判断是否触发过标识
            //触发过则结束,未触发过则只能原路返回
            if (secondChance) {
                //dead end
                return temp;
                //标识true并且继续dfs
            } else {
                //往回走一次
                temp = dfs(nowPosition, comeFrom, true, nowTime, temp, time, change, n, map);
                return temp;
            }
        }
        //3.判断是否可以继续通行

        List<Integer> integers = map.get(nowPosition);
        if (integers == null) {
            return temp;
        }
        for (Integer newTarget : integers) {
            /**
             * 当目的地等于来时
             * 且目标不等于终点时
             * 无法到达终点的路线或者往回走的路线判断是否回头其实没有意义
             * 2-1无下个节点就直接返回,
             * 4-1直接在5-4判断得了
             * 在当到达时去判断是否回头即可,每个节点之间时间相等所以直接在终点门口回头一次即可
             */
            if (newTarget == comeFrom && newTarget != n) {
                //dead end
                continue;
            }
            temp = dfs(nowPosition, newTarget, secondChance, nowTime, temp, time, change, n, map);
        }
        return temp;
    }

    private static int checkRedGreen(int nowTime, int time, int change) {
        int x = nowTime / change;
        if (x % 2 == 1) {
            nowTime = (x + 1) * change;
        }
        nowTime += time;
        return nowTime;
    }

    /**
     * 每次到一个地点要做的事情
     * 1.添加 time 时间到当前统计时间nowTime中
     * 2.判断是否是目的地,如果是目的地则比较时间
     * 3.判断是否可以继续通行
     * 4.判断红绿灯 (time/change/2==0)
     * 5.如果是红灯继续添加等待时间到当前统计时间中(nowTime+=(change-nowTime%change))
     */
    /**
     * 标识触发条件
     * 当到达目的地且标识未触发时,触发标识回到上一个点
     * 当目的地的可到达节点只有来的节点时,触发标识回到上一个节点
     */
    /**
     * 结束条件
     * 1.路径到头了,且已经触发过一次回头记录
     * 2.路径到头了到达目的地,且已经触发过一次回头记录,比较第一小值和第二小值
     * 3.路径到头了,可以往回开,有一次重复路径的机会,多于一次则打断,可以用一个boolean值来标识
     * 标识触发条件,第一次到达目的地,第一次无其他路径可选
     */
    /**
     * 每次到达目的地都要判断一次最小值和第二小值,不用管是否触发标识
     */
    //todo 1-2-1-2怎么解决

    /**
     * 更改结束循环条件为求出第二小值,依此为依据做剪枝和结束循环(改for为while)
     */


    private static void putValue(HashMap<Integer, List<Integer>> map, int come, int target) {
        List<Integer> list = map.get(come);
        if (list == null) {
            list = new ArrayList<Integer>();
        }
        list.add(target);
        map.put(come, list);
    }
}
