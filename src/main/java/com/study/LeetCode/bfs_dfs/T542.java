package com.study.LeetCode.bfs_dfs;

import java.util.LinkedList;
import java.util.Queue;

/**
 * T 542
 *
 * @author sjf
 * @version 1.0
 * @date 2021-12-24 10:04:59
 */
public class T542 {

    /**
     * Update matrix
     *
     * @param mat the mat
     * @return int[ ] [ ] the int [ ] [ ]
     * @author sjf
     * @date 2021-12-24 10:05:02
     */
//    public int[][] updateMatrix(int[][] mat) {
//        int m = mat.length;
//        int[][] res = new int[m][];
//        boolean[][] isV = new boolean[][];
//        int[][] path = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
//        Queue<int[]> queue = new LinkedList<>();
//        for (int i = 0; i < m; i++) {
//            for (int j = 0; j < mat[i].length; j++) {
//                res[i][j] = dfs(i, j, path, queue, isV, mat);
//            }
//        }
//    }

    /**
     * 图形bfs
     * 第一层是距离0的数据
     * 第二层是距离0上下左右的数据,如果上下左右有-1距离为1(-1是原数据1的数值标记),并将x,y存入队列
     * 第三层是距离1上下左右为-1的数据
     * 第四层是距离2上下左右为-1的数据
     * @param mat
     * @return
     */
    public int[][] updateMatrix(int[][] mat) {
        Queue<int[]> queue = new LinkedList<int[]>();
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                if (mat[i][j] == 1) {
                    mat[i][j] = -1;
                } else {
                    queue.add(new int[]{i, j});
                }
            }
        }
        int[][] path = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        while (!queue.isEmpty()) {
            int[] poll = queue.poll();
            int i1 = poll[0];
            int i2 = poll[1];
            for (int i = 0; i < path.length; i++) {
                int x = i1 + path[i][0];
                int y = i2 + path[i][1];
                if (x >= 0 && y >= 0 && mat.length > x && mat[x].length > y && mat[x][y] == -1) {
                    mat[x][y] = mat[i1][i2] + 1;
                    //todo 注意这里是offer添加在尾部.
                    queue.offer(new int[]{x, y});
                }
            }
        }
        return mat;
    }

    private boolean hasZero(int x, int y, int[][] mat) {
        if (x - 1 >= 0 && mat[x - 1][y] == 0) {
            return true;
        } else if (y - 1 >= 0 && mat[x][y - 1] == 0) {
            return true;
        } else if (y < mat[x].length && mat[x][y + 1] == 0) {
            return true;
        } else if (x < mat.length && mat[x + 1][y] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

//    private int dfs(int i, int j, int[][] path, Queue<int[]> queue, boolean[][] isV, int[][] mat) {
//        if (mat[i][j] == 0) {
//            return 0;
//        }
//        isV[i][j] = true;
//        queue.add(new int[]{i, j});
//        while (!queue.isEmpty()) {
//            for (int i1 = 0; i1 < path.length; i1++) {
//                int x = i + path[i][0];
//                int y = j + path[i][1];
//                if (x >= 0 && y >= 0 && mat.length > x && mat[x].length > y && !isV[x][y]) {
//                    return 1 + dfs(x, y, path, queue, isV, mat);
//                }
//            }
//        }
//
//    }
//}
