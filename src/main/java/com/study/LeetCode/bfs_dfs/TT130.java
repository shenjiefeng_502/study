package com.study.LeetCode.bfs_dfs;

public class TT130 {
    public static void main(String[] args) {
        char[][] chars = {{'O'}};
        solve(chars);
        System.out.println(1);
    }

    /**
     * Solve
     * 把所有不与边界相连的O变为X
     * 将所有O变为C
     * 然后将处于边界或与边界相连的C变为O
     * 将剩余的C变为X
     * 逻辑上来讲深度优先算法比较合适
     *
     * @param board the board
     * @author sjf
     * @date 2022-01-11 09:35:54
     */
    public static void solve(char[][] board) {
        int[][] path = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
        int m = board.length;
        int n = board[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'C';
                }
            }
        }
        for (int i = 0; i < m; i++) {
            dfs(i, 0, path, board);
            dfs(i, n - 1, path, board);
        }
        for (int j = 0; j < n; j++) {
            dfs(0, j, path, board);
            dfs(m - 1, j, path, board);
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'C') {
                    board[i][j] = 'X';
                }
            }
        }
    }

    private static void dfs(int i, int j, int[][] path, char[][] board) {
        if (i >= 0 && i < board.length && j >= 0 && j < board[0].length && board[i][j] == 'C') {
            board[i][j] = 'O';
            for (int i1 = 0; i1 < path.length; i1++) {
                int newX = path[i1][0] + i;
                int newY = path[i1][1] + j;
                dfs(newX, newY, path, board);
            }
        }
    }
}
