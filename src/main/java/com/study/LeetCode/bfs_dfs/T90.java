package com.study.LeetCode.bfs_dfs;

import java.util.*;

public class T90 {
    public static void main(String[] args) {
        List<List<Integer>> lists = subsetsWithDup(new int[]{1, 2, 2});
        System.out.println("123");
    }

    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        ArrayList<List<Integer>> result = new ArrayList<>();
        ArrayList<Integer> temp = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
        for (int k = 0; k <= nums.length; k++) {
            recursion(0, k, nums, temp, result, used);
        }
        return result;
    }

    public static void recursion(Integer startIndex, Integer target, int[] nums, List<Integer> temp, ArrayList<List<Integer>> result, boolean[] used) {
        //树状图层级条件
        if (target == temp.size()) {
            result.add(new ArrayList<>(temp));
            return;
        }
        //深度
        for (int i = startIndex; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1] && !used[i - 1]) {
                continue;
            } else {
                temp.add(nums[i]);
                used[i] = true;
                recursion(i + 1, target, nums, temp, result, used);
                used[i] = false;
                temp.remove(temp.size() - 1);
            }
        }
    }
//
//    static boolean[] used;
//
//    public static List<List<Integer>> subsetsWithDup(int[] nums) {
//        // 存放符合条件结果的集合
//        List<List<Integer>> result = new ArrayList<>();
//        // 用来存放符合条件结果
//        LinkedList<Integer> path = new LinkedList<>();
//        if (nums.length == 0) {
//            result.add(path);
//            return result;
//        }
//        Arrays.sort(nums);
//        used = new boolean[nums.length];
//        //通过for循环 定义k 为子集元素个数
//        //定义子集元素开始位置
//        int start = 0;
//        subsetsWithDupHelper(nums, 0, result, path);
//        return result;
//    }
//
//    private static void subsetsWithDupHelper(int[] nums, int startIndex, List<List<Integer>> result, LinkedList<Integer> path) {
//        result.add(new ArrayList<>(path));
//        if (startIndex >= nums.length) {
//            return;
//        }
//        for (int i = startIndex; i < nums.length; i++) {
//            if ((i > 0 && nums[i] == nums[i - 1] && !used[i - 1])) {
//
//            } else {
//                path.add(nums[i]);
//                used[i] = true;
//                subsetsWithDupHelper(nums, i + 1, result, path);
//                used[i] = false;
//                path.removeLast();
//            }
//        }
//    }
}
