package com.study.LeetCode.bfs_dfs;

public class T695 {
    public static void main(String[] args) {
        int i = maxAreaOfIsland(new int[][]{{0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 1,
                0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0}, {0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}});
        System.out.println(i);
    }

    public static int maxAreaOfIsland(int[][] grid) {
        int maxArea = 0;
        if (grid.length == 0) {
            return maxArea;
        }
        int[][] ints = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[x].length; y++) {
                maxArea = Math.max(maxArea, dfs(x, y, ints, grid));
            }
        }
        return maxArea;
    }

    private static int dfs(int x, int y, int[][] ints, int[][] grid) {
        if (grid[x][y] == 1) {
            grid[x][y] = 2;
            int nowArea = 1;
            for (int i = 0; i < ints.length; i++) {
                int newX = x + ints[i][0];
                int newY = y + ints[i][1];
                if (newX >= 0 && newY >= 0 && newX < grid.length && newY < grid[newX].length) {
                    nowArea += dfs(newX, newY, ints, grid);
                }
            }
            return nowArea;
        }
        return 0;
    }
}
