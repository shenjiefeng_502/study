package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.List;

public class T78 {
    public static void main(String[] args) {
//        List<List<Integer>> subsets = subsets(new int[]{1, 2, 3});
        /**
         * 尝试自己写回溯
         */
        List<List<Integer>> mySubsets = myRecursion(new int[]{1, 2, 3});
        System.out.println("23");
    }

    private static List<List<Integer>> myRecursion(int[] ints) {
        ArrayList<List<Integer>> res = new ArrayList<>();
        //定义临时子集存放处
        ArrayList<Integer> temp = new ArrayList<>();
        //添加空集
        res.add(temp);
        //通过for循环 定义k 为子集元素个数
        for (int k = 1; k <= ints.length; k++) {
            //定义子集元素开始位置
            int start = 0;
            recursion(start, k, res, temp, ints);
        }

        return res;
    }

    /**
     * k值固定不动
     * 递归循环增加start位置
     */
    private static void recursion(int start, int k, ArrayList<List<Integer>> res, ArrayList<Integer> temp, int[] ints) {
        if (temp.size() == k) {
            res.add(new ArrayList<>(temp));
            return;
        }
        for (int i = start; i < ints.length; i++) {
            temp.add(ints[i]);
            recursion(i + 1, k, res, temp, ints);
            temp.remove(temp.size() - 1);
        }
    }

    private List<List<Integer>> res;


    private void find(int[] nums, int begin, List<Integer> pre) {
        // 没有显式的递归终止
        // 注意：Java 的引用传递机制，这里要 new 一下
        res.add(new ArrayList<>(pre));
        for (int i = begin; i < nums.length; i++) {
            pre.add(nums[i]);
            find(nums, i + 1, pre);
            // 组合问题，状态在递归完成后要重置
            pre.remove(pre.size() - 1);
        }
    }

    public List<List<Integer>> subsets2(int[] nums) {
        int len = nums.length;
        res = new ArrayList<>();
        if (len == 0) {
            return res;
        }
        List<Integer> pre = new ArrayList<>();
        find(nums, 0, pre);
        return res;
    }

    /**
     * [[]] i 0 j 0 [1] recursion i 1
     * [[],[1]] i 1 j 1 [1,2] recursion i 2
     * [[],[1],[1,2]] i 2 j 2 [1,2,3] recursion i 3
     * [[],[1],[1,2],[1,2,3]] i 3 j 3 return
     * i 2 , j 2 remove j 2 [1,2]
     * i 1 , j 1 remove j 1 [1]
     * [[],[1],[1,2],[1,2,3]] i 1 j 2 [1,3] recursion i 3
     * [[],[1],[1,2],[1,2,3],[1,3]] i 3 j 3 return
     * i 1 j 2 remove j 2 [1]
     * i 1 remove j 1 [1]
     * i 0 j 1 [2] recursion i 2
     * [[],[1],[1,2],[1,2,3],[1,3]] i 2 j 2 [2,3] recursion i 3
     * [[],[1],[1,2],[1,2,3],[1,3],[2,3]] i 3 j 3
     * [[],[1],[1,2],[1,2,3],[1,3],[2,3]]  i 1 j 2 [2,3] remove [3]
     * [[],[1],[1,2],[1,2,3],[1,3],[2,3]]  i 1 j 2 [2] remove [2]
     * [[],[1],[1,2],[1,2,3],[1,3],[2,3],[2]]  i 1 j 2 [] remove [2]
     * [[],[1],[1,2],[1,2,3],[1,3],[2],[2,3],[3]]  i 1 j 2 [3]
     * <p>
     * []->[1]->[1,2]->[1,2,3]->[1,3]->[2]->[2,3]->[3]
     */
    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        backtrack(0, nums, res, new ArrayList<Integer>());
        return res;

    }

    private static void backtrack(int i, int[] nums, List<List<Integer>> res, ArrayList<Integer> tmp) {
        res.add(new ArrayList<>(tmp));
        for (int j = i; j < nums.length; j++) {
            tmp.add(nums[j]);
            backtrack(j + 1, nums, res, tmp);
            tmp.remove(tmp.size() - 1);
        }
    }

}
