package com.study.LeetCode.bfs_dfs;

public class T733 {
    public static void main(String[] args) {
        int[][] ints = {{1, 1, 1}, {1, 1, 0}, {1, 0, 1}};
        floodFill(ints, 1, 1, 2);
    }

    public static int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] == newColor) {
            return image;
        }
        dfs(image, sr, sc, image[sr][sc], newColor);
        return image;
    }

    private static void dfs(int[][] image, int sr, int sc, int oldColor, int newColor) {
        if (sr >= 0 && sc >= 0 && sr < image.length && sc < image[sr].length && image[sr][sc] == oldColor) {
            int[][] path = {{0, -1}, {-1, 0}, {1, 0}, {0, 1}};
            image[sr][sc] = newColor;
            for (int i = 0; i < path.length; i++) {
                int x = path[i][0] + sr;
                int y = path[i][1] + sc;
                dfs(image, x, y, oldColor, newColor);
            }
        }
    }
}
