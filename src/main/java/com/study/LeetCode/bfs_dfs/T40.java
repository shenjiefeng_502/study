package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class T40 {
    public static void main(String[] args) {
        int[] ints = {10, 1, 2, 7, 6, 1, 5};
        /**
         *  0
         *  1 0 +1
         *  2 1 +1 , 6 2 +5 ,8 3 +7
         *  4 2 +2 , 7 3 +5 ,12 4 +10
         *  9 5 +4
         *
         */
        List<List<Integer>> lists = combinationSum(ints, 8);
        System.out.println(lists);
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<List<Integer>> result = new ArrayList<>();
        backtracking(0, temp, candidates, target, result);
        return result;
    }

    private static void backtracking(int startIndex, ArrayList<Integer> temp, int[] candidates, int target, ArrayList<List<Integer>> result) {
        if (0 == target) {
            result.add(new ArrayList<>(temp));
            return;
        }
        //todo 添加条件去除相同集合
        for (int i = startIndex; i < candidates.length; i++) {
            if (target - candidates[startIndex] < 0) {
                break;
            }
            if (i > startIndex && candidates[i] == candidates[i - 1]) {
                continue;
            }

            target -= candidates[i];

            temp.add(candidates[i]);
            backtracking(i + 1, temp, candidates, target, result);
            Integer remove = temp.remove(temp.size() - 1);
            target += remove;
        }
    }
}
