package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class T47 {
    public static void main(String[] args) {
        int[] nums = {1, 1, 2};
        List<List<Integer>> lists = permuteUnique(nums);
        System.out.println("123");
    }
    /**
     * 0 r1 [1]
     * 1 r2 [1,1]
     * 2 r=k [1,1,2]
     *  [1,1,2]
     */

    /**
     * 查询集合所有排列结果
     *  测试用例:[1,1,2]
     *  测试结果:[[1,1,2],[1,2,1],[2,1,1]]
     *
     * @param nums the nums
     * @return list the list
     * @author sjf
     * @date 2021-12-10 16:31:01
     */
    public static List<List<Integer>> permuteUnique(int[] nums) {
        Arrays.sort(nums);
        int length = nums.length;
        ArrayList<List<Integer>> res = new ArrayList<>();
        ArrayList<Integer> temp = new ArrayList<>();
        boolean[] used = new boolean[length];
        dfs(length, nums, used, temp, res);
        return res;
    }

    private static void dfs(int k, int[] nums, boolean[] used, ArrayList<Integer> temp, ArrayList<List<Integer>> res) {
        if (k == temp.size()) {
            res.add(new ArrayList<>(temp));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (used[i]) continue;
            if (i > 0 && !used[i - 1] && nums[i] == nums[i - 1]) {
                continue;
            }
            temp.add(nums[i]);
            used[i] = true;
            dfs(k, nums, used, temp, res);
            temp.remove(temp.size() - 1);
            used[i] = false;
        }

    }
}
