package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class T3 {
    public static void main(String[] args) {
        int pwskw = lengthOfLongestSubstringPro("asddvdf");
        char s = 's';
        int[] ints = new int[1];
        ints[0] = s;
        System.out.println(ints[0]);
    }

    public static int lengthOfLongestSubstring(String s) {
        int max = 0;
        if (s == null || s.length() == 0) {
            return max;
        }
        List<Character> strings = new ArrayList<>();
        int r = 0;
        while (r < s.length()) {
            //todo l指针应该指向上一个重复字母后面一位
            char c = s.charAt(r);
            if (strings.contains(c)) {
                max = Math.max(max, strings.size());
                int indexOf = strings.indexOf(c);
                strings = strings.subList(indexOf + 1, strings.size());
            }
            strings.add(c);
            r++;
        }
        return Math.max(max, strings.size());
    }

    public static int lengthOfLongestSubstringPro(String s) {
        int[] last = new int[128];
        for (int i = 0; i < last.length; i++) {
            last[i] = -1;
        }
        int res = 0;
        int start = 0;
        for (int i = 0; i < s.length(); i++) {
            int index = s.charAt(i);
            start = Math.max(start, last[index] + 1);
            res   = Math.max(res, i - start + 1);
            last[index] = i;
        }
        return res;
    }
}
