package com.study.LeetCode.bfs_dfs;


import java.util.ArrayList;
import java.util.List;

public class T17 {
    public static void main(String[] args) {
        List<String> strings = letterCombinations("23");
        System.out.println("123");
    }

    /**
     * 把按键当做一个二维数组,第一维是按键顺序,第二个是这个按键可能的字母
     * 递归里面放一个foreach,控制第二维度,用递归来控制深度
     * @param digits
     * @return
     */
    public static List<String> letterCombinations(String digits) {
        ArrayList<String> res = new ArrayList<>();
        char[][] buttons = {{'a', 'b', 'c'}, {'d', 'e', 'f'}, {'g', 'h', 'i'}, {'j', 'k', 'l'}, {'m', 'n', 'o'}, {'p', 'q', 'r', 's'}, {'t', 'u', 'v'}, {'w', 'x', 'y', 'z'}};
        char[] chars = digits.toCharArray();
        if (chars.length == 0) {
            return res;
        }
        //用于存储位数不够的临时字符串
        StringBuilder temp = new StringBuilder();
        dfs(0, 0, buttons, chars, temp, res);
        return res;
    }

    private static void dfs(int startIndex, int button, char[][] buttons, char[] chars, StringBuilder temp, ArrayList<String> res) {
        if (temp.length() == chars.length) {
            //需要新new一个String 不然指针指向都是同一个
            res.add(new String(temp));
            return;
        }
        //for循环控制[0,0]或[1,0]
        //从[0,0]开始下一个递归[1,0]或[1,1]
        int but = Integer.valueOf(chars[startIndex]) - 50;
        char[] ints = buttons[but];
        for (int i = button; i < ints.length; i++) {
            temp.append(ints[i]);
            dfs(startIndex + 1, button, buttons, chars, temp, res);
            temp.deleteCharAt(temp.length() - 1);
        }
    }

}
