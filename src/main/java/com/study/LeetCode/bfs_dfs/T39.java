package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.List;

public class T39 {
    public static void main(String[] args) {
        int[] ints = new int[]{2, 3, 6, 7};
        List<List<Integer>> lists = combinationSum(ints, 7);
        System.out.println("123");
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<List<Integer>> result = new ArrayList<>();
        backtracking(0, temp, candidates, target, result);
        return result;
    }

    private static void backtracking(int startIndex, ArrayList<Integer> temp, int[] candidates, int target, ArrayList<List<Integer>> result) {
        if (0 == target) {
            result.add(new ArrayList<>(temp));
            return;
        }
        if (0 > target) {
            return;
        }
        for (int i = startIndex; i < candidates.length; i++) {
            target -= candidates[i];
            temp.add(candidates[i]);
            backtracking(i, temp, candidates, target, result);
            Integer remove = temp.remove(temp.size() - 1);
            target += remove;
        }
    }
}
