package com.study.LeetCode.bfs_dfs;

public class T79 {
    public static void main(String[] args) {
        boolean b = exist(new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "ABCCED");
        System.out.println(b);
    }

    /**
     * 普通递归回溯,没啥可讲的
     * 剪枝条件1:超出二维数组边界的
     * 已经被访问过的数据不能再次访问
     *
     * @param board
     * @param word
     * @return
     */
    public static boolean exist(char[][] board, String word) {
        boolean[][] isVist = new boolean[board.length][board[0].length];
        int[][] xyList = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boolean exit = dfs(0, i, j, isVist, xyList, word, board);
                if (exit) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Dfs
     *
     * @param checkIndex 该字母需要跟char数组中的第几位进行比对
     * @param i          x轴
     * @param j          y轴
     * @param isVist
     * @param xyList
     * @param chars      the chars
     * @param board      the board
     * @return boolean the boolean
     * @author sjf
     * @date 2021-12-14 09:55:13
     */
    private static boolean dfs(int checkIndex, int i, int j, boolean[][] isVist, int[][] xyList, String chars, char[][] board) {
        //结束条件:当判断完数组以后继续递归时,结束
        if (checkIndex == chars.length() - 1 && board[i][j] == chars.charAt(checkIndex)) {
            return true;
        }
        //递归
        boolean result = false;
        if (board[i][j] == chars.charAt(checkIndex)) {
            isVist[i][j] = true;
            for (int l = 0; l < xyList.length; l++) {
                int x = i + xyList[l][0];
                int y = j + xyList[l][1];
                //递归前剪枝
                if (x >= 0 && y >= 0 && board.length > x && board[x].length > y && !isVist[x][y]) {
                    boolean flag = dfs(checkIndex + 1, x, y, isVist, xyList, chars, board);
                    if (flag) {
                        result = true;
                        break;
                    }
                }
            }
            isVist[i][j] = false;
        }
        return result;
    }
}
