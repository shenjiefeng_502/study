package com.study.LeetCode.bfs_dfs;

public class TT695 {
    public static void main(String[] args) {
        int i = maxAreaOfIsland(new int[][]{{0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 1,
                0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0}, {0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}});
        System.out.println(i);
    }

    public static int maxAreaOfIsland(int[][] grid) {
        int maxCount = 0;
        if (grid.length == 0) {
            return 0;
        }
        int[][] ints = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                maxCount = Math.max(maxCount, dfs(grid, i, j, ints));
            }
        }
        return maxCount;
    }

    private static int dfs(int[][] grid, int i, int j, int[][] ints) {
        if (grid[i][j] == 1) {
            grid[i][j] = -1;
            int nowArea = 1;
            for (int i1 = 0; i1 < ints.length; i1++) {
                int x = ints[i1][0] + i;
                int y = ints[i1][1] + j;
                if (x >= 0 && y >= 0 && grid.length > x && grid[x].length > y) {
                    nowArea += dfs(grid, x, y, ints);
                }
            }
            return nowArea;
        }
        return 0;
    }
}
