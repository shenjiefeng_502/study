package com.study.LeetCode.bfs_dfs;

import java.util.ArrayList;
import java.util.HashSet;

//给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则
//，返回 false 。
//
// 二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。
//
//
//
// 示例 1：
//
//
//输入：root = [3,4,5,1,2], subRoot = [4,1,2]
//输出：true
public class T572 {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static boolean isSubtree(TreeNode root, TreeNode subRoot) {
        return dfs(root, subRoot);
    }

    public static boolean dfs(TreeNode s, TreeNode t) {
        if (s == null) {
            return false;
        }
        return isSame(s, t) || dfs(s.left, t) || dfs(s.right, t);
    }

    private static boolean isSame(TreeNode node, TreeNode subRoot) {
        if (node == null && subRoot == null) {
            return true;
        }
        if (node == null || subRoot == null || node.val != subRoot.val) {
            return false;
        }
        return isSame(node.left, subRoot.left) && isSame(node.right, subRoot.right);
    }

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(3);
        treeNode.right = new TreeNode(5);
        TreeNode node1 = new TreeNode(4);
        node1.right = new TreeNode(2);
        node1.left = new TreeNode(1);
        treeNode.left = node1;
        TreeNode node = new TreeNode(4);
        node.right = new TreeNode(1);
        node.right = new TreeNode(2);
        boolean subtree = isSubtree(treeNode, node);
    }
}
