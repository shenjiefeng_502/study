package com.study.LeetCode.bfs_dfs;

import java.util.LinkedList;

public class T130 {
    /**
     * bfs行不通,没法一次遍历就把结果更改完,
     * 可能存在最后一行影响上面结果
     *
     * @param board
     */
    public static void bfssolve(char[][] board) {
        int m = board.length;
        int n = board[0].length;
        int[][] isV = new int[m][n];
        for (int x = 0; x < m; x++) {
            for (int y = 0; y < n; y++) {
                char checkValue = checkValue(x, y, m, n);
                bfs(board, x, y, isV, checkValue);
            }
        }
    }

    private static char checkValue(int x, int y, int m, int n) {
        if (x == 0 || y == 0 || x == m - 1 || y == n - 1) {
            return 'O';
        } else {
            return 'X';
        }
    }

    private static void bfs(char[][] board, int i, int j, int[][] isV, char checkValue) {
        int[] xs = {1, 0, -1, 0};
        int[] ys = {0, 1, 0, -1};
        LinkedList<int[]> queue = new LinkedList();
        queue.add(new int[]{i, j});
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int count = 0; count < size; count++) {
                int[] poll = queue.poll();
                int x = poll[0];
                int y = poll[1];
                if (x >= 0 && y >= 0 && board.length > x && board[0].length > y && board[x][y] == 'O' && isV[x][y] == 0) {
                    board[x][y] = checkValue;
                    for (int i1 = 0; i1 < 4; i1++) {
                        int i2 = x + xs[i1];
                        int i3 = y + ys[i1];
                        if (i2 >= 0 && i3 >= 0 && board.length > i2 && board[0].length > i3) {
                            queue.add(new int[]{i2, i3});
                        }
                    }
                    isV[x][y]++;
                }
            }
        }
    }

    public static void main(String[] args) {
        char[][] chars = {{'X', 'O', 'X', 'O', 'X', 'O'}, {'O', 'X', 'O', 'X', 'O', 'X'}, {'X', 'O', 'X', 'O', 'X', 'O'}, {'O', 'X', 'O', 'X', 'O', 'X'}};
        solve(chars);
        for (int i = 0; i < chars.length; i++) {
            for (int i1 = 0; i1 < chars[i].length; i1++) {
                System.out.println(chars[i][i1]);
            }
            System.out.println("-----");
        }
    }

    private static void solve(char[][] board) {
        int m = board.length;
        int n = board[0].length;
        //对边界上的o进行深度搜索将每个o变成-
        for (int x = 0; x < m; x++) {
            int y = 0;
            dfs(board, x, y);
            y = n - 1;
            dfs(board, x, y);
        }
        for (int y = 0; y < n; y++) {
            int x = 0;
            dfs(board, x, y);
            x = m - 1;
            dfs(board, x, y);
        }
        //将o变成x
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
                if (board[i][j] == '-') {
                    board[i][j] = 'O';
                }
            }
        }
        //将-变成o

    }

    private static void dfs(char[][] board, int x, int y) {
        int[] xs = {1, 0, -1, 0};
        int[] ys = {0, 1, 0, -1};
        if (board[x][y] == 'O') {
            board[x][y] = '-';
            for (int i = 0; i < 4; i++) {
                int x1 = xs[i] + x;
                int y1 = ys[i] + y;
                if (x1 >= 0 && y1 >= 0 && board.length > x1 && board[0].length > y1) {
                    dfs(board, x1, y1);
                }
            }
        }
    }
}
