package com.study.LeetCode.dongtaiguihua;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/11/27  20:05
 * @description:动态规划
 */
public class T907 {
    /**
     * 数组大小	初始数字	0	    1	    2	    3
     * 1	    1	    3	    1	    2	    4
     * 2		1	    1	    2
     * 3		1	    1
     * 4		1
     */
    public static int sumSubarrayMins(int[] arr) {
        //动态规划
        int minSum = 0;
        if (arr == null || arr.length == 0) {
            return minSum;
        }
        //子数组的数组个数
        for (int i = 1; i <= arr.length; i++) {
            //遍历的起始位置
            for (int j = 0; j < arr.length; j++) {
                minSum += getMin(getChildArr(arr, i, j));
            }
        }
        return minSum;
    }

    public static void main(String[] args) {
        int[] ints = {3, 1, 2, 4};
        int i = sumSubarrayMins(ints);
        System.out.println(i);
    }

    /**
     * 获取子树组
     *
     * @param arr
     * @param i   遍历的起始位置
     * @param j   子数组的数组个数
     * @return
     */
    private static int[] getChildArr(int[] arr, int i, int j) {
        int[] ints = new int[i];
        int count = 0;
        int boundary = arr.length;
        //遍历越界,会存在数组执行到末尾还会往后遍历一次的情况,但由于数组默认值为0,最小值就为0
        //笑哭:依赖bug运行
        for (int index = j; index < boundary; index++) {
            ints[count++] = arr[index];
            if (count == i) {
                break;
            }
        }
        return ints;
    }

    private static int getMin(int[] childArr) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < childArr.length; i++) {
            if (childArr[i] < min) {
                min = childArr[i];
            }
        }
        return min;
    }

}
