package com.study.LeetCode.dongtaiguihua;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class T120 {
    /**
     * [2],[3,4],[6,5,7],[4,1,8,3]
     *
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<List<Integer>> lists = new ArrayList<>();
        lists.add(Arrays.asList(2));
        lists.add(Arrays.asList(3, 4));
        lists.add(Arrays.asList(6, 5, 7));
        lists.add(Arrays.asList(4, 1, 8, 3));
        int i = minimumTotal(lists);
        System.out.println(i);
    }

    /**
     * 1.dp数组以及dp数组中i和j的含义
     * 2.递归公式
     * 3.dp数组如何初始化
     * 4.遍历顺序
     */
    public static int minimumTotal(List<List<Integer>> triangle) {
        //dp数组以及dp数组中i和j的含义
        //dp数组最短路径,每个i表示行,j表示列数
        int size = triangle.size();
        int[] dp = new int[size + 1];
        //初始化化值为0 dp[0][0]=triangle.get(0).get(0)
        //从下到上
        for (int i = size - 1; i >= 0; i--) {
            //todo 领会一下
            for (int j = 0; j <= i; j++) {
                //递推公式
                //dp[i][j]=min(dp[i+1],dp[i+1][j+1])+dp[1][j]
                Integer a = dp[j];
                Integer b = dp[j + 1];
                Integer integer = triangle.get(i).get(j);
                dp[j] = Math.min(a, b) + integer;
            }
        }
        return dp[0];
    }
}
