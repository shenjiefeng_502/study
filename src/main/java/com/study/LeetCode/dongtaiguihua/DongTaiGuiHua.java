package com.study.LeetCode.dongtaiguihua;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/11/19  22:19
 * @description:金矿问题
 */

/**
 * 有n座金矿，每座金矿的黄金储备不同，需要的工人也不同，计算出m个工人下，最大的收益
 * 现在总共有10名工人，5座金矿分别为200kg/需3人,300kg/4人,350kg/3人,400kg/5人,500kg/5人
 */
public class DongTaiGuiHua {
    /**
     * 每个金矿存在着挖和不挖两种选择
     * 首先做个假设
     * 假设最后一座金矿永远不会被挖掘，那么问题就变成了从四个金矿里选择最优结果
     * 又或者，最后一座金矿一定要挖掘，那么收益就是这座金矿+剩余人的总收益
     * 这样一次又一次的进行选择，直到剩余0个工人，（0个工人就是边界）
     * 把整个问题拆分成这样一次又一次的查询最优解，就能得到最高的收益
     * 按这种思路通过递归的方式进行实现
     */
    /**
     * 获得的金矿最优收益
     *
     * @param w 工人数量
     * @param n 可选金矿数量
     * @param p 金矿开采所需的工人数量
     * @param g 金矿收益
     */
    public static int getBestGoldMining(int w, int n, int[] p, int[] g) {
        //到达边界
        if (w == 0 || n == 0) {
            return 0;
        }
        if (w < p[n - 1]) {
            return getBestGoldMining(w, n - 1, p, g);
        }
        return Math.max(getBestGoldMining(w, n - 1, p, g)
                , getBestGoldMining(w - p[n - 1], n - 1, p, g) + g[n - 1]);

    }

    public static void main(String[] args) {
        int w = 10;
        int[] p = {5, 5, 3, 4, 3};
        int[] g = {400, 500, 200, 300, 350};
        getBestGoldMining(w, g.length, p, g);
    }
    /**
     * 上面递归算法其实存在很多数据重复运算
     */
    /**
     * 改进一下思路使用动态规划
     * 将数据更改成一张表格，
     * 每有一座金矿就增加一行数据
     * 每有一个工人则增加一列数据
     * 中间空白的格子上，是用于计算当前格子里的最佳收益的。
     */
    /**
     * @param w 工人数量
     * @param p 金矿开采所需的工人数量
     * @param g 金矿收益
     * @return
     */
    public static int getBestGoldMiningV2(int w, int[] p, int[] g) {
        //创建表格
        int[][] resultTable = new int[g.length + 1][w + 1];
        //填充表格
        for (int i = 1; i < g.length; i++) {
            for (int j = 0; j < w; j++) {
                if (j < p[i - 1]) {
                    resultTable[i][j] = resultTable[i - 1][j];
                } else {
                    //获得当前位置最优解
                    resultTable[i][j] = Math.max(resultTable[i - 1][j],
                            resultTable[i - 1][j - p[i - 1]] + g[i - 1]);
                }
            }
        }
        return resultTable[g.length][w];
    }
    /**
     * 优化空间位o(n)
     * 根据人数遍历时，只保存当前人数下的最优解就可以了
     */
    /**
     * @param w 工人数量
     * @param p 金矿开采所需的工人数量
     * @param g 金矿收益
     * @return
     */
    public static int getBestGoldMiningV3(int w, int[] p, int[] g) {
        //创建表格
        int[] results = new int[g.length + 1];
        //填充表格
        for (int i = 1; i < g.length; i++) {
            //这里需要从最大人数往最小人数进行计算，
            //因为左边的值 我们计算最大值的时候会用到，如果从左往右，则左边值会被更新掉
            for (int j = w; j > 0; j--) {
                if (j >= p[i - 1]) {
                    results[j] = Math.max(results[j],
                            results[j - p[i - 1]] + g[i - 1]);
                }
            }
        }
        return results[w];
    }

}
