package com.study.LeetCode.dongtaiguihua;

import java.util.ArrayList;

public class T70 {
    public static void main(String[] args) {
        int res = climbStairs(5);
        System.out.println(res);
    }

    public static int climbStairs(int n) {
        if (n < 3) {
            return n;
        }
        int[] dp = new int[3];
        dp[0] = 0;
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 2; i < n; i++) {
            dp[0] = dp[1];
            dp[1] = dp[2];
            dp[2] = dp[0] + dp[1];
        }
        return dp[2];
    }
}
