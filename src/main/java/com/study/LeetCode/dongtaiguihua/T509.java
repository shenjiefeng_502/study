package com.study.LeetCode.dongtaiguihua;

public class T509 {
    public static void main(String[] args) {
        int fib = fib(0);
        System.out.println(fib);
    }

    /**
     * 1.dp数组以及dp数组中i和j的含义
     * 2.递归公式
     * 3.dp数组如何初始化
     * 4.遍历顺序
     *
     * @author sjf
     * @date 2021-12-29 10:56:07
     */
    public static int fib(int n) {
        if (n < 2) {
            return n;
        }
        //dp[n]=n位上的和值
        //由于是从0开始的所以数组保存的数据总数要比n多一位,
        int[] dp = new int[n + 1];
        //递归公式初始化f(0)和f(1)即可
        dp[0] = 0;
        dp[1] = 1;
        //递归公式= F(2) = F(1) + F(0)
        //递归顺序是从0到n
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }
}
