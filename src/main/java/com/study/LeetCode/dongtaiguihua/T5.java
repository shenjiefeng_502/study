package com.study.LeetCode.dongtaiguihua;

public class T5 {
    public static void main(String[] args) {
        String s = "abdce";
        System.out.println(longestPalindrome("abadce"));
    }

    /**
     * s[i+1:j-1]s[i+1:j−1] 是回文串，并且 ss 的第 ii 和 jj 个字母相同时，s[i:j]s[i:j] 才会是回文串。
     *
     * 上文的所有讨论是建立在子串长度大于 2 的前提之上的，
     * 我们还需要考虑动态规划中的边界条件，即子串的长度为 1 或 2。
     * 对于长度为 1 的子串，它显然是个回文串；
     * 对于长度为 2 的子串，只要它的两个字母相同，它就是一个回文串。因此我们就可以写出动态规划的边界条件：
     *
     * @param s
     * @return
     */
    public static String longestPalindrome(String s) {

        int len = s.length();
        if (len < 2) {
            return s;
        }

        int maxLen = 1;
        int begin = 0;
        boolean[][] dp = new boolean[len][len];
        for (int i = 0; i < len; i++) {
            dp[i][i] = true;
        }
        char[] charArray = s.toCharArray();
        for (int l = 2; l <= len; l++) {
            for (int i = 0; i < len; i++) {
                // 由 L 和 i 可以确定右边界，即 j - i + 1 = L 得
                //abacd
                int j = l + i - 1;
                if (j >= len) {
                    break;
                }
                if (charArray[i] != charArray[j]) {
                    dp[i][j] = false;
                } else {
                    if (j - i < 3) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }
                // 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
                if (dp[i][j] && j - i + 1 > maxLen) {
                    maxLen = j - i + 1;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin + maxLen);
    }

    private static boolean isHuiWen(String substring) {
        int length = substring.length() - 1;
        int i = 0;
        while ((substring.charAt(i) == substring.charAt(length)) && length > i) {
            i++;
            length--;
        }
        if (length == i) {
            return true;
        } else {
            return false;
        }
    }
}
