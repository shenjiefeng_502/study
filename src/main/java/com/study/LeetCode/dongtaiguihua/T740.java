package com.study.LeetCode.dongtaiguihua;

/**
 * 将数组转换一下,转换为list,list下标表明该数字的值,list值表示数组中有几个这个下标数组.
 */
public class T740 {
    public static void main(String[] args) {
        int[] ints = {2, 2, 3, 3, 3, 4};
        int i = deleteAndEarn(ints);
        System.out.println(i);
    }

    public static int deleteAndEarn(int[] nums) {
        int[] temp = new int[10001];
        for (int val : nums) {
            temp[val] += val;
        }
        int first = temp[0], second = temp[1];
        for (int i = 2; i < temp.length; i++) {
            int tem=second;
            second=Math.max(second,first+temp[i]);
            first=tem;
        }
        return second;
    }
}
