package com.study.LeetCode.dongtaiguihua;

// 示例 2：
//
//
//输入：[2,7,9,3,1]
//输出：12
//解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
//     偷窃到的最高金额 = 2 + 9 + 1 = 12 。
public class T198 {
    public static void main(String[] args) {
        int[] ints = {2, 1, 1, 2};
        int rob = rob(ints);
        System.out.println(rob);
    }

    /**
     * 偷7不偷2,偷11不偷7,偷11不偷10,偷12不偷10
     * 把他当成两个平行数据,比较两个数据谁大,反正只需要最大金额,不需要数组index
     *
     * @param nums the nums
     * @return int the int
     * @author sjf
     * @date 2021-12-28 15:21:15
     */
    public static int rob(int[] nums) {
//        int maxmon = 0;
//        if (nums == null || nums.length == 0) {
//            return maxmon;
//        }
//        int length = nums.length;
//        if (length == 1) {
//            return nums[0];
//        }
//        int[] dp = new int[length];
//        dp[0] = nums[0];
//        dp[1] = Math.max(nums[0], nums[1]);
//        for (int i = 2; i < length; i++) {
//            dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
//        }
//        return dp[length - 1];
//    }


        int len = nums.length;
        if (len == 1) {
            return nums[0];
        }
        if (len == 2) {
            return Math.max(nums[0], nums[1]);
        }
        int[] dp = new int[3];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0],nums[1]);
        for (int i = 2; i < len; i++) {
            dp[2] = Math.max((dp[0] + nums[i]), dp[1]);
            dp[0] = dp[1];
            dp[1] = dp[2];
        }
        return dp[2];
    }


//        int s = 0;
//        int d = 0;
//        for (int i = 0; i < nums.length; i++) {
//            todo 不应该定死加哪个数据,
//            if (i % 2 == 0) {
//                d += nums[i];
//                maxmon = Math.max(maxmon, d);
//            } else {
//                s += nums[i];
//                maxmon = Math.max(maxmon, s);
//            }
//        }
//        return maxmon;
//    }
}
