package com.study.LeetCode.dongtaiguihua;

public class T1137 {

    public int tribonacci(int n) {
        int[] dp = new int[4];
        int[] dpl = new int[n + 1];
        dp[0] = 0;
        dp[1] = 0;
        dp[2] = 1;
        dp[3] = 1;
        if (n < 3) {
            return dp[n];
        }
        for (int i = 3; i <= n; ++i) {
            dp[0] = dp[1];
            dp[1] = dp[2];
            dp[2] = dp[3];
            int sum = dp[0] + dp[1] + dp[2];
            dp[3] = sum;
        }
        return dp[3];
    }
}
