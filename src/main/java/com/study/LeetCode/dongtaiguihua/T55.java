package com.study.LeetCode.dongtaiguihua;

//给定一个非负整数数组 nums ，你最初位于数组的 第一个下标 。
//数组中的每个元素代表你在该位置可以跳跃的最大长度。
//判断你是否能够到达最后一个下标。
public class T55 {
    /**
     * 动态规划
     * dp数组存储最远距离
     * 最远距离等于下标加距离
     * 如果最远距离大于直接返回true
     *
     * @param nums
     * @return
     */
    public boolean canJump(int[] nums) {
        int len = nums.length;
        if (len < 2) {
            return true;
        }
        int[] dp = new int[len];
        int maxStep = nums[0];
        dp[0] = nums[0];
        dp[1] = nums[1];
        for (int i = 1; i < nums.length; i++) {
            //如果当前位置大于了能跳的最远位置直接返回false
            if (i > maxStep) return false;
            dp[i] = Math.max(dp[i - 1], nums[i] + i);
            //维护最远位置
            maxStep = Math.max(dp[i],maxStep);
            if(maxStep > nums.length) return true;
        }
        return false;
    }
}
