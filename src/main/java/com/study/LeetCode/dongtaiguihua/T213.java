package com.study.LeetCode.dongtaiguihua;

/**
 * 比一版多了个头尾相连
 */
public class T213 {
    /**
     * 只能偷 1 , n 或者 0 , n - 1
     * n为len - 1
     *
     * @param nums
     * @return
     */
    public int rob(int[] nums) {
        int len = nums.length;
        if (len == 1) {
            return nums[0];
        }
        if (len == 2) {
            return Math.max(nums[0], nums[1]);
        }
        return Math.max(robRange(nums, 0, len - 2), robRange(nums, 1, len - 1));
    }

    private int robRange(int[] nums, int start, int end) {
        int first = nums[start];
        int second = Math.max(nums[start], nums[start +1]);
        for (int i = start + 2; i <= end; i++) {
            int temp = second;
            second = Math.max(second, nums[i] + first);
            first = temp;
        }
        return second;
    }
}
