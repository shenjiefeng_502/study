package com.study.LeetCode.dongtaiguihua;

public class T746 {
    public static void main(String[] args) {
        int[] ints = {1,100,1,1,1,100,1,1,100,1};
        int i = minCostClimbingStairs(ints);
    }
    /**
     * 动态规划
     *
     * @param cost
     * @return
     */
    public static int minCostClimbingStairs(int[] cost) {
        if (cost.length < 2) {
            return 0;
        }
        if (cost.length == 2) {
            return Math.min(cost[0], cost[1]);
        }
        /**
         * dp数组的含义,已经数组内元素的含义
         * dp数组表示当前位置上,最小消费,i表示位置
         */
        int[] dp = new int[cost.length + 1];
        /**
         * dp数组初始化都是0
         */
        /**
         * 递推函数为 f(n) = min(f(n-1),f(n-2))
         */
        dp[0] = cost[0];
        dp[1] = cost[1];
        for (int i = 2; i < cost.length; i++) {
            dp[i] = Math.min(dp[i - 1], dp[i - 2]) + cost[i];
        }
        return Math.min(dp[cost.length - 1], dp[cost.length - 2]);

    }
}
