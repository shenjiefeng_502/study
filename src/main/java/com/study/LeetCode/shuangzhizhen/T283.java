package com.study.LeetCode.shuangzhizhen;

import java.util.Arrays;

/**
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 * 示例:
 * 输入: [0,1,0,3,12]
 * 输出: [1,3,12,0,0]
 */
public class T283 {
    public static void main(String[] args) {
        int[] ints = {1, 0, 12, 0};
        moveZeroes(ints);
        System.out.println("123");
    }

    /**
     * 双指针
     * 左指向已经处理好的尾部
     * 右指向未处理的头部
     * 右指针指向非0时,放在l末尾,l++
     *
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int l = 0, r = 0;
        while (r < nums.length) {
            if (nums[r] != 0) {
                swap(nums, l, r);
                l++;
            }
            r++;
        }
    }

    private static void swap(int[] nums, int l, int r) {
        int temp = nums[l];
        nums[l] = nums[r];
        nums[r] = temp;
    }
}
