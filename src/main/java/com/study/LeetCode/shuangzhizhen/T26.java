package com.study.LeetCode.shuangzhizhen;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/8/18  16:52
 * @description:正序排序数组原地删除重复项
 */
public class T26 {
    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int i = removeDuplicates(nums);
        System.out.println(i);
    }

    public static int removeDuplicates(int[] nums) {
        if (nums.length <= 1) {
            return nums.length;
        }
        //由于正序，尝试双指针
        int j = 1;
        //初始第二个指针
        int i = 1;
        //i指向下个数字位置
        while (j < nums.length) {
            //当存在重复数时
            if (nums[j] != nums[j - 1]) {
                nums[i] = nums[j];
                //查找下一个不一样的数
                //原地删除后下一个存储位置不变
                //下一个数组查找的位置需要为j
                i++;
                //原地删除

            }
            ++j;
        }
        return i;
    }
}
