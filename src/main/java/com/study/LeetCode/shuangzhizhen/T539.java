package com.study.LeetCode.shuangzhizhen;

import java.util.Arrays;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/24  09:44
 * @description: 最小时间差
 */
public class T539 {
    public static void main(String[] args) {
        int i = findMinDifference(Arrays.asList("00:00", "23:59", "22:22", "23:57", "00:10", "02:05"));
        System.out.println(i);
    }

    /**
     * 查找最小时间差
     * 1.当时间等于00:00时需要把00:00转为24:00再次判断
     *
     * @param timePoints
     * @return int
     * @MethodName: findMinDifference
     * @auth: sjf
     * @date 2022/1/24 09:57
     */
    public static int findMinDifference(List<String> timePoints) {
        int midNum = 0;
        for (int i = 0; i < timePoints.size(); i++) {
            for (int j = 1; j < timePoints.size(); j++) {
                Integer tran = tranInt(timePoints.get(i));
                Integer tran1 = tranInt(timePoints.get(j));
                midNum = Math.min(midNum, Math.abs(tran - tran1));
                if (tran == 0 || tran1 == 0) {
                    midNum = Math.min(midNum, checkTF(tran, tran1));
                }
            }
        }
        return midNum;
    }

    private static int checkTF(Integer tran, Integer tran1) {
        if (tran == 0) {
            return Math.abs(2400 - tran1);
        } else {
            return Math.abs(2400 - tran);
        }
    }

    private static Integer tranInt(String s) {
        String replaceFirst = s.replaceFirst(":", "");
        return Integer.valueOf(replaceFirst);
    }
}
