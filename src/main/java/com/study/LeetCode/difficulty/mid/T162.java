package com.study.LeetCode.difficulty.mid;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/28  15:15
 * @description: 寻找峰值
 */
public class T162 {
    public static void main(String[] args) {
        int[] nums = new int[]{1,2,1,3,5,6,4};
        int peakElement = findPeakElement(nums);
        System.out.println(peakElement);
    }

    public static int findPeakElement(int[] nums) {
        int idx = 0;
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] > nums[idx]) {
                idx = i;
            }
        }
        return idx;
    }
}
