package com.study.LeetCode.difficulty.mid;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/19  14:06
 * @description:在排序数组中查找元素的第一个和最后一个位置
 */
public class T34 {
    public static void main(String[] args) {
        int[] ints = new int[]{5, 7, 7, 8, 10};
        int[] range = searchRange(ints, 5);
        System.out.println(range[0]);
        System.out.println(range[1]);
    }

    /**
     * 二分查找
     *
     * @param nums
     * @param target
     * @return int[]
     * @MethodName: searchRange
     * @auth: sjf
     * @date 2022/4/19 14:06
     */
    public static int[] searchRange(int[] nums, int target) {
        int mid = getInts(nums, target);
        if (mid == -1) {
            return new int[]{-1, -1};
        }
        return getIntAry(nums, mid);
    }

    private static int[] getIntAry(int[] nums, int mid) {
        int left = mid;
        while (true) {
            if (left >= 0 && nums[left] == nums[mid]) {
                left--;
            } else {
                left++;
                break;
            }
        }
        int right = mid;
        while (true) {
            if (right < nums.length && nums[right] == nums[mid]) {
                right++;
            } else {
                right--;
                break;
            }
        }
        return new int[]{left, right};
    }

    private static int getInts(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (right >= left) {
            int mid = (left + right) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }

}
