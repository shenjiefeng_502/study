package com.study.LeetCode.difficulty.mid;

import java.util.Arrays;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/28  14:43
 * @description: 寻找旋转排序数组中的最小值
 */
public class T153 {

    public static void main(String[] args) {
        int[] ints = new int[]{3, 4, 5, 1, 2};
        int min = findMin(ints);
        System.out.println(min);
    }

    public static int findMin(int[] nums) {
        Arrays.sort(nums);
        return nums[0];
    }
}
