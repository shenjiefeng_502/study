package com.study.LeetCode.difficulty.mid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/24  10:34
 * @description: T74
 */
public class T74 {
    public static void main(String[] args) {
        int[][] ints = new int[3][];
        ints[0] = new int[]{1, 3, 5, 7};
        ints[1] = new int[]{10, 11, 16, 20};
        ints[2] = new int[]{23, 30, 34, 60};
        boolean b = searchMatrix(ints, 13);
        System.out.println(b);
    }

    /**
     * 二分查找
     *
     * @param matrix
     * @param target
     * @return boolean
     * @MethodName: searchMatrix
     * @auth: sjf
     * @date 2022/4/24 10:44
     */
    public static boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        int l = 0;
        int r = (m * n) - 1;
        while (l <= r) {
            int mid = (r - l) / 2 + l;
            int x = matrix[mid / n][mid % n];
            if (x == target) {
                return true;
            }
            if (x > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return false;
    }
}
