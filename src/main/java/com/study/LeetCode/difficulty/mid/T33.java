package com.study.LeetCode.difficulty.mid;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/21  15:30
 * @description: 搜索旋转排序数组
 */
public class T33 {
    public static void main(String[] args) {
        int[] nums = new int[]{3, 1};
        int i = search(nums, 1);
        System.out.println(i);
    }

    /**
     * 二分查找,两边肯定有一边是有序的,
     * 先尝试解决有序那边数据的问题
     *
     * @param nums
     * @param target
     * @return int
     * @MethodName: search
     * @auth: sjf
     * @date 2022/4/24 09:56
     */
    public static int search(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return -1;
        }
        if (n == 1) {
            if (nums[0] == target) {
                return 0;
            } else {
                return -1;
            }
        }
        int l = 0;
        int r = n - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            //先把mid==target判断了
            if (nums[mid] == target) {
                return mid;
            }
            //左边有序的情况下
            if (nums[0] < nums[mid]) {
                //判断数据是否在有序数据的范围内,如果有则数据则数据该在0-mid之间,否则在右边或无
                if (nums[mid] > target && nums[0] <= target) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } else {
                if (nums[mid] < target && nums[n - 1] >= target) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return -1;
    }


}
