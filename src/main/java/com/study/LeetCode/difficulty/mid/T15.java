package com.study.LeetCode.difficulty.mid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/28  15:52
 * @description: 三数之和
 */
public class T15 {
    public static void main(String[] args) {
        int[] nums = new int[]{-1, 0, -1, 2, -1, -4};
        List<List<Integer>> list = threeSum(nums);
        System.out.println("");
    }

    /**
     * 三数之和
     *
     * @param nums
     * @return Integer
     * @MethodName: threeSum
     * @auth: sjf
     * @date 2022/5/6 13:27
     */
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        ArrayList<List<Integer>> result = new ArrayList<>();
        if (nums.length < 3) {
            return result;
        }
        for (int i = 0; i < nums.length; i++) {
            //当起始值就是正整数时,则三个正整数不可能为0,直接返回
            if (nums[i] > 0) {
                break;
            }
            //当两个数相同时,只进行一次循环,达到去重
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int l = i + 1;
            int r = l + 1;
            while (l < nums.length - 1) {//双指针循环结束条件
                if (r == nums.length || nums[l] == nums[l - 1]) {//l指针进位条件
                    l++;
                    r = l + 1;
                    continue;
                }
                // 三元组元素b去重
                if (r > i + 2 && nums[r - 1] == nums[r - 2] && nums[r] == nums[r - 1]) {
                    r++;
                    continue;
                }
                if (nums[i] + nums[l] + nums[r] == 0) {
                    result.add(Arrays.asList(nums[i], nums[l], nums[r]));
                }
                r++;
            }
        }
        return result;
    }
}
