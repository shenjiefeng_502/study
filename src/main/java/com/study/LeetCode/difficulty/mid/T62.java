package com.study.LeetCode.difficulty.mid;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/28  15:24
 * @description: 删除排序链表中的重复元素 II
 */
public class T62 {

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode next = head.next;
        if (head.val == next.val) {
            while (next != null && head.val == next.val) {
                next = next.next;
            }
            /**
             * 递归
             * 通过递归查询下一个数据是否是重复数据
             */
            head = deleteDuplicates(next);
        } else {
            head.next = deleteDuplicates(next);
        }
        return head;
    }
}
