package com.study.LeetCode.difficulty.hard;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/8  13:22
 * @description: 寻找两个正序数组的中位数
 */
public class T4 {
    public static void main(String[] args) {
        int[] num1 = new int[]{0};
        int[] num2 = new int[0];
        double v = findMedianSortedArrays(num1, num2);
        System.out.println(v);
    }

    /**
     * 循环终止条件:((l1+l2)/2)+1
     *
     * @param nums1
     * @param nums2
     * @return double
     * @MethodName: findMedianSortedArrays
     * @auth: sjf
     * @date 2022/4/18 10:36
     */
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        //aIndex:a数组下标
        int aIndex = 0;
        //bIndex:b数组下标
        int bIndex = 0;
        //left:上次循环的值
        int left = -1;
        //right:这次循环的值
        int right = -1;
        //总数
        int lena = nums1.length;
        int lenb = nums2.length;
        int len = lena + lenb;
        //循环终止条件计算
        //原理:取中位数都是取两数组中间值,如果是偶数数组,也是到(len / 2) + 1
        //但偶数取(left+right) /2
        int end = (len / 2) + 1;
        for (int i = 0; i < end; i++) {
            //aIndex,bIndex进位条件:
            //a小且未到末尾a进 或 b到末尾
            //a到末尾或其他则b进
            int temp = right;
            if (lena > aIndex && (lenb <= bIndex || nums1[aIndex] <= nums2[bIndex])) {
                right = nums1[aIndex];
                aIndex++;
            } else {
                right = nums2[bIndex];
                bIndex++;
            }
            left = temp;
        }
        //todo 从中位数开始判断
        //双指针分别指向两个数组的头
        if (len % 2 == 0) {
            return (left + right) / 2.0;
        } else {
            return right;
        }
    }

}
