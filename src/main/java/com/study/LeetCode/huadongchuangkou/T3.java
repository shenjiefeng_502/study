package com.study.LeetCode.huadongchuangkou;

import java.util.HashSet;

public class T3 {

    public static void main(String[] args) {
        int pwwkew = lengthOfLongestSubstring("abba");
        System.out.println(pwwkew);
        int abcabcbb = lengthOfLongestSubstring("abcabcde");
        System.out.println(abcabcbb);
    }

    public static int lengthOfLongestSubstring(String s) {
        int[] ints = new int[128];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = -1;
        }
        int maxCount = 0;
        int length = s.length();
        int left = 0;
        int right = 0;
        while (right < length) {
            char c = s.charAt(right);
            if (ints[c] >= 0) {
                left = Math.max(ints[c] + 1, left);
            }
            ints[c] = right;
            maxCount = Math.max(maxCount, right - left + 1);
            right++;
        }
        return maxCount;
    }

}
