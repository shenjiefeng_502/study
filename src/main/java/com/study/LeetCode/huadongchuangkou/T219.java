package com.study.LeetCode.huadongchuangkou;

import java.util.HashMap;

public class T219 {
    public static void main(String[] args) {
        boolean b = containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2);
        System.out.println(b);
    }

    /**
     * 记录下标值,并重排序数据
     * 1 2 3 1
     * 0 1 2 3
     * 1 1 2 3
     * 0 3 1 2
     *
     * @param nums
     * @param k
     * @return
     */
    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey(num) && i - map.get(num) <= k) {
                return true;
            }
            map.put(num, i);
        }
        return false;
    }
}
