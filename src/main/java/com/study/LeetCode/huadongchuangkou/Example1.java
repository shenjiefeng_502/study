package com.study.LeetCode.huadongchuangkou;

import java.util.Arrays;

/**
 * Example 1
 *
 * @author sjf
 * @version 1.0
 * @date 2021-12-21 17:06:30
 */
public class Example1 {
    public static void main(String[] args) {
        boolean b = checkInclusion("ab", "eidbaooo");
    }

    /**
     * Check inclusion
     * 比较s1字符串中是否有s2字符串的字串
     *
     * @param s1 the s 1
     * @param s2 the s 2
     * @return boolean the boolean
     * @author sjf
     * @date 2021-12-21 17:06:49
     */
    public static boolean checkInclusion(String s1, String s2) {
        int n = s1.length(), m = s2.length();
        if (n > m) {
            return false;
        }
        int[] cnt1 = new int[26];
        int[] cnt2 = new int[26];
        //将s1长度的字符串压入数组,设置窗口长度
        for (int i = 0; i < n; ++i) {
            ++cnt1[s1.charAt(i) - 'a'];
            ++cnt2[s2.charAt(i) - 'a'];
        }
        //判断长度是否相等
        if (Arrays.equals(cnt1, cnt2)) {
            return true;
        }
        //滑动窗口
        for (int i = n; i < m; ++i) {
            ++cnt2[s2.charAt(i) - 'a'];
            --cnt2[s2.charAt(i - n) - 'a'];
            if (Arrays.equals(cnt1, cnt2)) {
                return true;
            }
        }
        return false;
    }


}
