package com.study.LeetCode.huadongchuangkou;

import java.util.HashMap;

public class T220 {
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        int l = 0;
        int r = 1;
        while (r < nums.length) {
            //扩充窗口
            if (r - l <= k) {
                r++;
                continue;
            }
            //缩小窗口
            while (l < r) {
                if (Math.abs(nums[l] - nums[r]) <= t) {
                    return true;
                }
                l++;
            }
        }
        return false;
    }

    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey(num) && i - map.get(num) <= k) {
                return true;
            }
            map.put(num, i);
        }
        return false;
    }
}
