package com.study.LeetCode.huadongchuangkou;

import java.util.Arrays;

/**
 * T 567
 * 以后遇到字符串的题目.可以尝试将字符串转换为数组
 * 转换方式
 *
 * @author sjf
 * @version 1.0
 * @date 2021-12-21 17:04:21
 */
public class T567 {
    public static void main(String[] args) {
        boolean b = checkInclusion("ab", "eidbaooo");
        System.out.println(b);
    }

    public static boolean checkInclusion(String s1, String s2) {
        int[] cnt11 = new int[26];
        int[] cnt12 = new int[26];
        int n = s1.length();
        int m = s2.length();
        for (int i = 0; i < n; i++) {
            ++cnt11[s1.charAt(i) - 'a'];
            ++cnt12[s2.charAt(i) - 'a'];
        }
        if (Arrays.equals(cnt11, cnt12)) {
            return true;
        }
        for (int i = n; i < m; i++) {
            ++cnt12[s2.charAt(i) - 'a'];
            --cnt12[s2.charAt(i - n) - 'a'];
            if (Arrays.equals(cnt11, cnt12)) {
                return true;
            }

        }
        return false;
    }
}
