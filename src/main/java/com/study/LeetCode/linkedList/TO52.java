package com.study.LeetCode.linkedList;

public class TO52 {

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }

        ListNode(int x, ListNode next) {
            val = x;
            next = next;
        }
    }

    public class Solution {
        /**
         * 相交链表,给定两个链表,判断是否有相同一段数据
         * headA: [0, 2, 4, 6, 7, 8, 9]
         * headA: [-1, 1, 3, 5, 7, 8, 9]
         *
         * @param headA
         * @param headB
         * @return
         */
        public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
            /**
             * 将两个链表拼接起来,这样两个链表总长度就相等了,且第一个相等的就是相等点
             * [0, 2, 4, 6, 7, 8, 9][-1, 1, 3, 5, 7, 8, 9]
             * [-1, 1, 3, 5, 7, 8, 9][0, 2, 4, 6, 7, 8, 9]
             */
            ListNode a = headA, b = headB;
            //通过cnt控制表尾加入次数
            int cnt = 0;
            while (a != null && b != null) {
                //当两个链表长度相等时,则可以直接得出第一个相等点
                if (a == b) {
                    return a;
                }
                a = a.next;
                b = b.next;
                //判断是否到末尾.如果到末尾则将对方的链表的头部加入
                if (cnt < 2 && a == null) {
                    cnt++;
                    a = headB;
                }
                if (cnt < 2 && b == null) {
                    cnt++;
                    b = headA;
                }
            }
            return null;
        }

        /**
         * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
         */
        public ListNode removeNthFromEnd(ListNode head, int n) {
            /**
             * 通过快慢指针去实现
             * 快慢指针间距n 快指针触底时删除慢指针后一个元素
             */
            if (n < 0) {
                return head;
            }
            if (n == 0) {
                ListNode newHead = head.next;
                head = newHead;
                return newHead;
            }
            ListNode pre = new ListNode(0);
            pre.next = head;
            ListNode fast = pre;
            ListNode slow = pre;
            for (int i = 0; i < n; i++) {
                fast = fast.next;
            }
            while (fast != null) {
                fast = fast.next;
                slow = slow.next;
            }
            ListNode next = slow.next;
            slow.next = next.next;
            return head;
        }
    }

    /**
     * 反转链表
     *
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        /**
         * 使用头插法插入
         */
        ListNode temp = head;
        while (head.next != null) {
            ListNode newHead = head.next;
            head.next = head.next.next;
            newHead.next = temp;
            temp = newHead;
        }
        return temp;
    }

    /**
     * 删除链表中val值与入参相同的节点,返回新的链表表头
     * 使用哑节点.next来保存当前头指针
     *
     * @param head
     * @param val
     * @return
     */
    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return head;
        }

        ListNode pre = new ListNode(1, head);
        ListNode temp = pre;
        while (temp.next != null) {
            ListNode next = temp.next;
            if (next.val == val) {
                temp.next = next.next;
            } else {
                temp = next;
            }
        }
        return pre.next;
    }

    /**
     * 快慢指针
     *
     * @param head the head
     * @return list node the list node
     * @author sjf
     * @date 2021-12-22 13:34:21
     */
    public ListNode oddEvenList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode fast = head.next;
        ListNode slow = head;
        while (fast != null && fast.next != null) {
            ListNode target = fast.next;
            fast.next = fast.next.next;
            fast = fast.next;
            target.next = slow.next;
            slow.next = target;
            slow = slow.next;
        }
        return head;
    }
}
