package com.study.LeetCode.linkedList;

public class T876 {
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    /**
     * 快慢指针
     * 指针快的进两步,慢的进一步,快的到末尾时,慢的正好为中间
     * 1,2,3,4,5,6
     *
     * @param head
     * @return
     */
    public ListNode middleNode(ListNode head) {
        ListNode listNode = new ListNode();
        listNode.next = head;
        ListNode fast = listNode;
        ListNode slow = listNode;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        if (fast != null) {
            slow = slow.next;
        }
        return slow;
    }
}
