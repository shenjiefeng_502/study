package com.study.LeetCode.linkedList;

import java.util.ArrayList;

public class T19 {
    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
//        node.next = new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))));
        removeNthFromEnd(node, 2);
        System.out.println(node.next.val);
    }

    /**
     * 暴力破解法
     *
     * @param head
     * @param n
     * @return
     */
    public static ListNode removeNthFromEnd1(ListNode head, int n) {
        if (head.next == null && n > 0) {
            return null;
        }
        ArrayList<ListNode> nodes = new ArrayList<>();
        ListNode node = head;
        while (node != null) {
            nodes.add(node);
            node = node.next;
        }
        if (n == 1) {
            ListNode listNode = nodes.get(nodes.size() - 2);
            listNode.next = null;
        } else if (n == nodes.size()) {
            head = head.next;
        } else {
            ListNode listNode = nodes.get(nodes.size() - n - 1);
            ListNode nextNode = nodes.get(nodes.size() - n + 1);
            listNode.next = nextNode;
        }
        return head;

    }

    /**
     * 双指针法,左指针指向应该删除的节点前一位,右指针与左指针相隔n,当右指针指向null时,左指针就指向了该删除的位置
     *
     * @param head
     * @param n
     * @return
     */
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode pre = new ListNode(0);
        pre.next = head;
        ListNode left = pre;
        ListNode right = pre;
        //一步步来,先只管控制间距,不判断如果右指针已经到了怎么办
        while (n <= 0) {
            right = right.next;
            n--;
        }
        while (right != null) {
            left = left.next;
            right = right.next;
        }
        left.next = left.next.next;
        return pre.next;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
