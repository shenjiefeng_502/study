package com.study.LeetCode.List;

public class T1991 {
    public int findMiddleIndex(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int leftSum = sum(nums, 0, i - 1);
            int rightSum = sum(nums, i + 1, nums.length - 1);
            if (rightSum == leftSum) {
                return i;
            }
        }
        return -1;
    }


    //2,3,-1,8,4
    private int sum(int[] nums, int start, int end) {
        int sum = 0;
        while (end >= start) {
            sum += nums[start];
            start++;
        }
        return sum;
    }

}
