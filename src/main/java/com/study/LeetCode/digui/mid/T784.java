package com.study.LeetCode.digui.mid;

import java.util.ArrayList;
import java.util.List;

public class T784 {
    public static void main(String[] args) {
        List<String> res = letterCasePermutation("1a2b3c");
        System.out.println(1);
    }

    public static List<String> letterCasePermutation(String s) {
//        StringBuilder temp = new StringBuilder();
//        ArrayList<String> res = new ArrayList<>();
//        recursion(0, s, temp, res);
//        return res;
        List<String> list = new ArrayList<>();
        char[] array = s.toCharArray();
        lett(list, array, 0);
        return list;
    }

    public static void lett(List<String> list, char[] array, int cur) {
        if (cur == array.length) {
            list.add(new String(array));
            return;
        }
        if (array[cur] >= 'a' && array[cur] <= 'z') {
            lett(list, array, cur + 1);
            array[cur] = (char) (array[cur] - 32);
            lett(list, array, cur + 1);

        } else if (array[cur] >= 'A' && array[cur] <= 'Z') {
            lett(list, array, cur + 1);
            array[cur] = (char) (array[cur] + 32);
            lett(list, array, cur + 1);
        } else {
            lett(list, array, cur + 1);
        }
    }

    private static void recursion(int i, String s, StringBuilder temp, ArrayList<String> res) {
        if (temp.length() == s.length()) {
            res.add(new String(temp));
            return;
        }
        for (int index = i; index < s.length(); index++) {
            char c = s.charAt(i);
            temp.append(c);
            recursion(index + 1, s, temp, res);
            temp.deleteCharAt(temp.length() - 1);
            if (Character.isLowerCase(c) || Character.isUpperCase(c)) {
                if (c >= 'A' && c <= 'Z') {
                    c += 32;
                } else {
                    c -= 32;
                }
                temp.append(c);
                recursion(index + 1, s, temp, res);
                temp.deleteCharAt(temp.length() - 1);
            }
        }
    }

}
