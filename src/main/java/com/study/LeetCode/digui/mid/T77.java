package com.study.LeetCode.digui.mid;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

//给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
//
//你可以按 任何顺序 返回答案。
//示例 1：
//
//输入：n = 4, k = 2
//输出：
//[
//  [2,4],
//  [3,4],
//  [2,3],
//  [1,2],
//  [1,3],
//  [1,4],
//]
public class T77 {
    public static void main(String[] args) {
        List<List<Integer>> combine = combine(4, 2);
        System.out.println(1);
    }

    /**
     * Combine
     * 递归回溯,剪枝条件k
     */
    public static List<List<Integer>> combine(int n, int k) {
        // 从 1 开始是题目的设定
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<List<Integer>> res = new ArrayList<>();
        recursion(1, n, k, temp, res);
        return res;
    }

    private static void recursion(int i, int n, int k, ArrayList<Integer> temp, ArrayList<List<Integer>> res) {
        if (temp.size() == k) {
            res.add(new ArrayList<>(temp));
            return;
        }
        for (int index = i; index <= n; index++) {
            temp.add(index);
            recursion(index + 1, n, k, temp, res);
            temp.remove(temp.size() - 1);
        }
    }
}
