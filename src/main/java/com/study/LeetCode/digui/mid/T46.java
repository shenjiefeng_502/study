package com.study.LeetCode.digui.mid;

import java.util.ArrayList;
import java.util.List;

public class T46 {
    public static void main(String[] args) {
        int[] ints = {1, 2, 3};
        List<List<Integer>> res = permute(ints);
        System.out.println(1);
    }

    public static List<List<Integer>> permute(int[] nums) {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<List<Integer>> res = new ArrayList<>();
        boolean[] isV = new boolean[nums.length];
        recursion(isV, nums, temp, res);
        return res;
    }

    private static void recursion(boolean[] isV, int[] nums, ArrayList<Integer> temp, ArrayList<List<Integer>> res) {
        if (nums.length == temp.size()) {
            res.add(new ArrayList<>(temp));
            return;
        }
        for (int index = 0; index < nums.length; index++) {
            if (isV[index]) {
                continue;
            }
            temp.add(nums[index]);
            isV[index] = true;
            recursion(isV, nums, temp, res);
            isV[index] = false;
            temp.remove(temp.size() - 1);
        }
    }
}
