package com.study.LeetCode.digui.mid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/26  10:29
 * @description: 四数之和
 */

/**
 * 无重复四数之和
 * 数组重排序通过下标来判断是否重复
 * 递归回溯
 */
public class T18 {
    public static void main(String[] args) {
        List<List<Integer>> lists = fourSum(new int[]{1, 0, -1, 0, -2, 2}, 0);
        System.out.println(1);
    }

    public static List<List<Integer>> fourSum(int[] nums, int target) {
        ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
        if (nums.length < 4) {
            return res;
        }
        Arrays.sort(nums);
        ArrayList<Integer> temp = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            recursion(i, target, nums, temp, res);
        }
        return res;
    }

    private static void recursion(int index, int target, int[] nums, ArrayList<Integer> temp, ArrayList<List<Integer>> res) {
        if (target == 0 && temp.size() == 4) {
            System.out.println("-----");
            for (Integer integer : temp) {
                System.out.println(integer);
            }
            System.out.println("-----");
            res.add(new ArrayList<>(temp));
            return;
        }
        if (temp.size() == 4) {
            System.out.println("-----");
            for (Integer integer : temp) {
                System.out.println(integer);
            }
            System.out.println("-----");
            return;
        }
        for (int i = index; i < nums.length; i++) {
            target -= nums[index];
            temp.add(nums[index]);
            recursion(i + 1, target, nums, temp, res);
            target += nums[index];
            temp.remove(temp.size() - 1);
        }
    }
}
