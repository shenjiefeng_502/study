package com.study.LeetCode.digui.easy;
//给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。

//输入：head = [1,2]
//输出：[2,1]
public class T206 {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        ListNode list1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode head = reverseList(list1);
        System.out.println(head.val);
    }

    /**
     * 递归关系直到最后
     * 递归过程中将当前节点和下一个节点的连接关系反一下,继续递归下个节点
     *
     * @param head
     * @return
     */
    public static ListNode reverseList(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode next = head.next;
        head.next = null;
        return recursion(head, next);
    }

    private static ListNode recursion(ListNode head, ListNode next) {
        if (next == null) {
            return head;
        } else {
            ListNode temp = next.next;
            next.next = head;
            return recursion(next, temp);
        }
    }
}
