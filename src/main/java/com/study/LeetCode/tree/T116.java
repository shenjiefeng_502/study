package com.study.LeetCode.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * T 116
 *
 * @author sjf
 * @version 1.0
 * @date 2021-12-23 17:52:30
 */
public class T116 {
    static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        Node() {
        }

        Node(int _val) {
            val = _val;
        }

        Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    }

    public static void main(String[] args) {
        Node left = new Node(2);
        Node right = new Node(3);
        left.left = new Node(4);
        left.right = new Node(5);
        right.left = new Node(6);
        right.right = new Node(7);
        Node node = new Node(1, left, right, null);
        Node connect = connect(node);
    }

    /**
     * 递归传入两个节点,l.l->l.r->r.l->r.r->null
     *
     * @param root the root
     * @return node the node
     * @author sjf
     * @date 2021-12-23 17:53:53
     */
    public static Node connect(Node root) {
        if (root == null) {
            return null;
        }
        bfs(root);
//        dfs(root.left, root.right);
        return root;
    }

    private static void bfs(Node root) {
        Queue<Node> nodes = new LinkedList<Node>();
        nodes.add(root);
        while (!nodes.isEmpty()) {
            int size = nodes.size();
            Node temp = null;
            for (int i = 0; i < size; i++) {
                Node first = nodes.poll();
                if (temp != null) {
                    temp.next = first;
                }
                if (first.left != null) {
                    nodes.add(first.left);
                }
                if (first.right != null) {
                    nodes.add(first.right);
                }
                temp = first;
            }
            temp = null;
        }

    }

    private static void dfs(Node left, Node right) {
        if (left != null) {
            left.next = right;
            dfs(left.left, left.right);
        }
        if (left != null && right != null) {
            dfs(left.right, right.left);
        }
        if (right != null) {
            dfs(right.left, right.right);
            dfs(right, null);
        }
    }
}
