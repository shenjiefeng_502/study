package com.study.LeetCode.tree;

public class T617 {
    public static void main(String[] args) {
        TreeNode root2 = new TreeNode(1);
        mergeTrees(null, root2);
    }

    private static TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        TreeNode node = new TreeNode(root1.val + root2.val);
        node.left = mergeTrees(root1.left, root2.left);
        node.right = mergeTrees(root1.right, root2.right);
        return node;
    }

    //todo 可优化 不先new,直接引用原来的树节点
    public static TreeNode mergeTrees1(TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null) {
            return null;
        }
        TreeNode node = new TreeNode();
        dfs(node, root1, root2);
        return node;
    }

    private static void dfs(TreeNode node, TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null) {
            return;
        }
        if (root1 == null) {
            node.val = root2.val;
            //当两个树节点的左节点都为null时则不继续递归
            if (root2.left != null) {
                TreeNode left = new TreeNode();
                node.left = left;
                dfs(node.left, root1, root2.left);
            }
            //当两个树节点的右节点都为null时则不继续递归
            if (root2.right != null) {
                TreeNode right = new TreeNode();
                node.right = right;
                dfs(node.right, root1, root2.right);
            }
        } else if (root2 == null) {
            node.val = root1.val;
            //当两个树节点的左节点都为null时则不继续递归
            if (root1.left != null) {
                TreeNode left = new TreeNode();
                node.left = left;
                dfs(node.left, root1.left, root2);
            }
            //当两个树节点的右节点都为null时则不继续递归
            if (root1.right != null) {
                TreeNode right = new TreeNode();
                node.right = right;
                dfs(node.right, root1.right, root2);
            }
        } else {
            node.val = root1.val + root2.val;
            //当两个树节点的左节点都为null时则不继续递归
            if (root1.left != null || root2.left != null) {
                TreeNode left = new TreeNode();
                node.left = left;
                dfs(node.left, root1.left, root2.left);
            }
            //当两个树节点的右节点都为null时则不继续递归
            if (root1.right != null || root2.right != null) {
                TreeNode right = new TreeNode();
                node.right = right;
                dfs(node.right, root1.right, root2.right);
            }
        }

    }
}
