package com.study.LeetCode.erfengchazhao;

public class T35 {
    public static void main(String[] args) {
        int i = searchInsert(new int[]{1, 3, 5, 6}, 7);
        System.out.println(i);
    }

    /**
     * Search insert
     * 当数据存在时,则返回位置,当数据不存在时则返回插入位置,既比左端点大1的位置
     *
     * @param nums   the nums
     * @param target the target
     * @return int the int
     * @author sjf
     * @date 2021-12-16 13:34:57
     */
    public static int searchInsert(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        int mid = 0;
        while (l <= r) {
            mid = l + (r - l) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            //todo 微调保证端点正确
            if (nums[mid] < target) {
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }
        return l;
    }
}
