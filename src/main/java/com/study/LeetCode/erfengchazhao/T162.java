package com.study.LeetCode.erfengchazhao;

public class T162 {
    public static void main(String[] args) {
        int peakElement = findPeakElement(new int[]{2, 1});
        System.out.println(peakElement);
    }

    public static int findPeakElement(int[] nums) {
        int l = 0, r = nums.length - 1;
        while (l < r) {
            int mid = (l + r) / 2;
            if (nums[mid] < nums[mid + 1]) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }
        return l;
    }
}
