package com.study.LeetCode.erfengchazhao;
//编写一个高效的算法来判断 m x n 矩阵中，是否存在一个目标值。该矩阵具有如下特性：
//
// 每行中的整数从左到右按升序排列。
// 每行的第一个整数大于前一行的最后一个整数。

/**
 * 直接length=m*n
 * mid就等于 除数加余数
 */
public class T74 {
    public static void main(String[] args) {
        int[][] ints = {{1, 1}};
        boolean b = searchMatrix(ints, 3);
        System.out.println(b);
    }

    public static boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        int len = m * n - 1;
        int l = 0;
        int r = len;
        int mid = 0;
        while (l < r) {
            //todo 了解为什么不用 (l+r) / 2
            //0-11
            //l+r : 5 2/8
            //r-l : 5 2/
            mid = (r - l) / 2 + l;
            int tarM = (mid / n);
            int tarN = (mid % n);
            int x = matrix[tarM][tarN];
            if (x == target) {
                return true;
            }
            if (x > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return false;
    }
}
