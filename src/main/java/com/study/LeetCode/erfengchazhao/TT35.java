package com.study.LeetCode.erfengchazhao;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/6  10:06
 * @description:
 */
public class TT35 {

    public static void main(String[] args) {
        int[] nums = new int[]{1, 3, 5, 6};
        int i = searchInsert(nums, 7);
        System.out.println(i);
    }

    /**
     * 查找存在位置,如果找不到则返回该插入的位置
     *
     * @param nums
     * @param target
     * @return int
     * @MethodName: searchInsert
     * @auth: sjf
     * @date 2022/5/6 10:07
     */
    public static int searchInsert(int[] nums, int target) {
        //先实现一个二分查找
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int mid = ((r + l) / 2);
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return l;

    }
}
