package com.study.LeetCode.erfengchazhao;

public class T704 {
    public static void main(String[] args) {
        int[] ints = {-1, 0, 3, 5, 9, 12};
        int search = search(ints, 2);
    }

    /**
     * 二分查找简单版
     *
     * @param nums
     * @param target
     * @return
     */
    public static int search(int[] nums, int target) {
        int i = 0;
        int j = nums.length - 1;
        int mid = 0;
        while (i <= j) {
            mid = i + (j - i) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            //未查找到时必须把指针往前移动一次,否则容易死循环
            if (nums[mid] > target) {
                j = mid - 1;
            } else {
                i = mid + 1;
            }
        }
        return -1;
    }
}
