package com.study.LeetCode.erfengchazhao;

import java.util.Arrays;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/5  14:46
 * @description:
 */
public class TT704 {
    public static int search(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] nums= new int[]{-1,0,3,5,9,12};
        int search = search(nums, 9);
        System.out.println(search);
        
    }
}
