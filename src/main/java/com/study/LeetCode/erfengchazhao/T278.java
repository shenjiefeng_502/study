package com.study.LeetCode.erfengchazhao;

public class T278 {
    /**
     * 调用api来确定是否是有bug的版本,找到最早一个
     *
     * @param args
     */
    public static void main(String[] args) {
        firstBadVersionV2(5);
    }

    public static int firstBadVersionV1(int n) {
        int l = 1;
        int r = n;
        int mid = 1;
        while (l <= r) {
            mid = l + (r - l) / 2;
            if (!isBadVersion(mid - 1) && isBadVersion(mid)) {
                return mid;
            }
            if (isBadVersion(mid)) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return mid;
    }

    public static int firstBadVersionV2(int n) {
        int l = 1;
        int r = n;
        int mid = 1;
        //当左端点小于右端点时
        while (l < r) {
            mid = l + (r - l) / 2;
            /**
             * 不用mid来判断,而是通过把二分查找坍缩到一个点来确定
             */
            if (isBadVersion(mid)) {
                r = mid;
            } else {
                l = mid + 1;
            }
        }
        return l;
    }

    public static boolean isBadVersion(int version) {
        return 4 <= version;
    }
}
