package com.study.LeetCode.erfengchazhao;

//输入：nums = [5,7,7,8,8,10], target = 8
//输出：[3,4]
//
// 示例 2：
//
//
//输入：nums = [5,7,7,8,8,10], target = 6
//输出：[-1,-1]
public class T34 {
    public static void main(String[] args) {
        int[] ints = searchRange(new int[]{5,7,7,8,8,10}, 8);
        System.out.println(ints[0]);
        System.out.println(ints[1]);
    }

    /**
     * Search range
     * 二分查找查找到以后往前往后推
     *
     * @param nums   the nums
     * @param target the target
     * @return int[ ] the int [ ]
     * @author sjf
     * @date 2021-12-31 09:43:20
     */
    public static int[] searchRange(int[] nums, int target) {
        int leftIdx = binarySearch(nums, target, true);
        int rightIdx = binarySearch(nums, target, false) - 1;
        if (leftIdx <= rightIdx && rightIdx < nums.length && nums[leftIdx] == target && nums[rightIdx] == target) {
            return new int[]{leftIdx, rightIdx};
        }
        return new int[]{-1, -1};
    }

    public static int binarySearch(int[] nums, int target, boolean lower) {
        int left = 0, right = nums.length - 1, ans = nums.length;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] > target || (lower && nums[mid] >= target)) {
                right = mid - 1;
                ans = mid;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }

}
