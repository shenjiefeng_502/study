package com.study.LeetCode.erfengchazhao;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/6  11:02
 * @description:
 */
public class TT852 {
    public static void main(String[] args) {
        int[] nums = new int[]{0, 3, 1, 0};
        int easy = easy(nums);
        System.out.println(easy);
    }

    /**
     * 题目描述有且只有一个最大值,可以简单的理解为查找最大数
     *
     * @param arr
     * @return int
     * @MethodName: peakIndexInMountainArray
     * @auth: sjf
     * @date 2022/5/6 11:06
     */
    public static int peakIndexInMountainArray(int[] arr) {
        int easy = ecz(arr);
        System.out.println(easy);
        return ecz(arr);
    }

    /**
     * 二分查找实现查找最大数
     * 由于是山峰,所以最大数左边小于target,最大数右边小于target
     * 由此可得mid的判断公式
     * if ( mid > mid - 1 && mid > mid + 1 )
     *
     * @param arr
     * @return int
     * @MethodName: ecz
     * @auth: sjf
     * @date 2022/5/6 11:05
     */
    private static int ecz(int[] arr) {
        int l = 1, r = arr.length - 2;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (arr[mid] > arr[mid - 1] && arr[mid] < arr[mid + 1]) {
                mid = r + 1;
            } else if (arr[mid] < arr[mid - 1] && arr[mid] > arr[mid + 1]) {
                mid = r - 1;
            } else {
                return mid;
            }
        }
        return l;
    }

    private static int easy(int[] arr) {
        int target = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[target]< arr[i]) {
                target = i;
            }
        }
        return target;
    }
}
