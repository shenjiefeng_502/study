package com.study.LeetCode.erfengchazhao;

public class T33 {
    public static void main(String[] args) {
        int[] ints = {5, 1, 3};
        int search = search(ints, 3);
        System.out.println(search);
    }

    /**
     * 二分数组,数组左侧和右侧肯定有一侧是有序的,
     * <p>
     * 如果 [l, mid - 1] 是有序数组，
     * 且 target 的大小满足 [\textit{nums}[l],\textit{nums}[mid])[nums[l],nums[mid])，
     * 则我们应该将搜索范围缩小至 [l, mid - 1]，
     * 否则在 [mid + 1, r] 中寻找。
     * <p>
     * 如果 [mid, r] 是有序数组，
     * 且 target 的大小满足 (\textit{nums}[mid+1],\textit{nums}[r]](nums[mid+1],nums[r]]，
     * 则我们应该将搜索范围缩小至 [mid + 1, r]，
     * 否则在 [l, mid - 1] 中寻找。
     *
     * @param nums
     * @param target
     * @return
     */
    public static int search(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int mid = 0;
        int l = 0;
        int r = n - 1;
        while (l <= r) {
            mid = (l + r) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            //左边到中间有序时
            if (nums[mid] >= nums[0]) {
                //判断数字是否在左边到中间
                if (nums[0] <= target && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
                //否则判断中间到右边
            } else {
                if (nums[mid] < target && target <= nums[n - 1]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return nums[mid] == target ? mid : -1;
    }
}
