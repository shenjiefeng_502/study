package com.study.LeetCode.easy;

import java.util.Arrays;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/20  20:29
 * @description:
 */
public class T2828 {
    public static void main(String[] args) {
        boolean acronym = isAcronym(Arrays.asList("av", "bv", "cv"), "abc");
        System.out.println(acronym);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    public static boolean isAcronym(List<String> words, String s) {
        if (words == null || words.size() == 0 || s == null || s.length() == 0 || s.length() != words.size()) {
            return false;
        }
        char[] charArray = s.toCharArray();
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).charAt(0) != s.charAt(0)){
                return false;
            }
        }
        return true;
    }
//leetcode submit region end(Prohibit modification and deletion)

}
