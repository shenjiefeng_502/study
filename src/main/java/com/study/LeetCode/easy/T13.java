package com.study.LeetCode.easy;

import java.util.HashMap;

/**
 * 罗马转整数
 *
 * @author sjf
 * @version 1.0
 * @date 2022-01-11 13:43:52
 */
//I， V， X， L，C，D 和 M。
// I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
//字符          数值
//I             1
//V             5
//X             10
//L             50
//C             100
//D             500
//M             1000
// X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
// C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
public class T13 {
    public static void main(String[] args) {
        int iii = romanToInt("III");
        int i2 = romanToInt("IV");
        System.out.println(iii);
        System.out.println(i2);
        int i = romanToInt("LVIII");
        System.out.println(i);

    }

    /**
     * 罗马转整数
     * 最右边数为正数
     * 比较左边数与右边数的关系
     * 如果大为正,小为负
     *
     * @param s the s
     * @return int the int
     * @author sjf
     * @date 2022-01-11 13:44:08
     */
    public static int romanToInt(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        char[] chars = s.toCharArray();
        char last = s.charAt(s.length() - 1);
        int num = 0;
        num = map.get(last);
        for (int i = chars.length - 2; i >= 0; i--) {
            Integer nowRoman = map.get(last);
            Integer newRoman = map.get(chars[i]);
            if (nowRoman > newRoman) {
                num -= newRoman;
            } else {
                num += newRoman;
            }
            last = chars[i];
        }
        return num;
    }

    private static int getNum(Integer nowRoman, Integer newRoman, int num) {

        return num;
    }

}
