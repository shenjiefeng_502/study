package com.study.LeetCode.easy;


import java.util.Arrays;

public class T189 {
    /**
     * 1,2,3,4,5,6,7
     * 7,1,2,3,4,5,6
     * 6,7,1,2,3,4,5
     * 5,6,7,1,2,3,4
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7};
        rotate(nums, 3);
        System.out.println(nums);
    }

    /**
     * 思路:
     * 将整个数组往后移动的话效率太低了
     * 空间复杂度O(1)原地变动
     * 数组翻转法
     *
     * @param nums
     * @param k
     */
    private static void rotate(int[] nums, int k) {
        int n = nums.length;
        //可能存在k>n的情况,根据余数来判断分界位置
        k %= n;
        reversal(nums, 0, n - 1);
        reversal(nums, 0, k - 1);
        reversal(nums, k, n - 1);
    }

    private static void reversal(int[] nums, int startIndex, int endIndex) {
        int temp = 0;
        while (startIndex < endIndex) {
            temp = nums[startIndex];
            nums[startIndex] = nums[endIndex];
            nums[endIndex] = temp;
            startIndex++;
            endIndex--;
        }
    }

    /**
     * 会超时,可优化
     *
     * @param nums
     * @param k
     */
    public static void rotateLow(int[] nums, int k) {
        int temp = 0;
        for (int i = 0; i < k; i++) {
            for (int j = nums.length - 1; j > 0; j--) {
                temp = nums[j - 1];
                nums[j - 1] = nums[j];
                nums[j] = temp;
            }
        }
    }

}
