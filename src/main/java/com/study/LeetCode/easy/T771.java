package com.study.LeetCode.easy;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/20  20:37
 * @description:
 */
public class T771 {
    public int numJewelsInStones(String jewels, String stones) {
        int count = 0;
        char[] charArray = stones.toCharArray();
        for (int i = 0; i < stones.length(); i++) {
            if (jewels.contains(String.valueOf(charArray[i]))) {
                count++;
            }
        }
        return count;
    }
}
