package com.study.LeetCode.easy;

public class T167 {
    public static void main(String[] args) {
//        int[] ints = twoSum(new int[]{2, 3, 4}, 6);
        int[] ints = twoSum(new int[]{2, 7, 11, 15}, 9);
    }

    public static int[] twoSum(int[] numbers, int target) {
        int i = 0;
        int j = numbers.length - 1;
        while (i < j) {
            int sum = numbers[i] + numbers[j];
            if (sum < target) {
                i++;
            } else if (sum > target) {
                j--;
            } else {
                return new int[]{i + 1, j + 1};
            }
        }
//        return new int[]{-1, -1};
        //双指针
        //左指针指向最左侧,
        //右指针指向最右侧
        //如果相加小于目标直接剪枝
        int l = 0, r = numbers.length - 1;
        while (l < r) {
            if (r == 0) {
                r = numbers.length - 1;
                l++;
            }
            if (numbers[l] + numbers[r] < target) {
                l++;
                r = numbers.length - 1;
                continue;
            }
            if (numbers[l] + numbers[r] > target) {
                r--;
            }
            if (numbers[l] + numbers[r] == target) {
                return new int[]{++l, ++r};
            }
        }
        return null;
    }
}
