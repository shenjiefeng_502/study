package com.study.LeetCode.easy;

import java.util.ArrayList;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/9  16:21
 * @description: 增减字符串匹配
 */
public class T942 {
    public static void main(String[] args) {
        int[] ints = diStringMatch("IDID");
        System.out.println("");
    }

    public static int[] diStringMatch(String s) {
        int length = s.length() + 1;
        int[] ints = new int[length];
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(i);
        }
        for (int i = 0; i < s.length(); i++) {
            int index = 0;
            if (s.charAt(i) == 'I') {
                index = 0;
                ints[i] = list.get(index);
            } else {
                index = list.size() - 1;
                ints[i] = list.get(index);
            }
            list.remove(index);
        }
        ints[ints.length - 1] = list.get(0);
        return ints;
    }
}
