package com.study.LeetCode.easy;


public class T66 {
    public static void main(String[] args) {
        int[] int1 = {1, 2, 3};
        int[] int2 = {9, 1, 9};
        int[] int3 = {1, 9, 9, 9};
//        int[] ints1 = plusOne(int1);
        int[] ints2 = plusOne(int2);
        int[] ints3 = plusOne(int3);
        System.out.println(1);
    }

    public static int[] plusOne(int[] digits) {
        int count = 0;
        int len = digits.length;
        for (int i = len - 1; i >= 0; i--) {
            if (digits[i] == 9) {
                count++;
            } else {
                break;
            }
        }
        if (len == count) {
            int[] ints = new int[count + 1];
            ints[0] = 1;
            return ints;
        } else if (count == 0) {
            digits[len - 1]++;
            return digits;
        }
        for (int i = 0; i < count; i++) {
            digits[len - 1 - i] = 0;
        }
        digits[len - count - 1]++;
        return digits;
    }
}
