package com.study.LeetCode.easy;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/1/25  14:37
 * @description: 比赛中的配对次数
 */
public class T1688 {
    public static void main(String[] args) {
//        int i1 = numberOfMatches(14);
        int i1 = numberOfMatches(7);
        System.out.println(i1);
    }

    /**
     * @param n
     * @return int
     * @MethodName: numberOfMatches
     * @auth: sjf
     * @date 2022/1/25 14:38
     */
    public static int numberOfMatches(int n) {
        if (n < 2) {
            return 0;
        }
        int count = 0;
        boolean sin = false;
        //7
        while (n >= 2) {
            //当为奇数时,匹配次数向下取2的倍数
            sin = n % 2 == 1;
            //当有人轮空时,count应该减1,总队伍数保持2*
            n /= 2;
            //当为奇数时,队伍数向上取2的倍数
            count += n;
            if (sin) {
                n += 1;
            }
        }
        return count;
    }
}
