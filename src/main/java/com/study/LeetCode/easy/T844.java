package com.study.LeetCode.easy;

import org.apache.poi.ss.formula.functions.T;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/5  15:48
 * @description:
 */
public class T844 {
    public static void main(String[] args) {
//        boolean b = backspaceCompare("ab#c", "ad#c");
        boolean b1 = backspaceCompare("ab##", "a#c#");
        boolean b2 = backspaceCompare("a#c", "b");
//        System.out.println(b);
        System.out.println(b1);
        System.out.println(b2);

    }

    public static boolean backspaceCompare(String s, String t) {
        int l = s.length() - 1, r = t.length() - 1;
        int ld = 0;
        int rd = 0;
        while (l >= 0 || r >= 0) {
            while (l >= 0) {
                if (s.charAt(l) == '#') {
                    ld++;
                    l--;
                } else if (ld > 0) {
                    ld--;
                    l--;
                } else {
                    break;
                }
            }
            while (r >= 0) {
                if (t.charAt(r) == '#') {
                    rd++;
                    r--;
                } else if (rd > 0) {
                    r--;
                    rd--;
                }else {
                    break;
                }
            }
            //todo 当一个数组无数据,另一个数组还有可能是空
            //todo 判断不同就返回为false
            if (l >= 0 && r >= 0) {
                if (s.charAt(l) != t.charAt(r)) {
                    return false;
                }
            } else {
                if (l >= 0 || r >= 0) {
                    return false;
                }
            }
            l--;
            r--;
        }
        return true;
    }
}
