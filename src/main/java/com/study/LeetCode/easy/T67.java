package com.study.LeetCode.easy;

import java.math.BigInteger;

public class T67 {
    public static void main(String[] args) {
        String s = addBinary("11", "1");
        String s1 = addBinary("1010", "1011");
        System.out.println(s);
        System.out.println(s1);
    }

    public static String addBinary(String a, String b) {
        BigInteger i = new BigInteger(a, 2);
        i = i.add(new BigInteger(b, 2));
        return i.toString(2);
    }
}
