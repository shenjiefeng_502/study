package com.study.LeetCode.easy;

import java.util.HashMap;

//字符          数值
//I             1
//V             5
//X             10
//L             50
//C             100
//D             500
//M             1000
public class T12 {
    public static void main(String[] args) {
        int i = 1923;
    }

    /**
     * Int to roman整数转为罗马
     * 从第一位开始,判断
     *
     * @param num the num
     * @return string the string
     * @author sjf
     * @date 2022-01-11 14:05:33
     */
    public String intToRoman(int num) {
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        StringBuffer roman = new StringBuffer();
        for (int i = 0; i < values.length; ++i) {
            int value = values[i];
            String symbol = symbols[i];
            while (num >= value) {
                num -= value;
                roman.append(symbol);
            }
            if (num == 0) {
                break;
            }
        }
        return roman.toString();
    }
}
