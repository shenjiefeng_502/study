package com.study.LeetCode.easy;

public class T344 {
    /**
     * Reverse string
     * t167中有一种方法就是逆转数组的
     *
     * @param s the s
     * @author sjf
     * @date 2021-12-21 09:24:15
     */
    public void reverseString(char[] s) {
        reversal(s, 0, s.length - 1);


    }
    public String reverseWords(String s) {
        char[] chars = s.toCharArray();
        reversal(chars,0,chars.length-1);
        return new String(chars);
    }

    private static void reversal(char[] nums, int startIndex, int endIndex) {
        char temp = 0;
        while (startIndex < endIndex) {
            temp = nums[startIndex];
            nums[startIndex] = nums[endIndex];
            nums[endIndex] = temp;
            startIndex++;
            endIndex--;
        }
    }
}
