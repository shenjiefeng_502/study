package com.study.LeetCode.tanxin;

public class T55 {
    public static void main(String[] args) {
        int[] ints = {0};
        boolean b = canJump(ints);
        System.out.println(b);
    }

    /**
     * 定义一个当前可到达的最远值,当最远值大于数组长度时retur true
     * while true 实现
     * 条件,当前index小于最远可到达的index或已到达
     *
     * @param nums
     * @return
     */
    public static boolean canJump(int[] nums) {
        int length = nums.length-1;
        if (nums.length == 0) {
            return false;
        }
        int farIndex = nums[0];
        int nowIndex = 0;
        while (farIndex >= nowIndex) {
            if (nowIndex >= length) {
                return true;
            }
            int newFarIndex = (nowIndex + nums[nowIndex]);
            farIndex = farIndex < newFarIndex ? newFarIndex : farIndex;
            nowIndex++;
        }
        return false;
    }
}
