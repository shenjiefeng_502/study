package com.study.LeetCode.tanxin;

public class T45 {
    public static void main(String[] args) {
        int[] ints = {2, 3, 0, 1, 4};
        int jump = lcJump(ints);
        System.out.println(jump);
    }

    /**
     * 跟t45差不多,只需要加上一个统计跳跃次数的字段,判断跳跃最小值
     * 判断跳的最远的,不是跳跃距离最大的
     *
     * @param nums
     * @return
     */
    public static int jump(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        //当前位置
        int nowIndex = 0;
        //可跳跃数
        int jumpNum = nums[nowIndex];
        //总跳跃数
        int jumpCount = 0;
        //需要跳跃的位置
        int length = nums.length;
        //当前可跳跃数中最大的值下标位置
        int maxJumpNum = 0;
        //当前位置不是终点时
        while (nowIndex <= length - 1) {
            for (int i = nowIndex + 1; i <= jumpNum; i++) {
                int nowFar = maxJumpNum + nums[maxJumpNum];
                int newFar = i + nums[i];
                maxJumpNum = nowFar < newFar ? i : maxJumpNum;
            }
            nowIndex = maxJumpNum;
            jumpCount++;
            jumpNum = nums[maxJumpNum];
        }
        return jumpCount;
    }

    public static int lcJump(int[] nums) {
        int length = nums.length;
        int end = 0;
        int maxPosition = 0;
        int steps = 0;
        for (int i = 0; i < length - 1; i++) {
            maxPosition = Math.max(maxPosition, i + nums[i]);
            if (i == end) {
                end = maxPosition;
                steps++;
            }
        }
        return steps;
    }

}
