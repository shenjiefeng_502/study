package com.study.collection.set.TreeSet;

import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @program: ListStudyDemo
 * @ClassName SjfTreeSet
 * @description:
 * @author: sjf
 * @create: 2020-05-18 14:58
 * @Version 1.0
 **/
//TODO 试着重写TreeSet
public class SjfTreeSet<E> extends TreeSet<E>
        implements NavigableSet<E>, Cloneable, java.io.Serializable {
    private static final long serialVersionUID = -2414906341926166627L;
    private transient NavigableMap<E, Object> m;
    private static final Object PRESENT = new Object();

    SjfTreeSet(NavigableMap<E, Object> m) {
        this.m = m;
    }

    public SjfTreeSet() {
        this(new TreeMap<E, Object>());
    }

    @Override
    public boolean add(E e) {
        return m.put(e, PRESENT) == null;
    }
    @Override
    public E first() {
        return m.firstKey();
    }
}
