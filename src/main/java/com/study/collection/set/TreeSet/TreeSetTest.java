package com.study.collection.set.TreeSet;

import java.util.TreeSet;

/**
 * @program: ListStudyDemo
 * @ClassName TreeSetTest
 * @description:
 * @author: sjf
 * @create: 2020-05-18 14:55
 * @Version 1.0
 **/
public class TreeSetTest {
    public static void main(String[] args) {
        TreeSet<Integer> integerTreeSet = new TreeSet<>();
        integerTreeSet.add(1);
        TreeSet<Integer> integers = new SjfTreeSet<>();
        integers.add(1);
        //调用TreeMap的FirstKey方法（递归查询最左节点）
        Integer first = integers.first();
    }
}
