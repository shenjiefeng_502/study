package com.study.collection.set.hashset;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: ListStudyDemo
 * @ClassName HashSetTest
 * @description:
 * @author: sjf
 * @create: 2020-05-15 16:15
 * @Version 1.0
 **/
public class HashSetTest {
    public static void main(String[] args) {
        Set<Integer> hashSet = new HashSet<>();
        SjfHashSet<Integer> sjfHashSet = new SjfHashSet<>();
        hashSet.add(1);
        sjfHashSet.add(1);
        System.out.println(hashSet.size());
        System.out.println(sjfHashSet.size());
        hashSet.remove(1);
        sjfHashSet.remove(1);
        System.out.println(hashSet.size());
        System.out.println(sjfHashSet.size());
        System.out.println(hashSet.contains(1));
        System.out.println(sjfHashSet.contains(1));
        sjfHashSet.clear();
        Object clone = sjfHashSet.clone();
    }
}
