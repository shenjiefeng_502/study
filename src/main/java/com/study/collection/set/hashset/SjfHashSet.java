package com.study.collection.set.hashset;


import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Sjf hash set
 *
 * @param <E> the type parameter
 * @author sjf
 * @version 1.0
 * @date 2022-01-20 14:27:31
 * @program: ListStudyDemo
 * @ClassName SjfHashSet
 * @description:
 * @author: sjf
 * @create: 2020-05-15 16:15
 * @Version 1.0
 */
public class SjfHashSet<E>
        extends AbstractSet<E>
        implements Set<E>, Cloneable, java.io.Serializable {
    static final long serialVersionUID = -5024744406713321676L;
    //底层是map,通过map的key来操作
    /**
     * transient:将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会被序列化。
     * hashmap通过writeObject和readObject来实现读写到磁盘
     */
    private transient HashMap<E, Object> map;

    // Dummy value to associate with an Object in the backing Map
    private static final Object PRESENT = new Object();

    /**
     * Constructs a new, empty set; the backing <tt>HashMap</tt> instance has
     * default initial capacity (16) and load factor (0.75).
     */
    public SjfHashSet() {
        map = new HashMap<>();
    }

    /**
     * add
     * 返回e是否已存在
     * 通过map返回值是否等于null来判断是否已存在
     *
     * @param e
     * @return
     */
    @Override
    public boolean add(E e) {
        return map.put(e, PRESENT) == null;
    }

    /**
     * 通过判断map返回的是否等于PRESENT判断是否已存在
     * @param e
     * @return
     */
    @Override
    public boolean remove(Object e) {
        return map.remove(e) == PRESENT;
    }

    /**
     * 通过判断key是否存在
     * @param o
     * @return
     */
    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }
    @Override
    public Object clone() {
        try {
            SjfHashSet<E> newSet = (SjfHashSet<E>) super.clone();
            newSet.map = (HashMap<E, Object>) map.clone();
            return newSet;
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
    }
    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public int size() {
        return map.size();
    }

}
