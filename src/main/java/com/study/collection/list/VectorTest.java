package com.study.collection.list;

import java.util.Vector;

/**
 * @program: ListStudyDemo
 * @ClassName VectorTest
 * @description:
 * @author: sjf
 * @create: 2020-05-12 09:20
 * @Version 1.0
 **/
public class VectorTest {
    public static void main(String[] args) {
        Vector<Integer> integers = new Vector<>();
        integers.add(1);
        SjfVector<Integer> sjfVector = new SjfVector<>();
        sjfVector.add(1);
    }
}
