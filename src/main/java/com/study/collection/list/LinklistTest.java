package com.study.collection.list;

import java.util.LinkedList;

/**
 * @program: ListStudyDemo
 * @ClassName LinklistTest
 * @description:
 * @author: sjf
 * @create: 2020-05-12 10:51
 * @Version 1.0
 **/
public class LinklistTest {
    public static void main(String[] args) {
        LinkedList<Integer> integers = new LinkedList<>();
        integers.add(1);
        integers.addFirst(1);
        integers.add(1,1);
        integers.remove(1);
        integers.remove();
    }
}
