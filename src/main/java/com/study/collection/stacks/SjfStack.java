package com.study.collection.stacks;

import java.util.EmptyStackException;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/3/16  15:25
 * @description: 栈实现类, (专注于几个经典概念, 不具体实现动态扩容)
 */
public class SjfStack<T> {
    private int pos = -1;
    private int maxSize;
    private T[] values;

    public SjfStack(int maxSize) {
        this.maxSize = maxSize;
        values = (T[]) new Object[maxSize];
    }

    public SjfStack() {
        values = (T[]) new Object[1000];
    }

    /**
     * 入栈
     *
     * @param v
     * @return void
     * @MethodName: push
     * @auth: sjf
     * @date 2022/3/16 15:33
     */
    public void push(T v) {
        values[++pos] = v;
    }

    /**
     * 出栈
     *
     * @param
     * @return T
     * @MethodName: pop
     * @auth: sjf
     * @date 2022/3/16 15:33
     */
    public T pop() {
        return values[pos--];
    }

    /**
     * 栈总数
     *
     * @param
     * @return int
     * @MethodName: size
     * @auth: sjf
     * @date 2022/3/16 15:34
     */
    public int size() {
        return pos + 1;
    }

    /**
     * 查看第一个元素
     *
     * @param
     * @return T
     * @MethodName: peek
     * @auth: sjf
     * @date 2022/3/16 16:05
     */
    public synchronized T peek() {
        if (maxSize == -1) {
            throw new EmptyStackException();
        }
        return values[pos];
    }
}
