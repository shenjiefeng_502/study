package com.study.collection.stacks;

import java.util.Stack;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/3/16  15:38
 * @description: 栈测试
 */
public class StackTest {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        SjfStack<Integer> sjfStack = new SjfStack<>();
        stack.push(1);
        sjfStack.push(1);
        System.out.println(stack.peek());
        System.out.println(sjfStack.peek());
        System.out.println(stack.size());
        System.out.println(sjfStack.size());
        System.out.println(sjfStack.pop());
        System.out.println(stack.pop());

    }
}
