package com.study.collection.vectors;

import java.util.Arrays;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/3/16  16:10
 * @description:
 */
public class SjfVector<E> {
    protected transient int modCount = 0;
    protected int elementCount;
    protected Object[] elementData;

    public boolean add(E e) {
        modCount++;
        check(elementCount++);
        elementData[elementCount++] = e;
        return true;
    }

    private void check(int minCapacity) {
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    protected int capacityIncrement;
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + ((capacityIncrement > 0) ?
                capacityIncrement : oldCapacity);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

    private int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) // overflow
            throw new OutOfMemoryError();
        return (minCapacity > MAX_ARRAY_SIZE) ?
                Integer.MAX_VALUE :
                MAX_ARRAY_SIZE;
    }

    public synchronized int size() {
        return elementCount;
    }
    
}
