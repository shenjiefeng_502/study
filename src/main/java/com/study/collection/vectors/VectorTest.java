package com.study.collection.vectors;

import java.util.Vector;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/3/16  16:11
 * @description: 测试
 */
public class VectorTest {
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        SjfVector<Integer> sjfVector = new SjfVector<>();
        vector.add(1);
        sjfVector.add(1);
        System.out.println(vector.size());
        System.out.println(sjfVector.size());
    }
}
