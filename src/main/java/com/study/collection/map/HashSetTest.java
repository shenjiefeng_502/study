package com.study.collection.map;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @program: ListStudyDemo
 * @ClassName HashSetTest
 * @description:
 * @author: sjf
 * @create: 2020-05-18 14:46
 * @Version 1.0
 **/
public class HashSetTest {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(null);
        set.remove(1);
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()){
            Integer next = iterator.next();
            System.out.println(next);
        }
    }
}
