package com.study.collection.map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;

/**
 * @program: ListStudyDemo
 * @ClassName MapTest
// * @description:
 * @author: sjf
 * @create: 2020-05-12 13:01
 * @Version 1.0
 **/
public class MapTest {
    public static void main(String[] args) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (int i=0;i<20;i++) {
            hashMap.put("test"+i, 1);
        }
        int MAXIMUM_CAPACITY = 1 << 30;
        System.out.println(MAXIMUM_CAPACITY);
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        treeMap.put(1,1);
        Hashtable<Integer, Integer> hashtable = new Hashtable<>();
        Integer integer = hashtable.put(1, 1);
    }
}
