package com.study.collection.map.hashmap;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: ListStudyDemo
 * @ClassName HashMapTest
 * @description:
 * @author: sjf
 * @create: 2020-05-18 17:06
 * @Version 1.0
 **/
public class HashMapTest {
    public static void main(String[] args) {
        HashMap<Integer, Integer> map = new HashMap<>();
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        concurrentHashMap.put(1,1);
    }
}
