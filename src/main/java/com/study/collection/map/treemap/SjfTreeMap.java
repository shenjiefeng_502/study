//package com.study.collection.map.treemap;
//import java.util.*;
//
///**
// * @program: ListStudyDemo
// * @ClassName SjfTreeMap
// * @description:
// * @author: sjf
// * @create: 2020-05-18 16:20
// * @Version 1.0
// **/
//public class SjfTreeMap<K,V> extends AbstractMap<K,V>
//        implements NavigableMap<K,V>, Cloneable, java.io.Serializable{
//    private static final long serialVersionUID = -6803584524146531136L;
//    /**
//     * 根节点
//     */
//    private transient Entry<K,V> root;
//    private final Comparator<? super K> comparator;
//    public SjfTreeMap() {
//        comparator = null;
//    }
//    /**
//     * tree数量
//     */
//    private transient int size = 0;
//    public SjfTreeMap(Comparator<? super K> comparator) {
//        this.comparator = comparator;
//    }
//
//    /**
//     * Compares two keys using the correct comparison method for this TreeMap.
//     */
//    @SuppressWarnings("unchecked")
//    final int compare(Object k1, Object k2) {
//        return comparator==null ? ((Comparable<? super K>)k1).compareTo((K)k2)
//                : comparator.compare((K)k1, (K)k2);
//    }
//
//    public TreeMap(Map<? extends K, ? extends V> m) {
//        comparator = null;
//        putAll(m);
//    }
//
//
//    // Red-black mechanics
//
//    private static final boolean RED   = false;
//    private static final boolean BLACK = true;
//
//    static final class Entry<K,V> implements Map.Entry<K,V> {
//        K key;
//        V value;
//        Entry<K,V> left;
//        Entry<K,V> right;
//        Entry<K,V> parent;
//        boolean color = BLACK;
//
//        /**
//         * The number of entries in the tree
//         */
//        private transient int size = 0;
//
//        /**
//         * The number of structural modifications to the tree.
//         */
//        private transient int modCount = 0;
//
//        /**
//         * Make a new cell with given key, value, and parent, and with
//         * {@code null} child links, and BLACK color.
//         */
//        Entry(K key, V value, Entry<K,V> parent) {
//            this.key = key;
//            this.value = value;
//            this.parent = parent;
//        }
//
//        /**
//         * Returns the key.
//         *
//         * @return the key
//         */
//        public K getKey() {
//            return key;
//        }
//
//        /**
//         * Returns the value associated with the key.
//         *
//         * @return the value associated with the key
//         */
//        public V getValue() {
//            return value;
//        }
//
//        /**
//         * Replaces the value currently associated with the key with the given
//         * value.
//         *
//         * @return the value associated with the key before this method was
//         *         called
//         */
//        public V setValue(V value) {
//            V oldValue = this.value;
//            this.value = value;
//            return oldValue;
//        }
//
//        public boolean equals(Object o) {
//            if (!(o instanceof Map.Entry))
//                return false;
//            Map.Entry<?,?> e = (Map.Entry<?,?>)o;
//
//            return valEquals(key,e.getKey()) && valEquals(value,e.getValue());
//        }
//
//        public int hashCode() {
//            int keyHash = (key==null ? 0 : key.hashCode());
//            int valueHash = (value==null ? 0 : value.hashCode());
//            return keyHash ^ valueHash;
//        }
//
//        public String toString() {
//            return key + "=" + value;
//        }
//    }
//
//    @Override
//    public V put(K key, V value) {
//        //root为根节点
//        Entry<K,V> t = root;
//        if (t == null) {
//            //调用方法查看是否重写Compare
//            compare(key, key); // type (and possibly null) check
//
//            root = new Entry<>(key, value, null);
//            size = 1;
//            modCount++;
//            return null;
//        }
//        int cmp;
//        Entry<K,V> parent;
//        // split comparator and comparable paths
//        Comparator<? super K> cpr = comparator;
//        if (cpr != null) {
//            //循环调用直到Key已经存在或者已经到叶子节点
//            do {
//                parent = t;
//                cmp = cpr.compare(key, t.key);
//                if (cmp < 0)
//                    t = t.left;
//                else if (cmp > 0)
//                    t = t.right;
//                else
//                    //键已存在时，更新value
//                    return t.setValue(value);
//            } while (t != null);
//        }
//        else {
//            if (key == null)
//                throw new NullPointerException();
//            @SuppressWarnings("unchecked")
//            Comparable<? super K> k = (Comparable<? super K>) key;
//            do {
//                parent = t;
//                cmp = k.compareTo(t.key);
//                if (cmp < 0)
//                    t = t.left;
//                else if (cmp > 0)
//                    t = t.right;
//                else
//                    return t.setValue(value);
//            } while (t != null);
//        }
//        //把查询到的叶子节点的父节点复制给该Entry
//        Entry<K,V> e = new Entry<>(key, value, parent);
//        //更新父节点子节点引用信息
//        if (cmp < 0)
//            parent.left = e;
//        else
//            parent.right = e;
//        //修复红黑树平衡
//        fixAfterInsertion(e);
//        size++;
//        modCount++;
//        return null;
//    }
//}
