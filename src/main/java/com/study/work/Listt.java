package com.study.work;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/9/2  23:02
 * @description: Copyright 2023 json.cn
 * <p>
 * Copyright 2023 json.cn
 */
/**
 * Copyright 2023 json.cn 
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Auto-generated: 2023-09-02 23:1:53
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
@Data
public class Listt {
    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject();
        List item = new ArrayList();
        item.add("jijie");
        item.add("nianfen");
        List sku = new ArrayList();
        sku.add("jijie");
        sku.add("nianfen");
        jsonObject.fluentPut("item",item);
        jsonObject.fluentPut("sku",sku);
        System.out.println(JSON.toJSONString(jsonObject));
    }

    private List<String> item;
    private List<String> sku;

}
