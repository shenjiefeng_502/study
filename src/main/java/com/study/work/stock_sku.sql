SELECT
  sys_item_id,
  sys_sku_id,
  pic_path,
  sku_outer_id,
  item_outer_id,
  title,
  short_title,
  properties_name,
  supplier_name,
  supplier_outer_id,
  brand,
  component,
  item_record,
  sku_record,
  defined_json,
  cost,
  sku_create_time,
  price,
  sum_stock,
  available_in_stock,
  lock_stock,
  defective_stock,
  virtual_stock,
  on_way_quantity as purchase_num,
  allocate_on_way_quantity as allocate_num,
  stock_down,
  advice_purchase_stock as advice_purchase_num,
  pick_stock,
  back_stock,
  purchase_stock,
  refund_stock,
  sum_cost,
  good_product_cost,
  defective_cost,
  available_cost,
  lock_cost,
  pick_cost,
  back_cost,
  purchase_cost,
  sum_price,
  available_price,
  lock_price,
  pick_price,
  back_price,
  purchase_price,
  purchase_stock_cost,
  refund_stock_cost,
  purchase_stock_price,
  refund_stock_price
FROM
  (
    SELECT
      t.sys_item_id,
      t.sys_sku_id,
      t.pic_path,
      t.sku_outer_id,
      t.item_outer_id,
      t.title,
      t.short_title,
      t.properties_name,
      t.brand,
      t.component,
      t.item_record,
      t.sku_record,
      t.defined_json,
      COALESCE(ox.import_price, t.cost, 0) cost,
      t.price,
      t.sku_create_time,
      isb.supplier_name,
      COALESCE(isb.supplier_outer_id, '') as supplier_outer_id,
      COALESCE(isb.supplier_id, '-1') as supplier_id,
      COALESCE(mt.sum_stock, 0) AS sum_stock,
      COALESCE(mt.available_in_stock, 0) AS available_in_stock,
      COALESCE(mt.lock_stock, 0) AS lock_stock,
      COALESCE(mt.defective_stock, 0) AS defective_stock,
      COALESCE(mt.virtual_stock, 0) AS virtual_stock,
      COALESCE(mt.on_way_quantity, 0) AS on_way_quantity,
      COALESCE(mt.allocate_on_way_quantity, 0) AS allocate_on_way_quantity,
      COALESCE(mt.stock_down, 0) AS stock_down,
      COALESCE(mt.advice_purchase_stock, 0) AS advice_purchase_stock,
      COALESCE(mt.pick_stock, 0) AS pick_stock,
      COALESCE(mt.back_stock, 0) AS back_stock,
      COALESCE(mt.purchase_stock, 0) AS purchase_stock,
      COALESCE(mt.refund_stock, 0) AS refund_stock,
      COALESCE(mt.sum_cost, 0) AS sum_cost,
      COALESCE(mt.good_product_cost) as good_product_cost,
      COALESCE(mt.defective_cost) as defective_cost,
      COALESCE(mt.available_cost, 0) AS available_cost,
      COALESCE(mt.lock_cost, 0) AS lock_cost,
      COALESCE(mt.pick_cost, 0) AS pick_cost,
      COALESCE(mt.back_cost, 0) AS back_cost,
      COALESCE(mt.purchase_cost, 0) AS purchase_cost,
      COALESCE(mt.purchase_stock_cost, 0) AS purchase_stock_cost,
      COALESCE(mt.refund_stock_cost, 0) AS refund_stock_cost,
      COALESCE(mt.sum_stock, 0) * t.price AS sum_price,
      COALESCE(mt.available_in_stock, 0) * t.price AS available_price,
      COALESCE(mt.lock_stock, 0) * t.price AS lock_price,
      COALESCE(mt.pick_stock, 0) * t.price AS pick_price,
      COALESCE(mt.back_stock, 0) * t.price AS back_price,
      COALESCE(mt.on_way_quantity, 0) * t.price AS purchase_price,
      COALESCE(mt.purchase_stock, 0) * t.price AS purchase_stock_price,
      COALESCE(mt.refund_stock, 0) * t.price AS refund_stock_price
    FROM
      (
        SELECT
          company_id,
          sys_item_id,
          0 AS sys_sku_id,
          pic_path,
          '' AS sku_outer_id,
          outer_id AS item_outer_id,
          title,
          COALESCE(short_title, '') as short_title,
          '' AS properties_name,
          purchase_price AS cost,
          created AS sku_create_time,
          selling_price AS price,
          brand,
          component,
          record as item_record,
          '' as sku_record,
          defined_json
        FROM
          dmj_item_18 item
        WHERE
          (
            company_id = 10438
            AND enable_status = 1
            AND active_status = 1
            AND is_sku_item = 0
            AND type = '0'
          )
        UNION ALL
        SELECT
          item.company_id AS company_id,
          item.sys_item_id AS sys_item_id,
          sku.sys_sku_id AS sys_sku_id,
          CASE
            WHEN (
              sku.pic_path IS NULL
              OR sku.pic_path = ''
              OR sku.pic_path = '/resources/css/build/images/no_pic.png'
            ) THEN item.pic_path
            ELSE sku.pic_path
          END AS pic_path,
          sku.outer_id AS sku_outer_id,
          item.outer_id AS item_outer_id,
          item.title AS title,
          case
            when sku.short_title is not null
            and sku.short_title != '' then sku.short_title
            else COALESCE(item.short_title, '')
          end as short_title,
          sku.properties_name AS properties_name,
          CASE
            WHEN sku.purchase_price IS NULL
            OR sku.purchase_price = 0 THEN item.purchase_price
            ELSE sku.purchase_price
          END AS cost,
          sku.created AS sku_create_time,
          CASE
            WHEN sku.selling_price IS NULL
            OR sku.selling_price = 0 THEN item.selling_price
            ELSE sku.selling_price
          END AS price,
          COALESCE(sku.brand, item.brand) as brand,
          COALESCE(sku.sku_component, item.component) as component,
          item.record as item_record,
          sku.record as sku_record,
          COALESCE(sku.defined_json, item.defined_json) as defined_json
        FROM
          dmj_item_18 item
          JOIN dmj_sku_38 sku ON item.sys_item_id = sku.sys_item_id
        WHERE
          (
            item.company_id = 10438
            AND item.enable_status = 1
            AND item.active_status = 1
            AND item.is_sku_item = 1
            AND item.type = '0'
            AND sku.company_id = 10438
            AND sku.enable_status = 1
            AND sku.active_status = 1
          )
      ) t
      LEFT JOIN history_import_price ox ON t.company_id = ox.company_id
      AND t.sys_item_id = ox.sys_item_id
      AND ox.sys_sku_id = CASE
        WHEN t.sys_sku_id = -1 THEN 0
        ELSE t.sys_sku_id
      END
      AND ox.import_price >= 0.0001
      AND '2023-09-12 16:27:25.321' >= ox.start
      AND '2023-09-12 16:27:25.321' <= ox.end_time
      LEFT JOIN (
        select
          sys_item_id,
          sys_sku_id,
          sum(COALESCE(available_in_stock, 0)) AS available_in_stock,
          sum(COALESCE(defective_stock, 0)) AS defective_stock,
          sum(COALESCE(lock_stock, 0)) AS lock_stock,
          sum(COALESCE(virtual_stock, 0)) AS virtual_stock,
          sum(COALESCE(on_way_quantity, 0)) AS on_way_quantity,
          sum(COALESCE(allocate_on_way_quantity, 0)) AS allocate_on_way_quantity,
          sum(COALESCE(stock_down, 0)) as stock_down,
          sum(COALESCE(pick_stock, 0)) as pick_stock,
          sum(COALESCE(back_stock, 0)) as back_stock,
          sum(COALESCE(purchase_stock, 0)) as purchase_stock,
          sum(COALESCE(refund_stock, 0)) as refund_stock,
          sum(COALESCE(sum_stock, 0)) as sum_stock,
          sum(COALESCE(good_product_stock, 0)) as good_product_stock,
          sum(COALESCE(advice_purchase_stock, 0)) as advice_purchase_stock,
          sum(COALESCE(sum_cost, 0)) as sum_cost,
          max(moving_cost) as moving_cost,
          sum(COALESCE(good_product_cost, 0)) as good_product_cost,
          sum(COALESCE(defective_cost, 0)) as defective_cost,
          sum(COALESCE(available_cost, 0)) as available_cost,
          sum(COALESCE(lock_cost, 0)) as lock_cost,
          sum(COALESCE(pick_cost, 0)) as pick_cost,
          sum(COALESCE(back_cost, 0)) as back_cost,
          sum(COALESCE(purchase_cost, 0)) as purchase_cost,
          sum(COALESCE(purchase_stock_cost, 0)) as purchase_stock_cost,
          sum(COALESCE(refund_stock_cost, 0)) as refund_stock_cost
        from
(
            select
              t.sys_item_id,
              t.sys_sku_id,
              t.warehouse_id,
              t.available_in_stock,
              t.defective_stock,
              t.lock_stock,
              t.virtual_stock,
              t.on_way_quantity,
              t.allocate_on_way_quantity,
              w.stock_down,
              ag.pick_stock,
              ag.back_stock,
              wa.purchase_stock,
              wa.refund_stock,
              COALESCE(t.available_in_stock, 0) + COALESCE(t.defective_stock, 0) + COALESCE(t.lock_stock, 0) AS sum_stock,
              COALESCE(t.available_in_stock, 0) + COALESCE(t.lock_stock, 0) as good_product_stock,
              COALESCE(w.stock_down, 0) - COALESCE(t.available_in_stock, 0) - COALESCE(t.lock_stock, 0) - COALESCE(t.on_way_quantity, 0) AS advice_purchase_stock,
              r.moving_cost,
              (
                COALESCE(t.available_in_stock, 0) + COALESCE(t.defective_stock, 0) + COALESCE(t.lock_stock, 0)
              ) * r.moving_cost AS sum_cost,
              (
                COALESCE(t.available_in_stock, 0) + COALESCE(t.lock_stock, 0)
              ) * r.moving_cost AS good_product_cost,
              COALESCE(t.defective_stock, 0) * r.moving_cost AS defective_cost,
              COALESCE(t.available_in_stock, 0) * r.moving_cost AS available_cost,
              COALESCE(t.lock_stock, 0) * r.moving_cost AS lock_cost,
              COALESCE(ag.pick_stock, 0) * r.moving_cost AS pick_cost,
              COALESCE(ag.back_stock, 0) * r.moving_cost AS back_cost,
              COALESCE(t.on_way_quantity, 0) * r.moving_cost AS purchase_cost,
              COALESCE(wa.purchase_stock, 0) * r.moving_cost AS purchase_stock_cost,
              COALESCE(wa.refund_stock, 0) * r.moving_cost AS refund_stock_cost
            from
              (
                select
                  sys_item_id,
                  sys_sku_id,
                  ware_house_id as warehouse_id,
                  sum(COALESCE(available_in_stock, 0)) AS available_in_stock,
                  sum(COALESCE(defective_stock, 0)) AS defective_stock,
                  sum(COALESCE(lock_stock, 0)) AS lock_stock,
                  sum(COALESCE(virtual_stock, 0)) AS virtual_stock,
                  sum(COALESCE(on_way_quantity, 0)) AS on_way_quantity,
                  sum(COALESCE(allocate_on_way_quantity, 0)) AS allocate_on_way_quantity
                FROM
                  stock_38 s
                where
                  company_id = 10438
                  AND enable_status = 1
                  AND ware_house_id in (31462)
                group by
                  sys_item_id,
                  sys_sku_id,
                  ware_house_id
              ) t
              LEFT JOIN item_warn_38 w ON (
                t.sys_item_id = w.sys_item_id
                AND t.sys_sku_id = w.sys_sku_id
                AND t.warehouse_id = w.warehouse_id
                AND w.warehouse_id = 0
                AND w.warehouse_id in (31462)
                AND w.enable_status = 1
              )
              LEFT JOIN (
                SELECT
                  sys_item_id,
                  sys_sku_id,
                  warehouse_id,
                  SUM(
                    CASE
                      WHEN stock_region_type = 1 THEN total_num
                      ELSE 0
                    END
                  ) AS pick_stock,
                  SUM(
                    CASE
                      WHEN stock_region_type = 2 THEN total_num
                      ELSE 0
                    END
                  ) AS back_stock
                FROM
                  asso_goods_section_sku_38
                WHERE
                  company_id = 10438
                  AND container_type = 0
                  AND warehouse_id in (31462)
                GROUP BY
                  sys_item_id,
                  sys_sku_id,
                  warehouse_id
              ) ag ON t.sys_item_id = ag.sys_item_id
              AND t.sys_sku_id = ag.sys_sku_id
              AND t.warehouse_id = ag.warehouse_id
              LEFT JOIN (
                SELECT
                  ag.sys_item_id,
                  ag.sys_sku_id,
                  ag.warehouse_id,
                  SUM(
                    CASE
                      WHEN ws.type = 'PURCHASE' THEN total_num
                      ELSE 0
                    END
                  ) AS purchase_stock,
                  SUM(
                    CASE
                      WHEN ws.type = 'REFUND' THEN total_num
                      ELSE 0
                    END
                  ) AS refund_stock
                FROM
                  working_storage_section ws
                  JOIN asso_goods_section_sku_38 ag ON ws.id = ag.stock_region_id
                  AND ws.company_id = ag.company_id
                WHERE
                  (
                    ag.company_id = 10438
                    AND container_type = 1
                    AND ws.warehouse_id in (31462)
                    and ag.warehouse_id in (31462)
                    AND ws.type IN ('PURCHASE', 'REFUND')
                  )
                GROUP BY
                  ag.sys_item_id,
                  ag.sys_sku_id,
                  ag.warehouse_id
              ) wa ON t.sys_item_id = wa.sys_item_id
              AND t.sys_sku_id = wa.sys_sku_id
              AND t.warehouse_id = wa.warehouse_id
              LEFT JOIN (
                select
                  sys_item_id,
                  sys_sku_id,
                  warehouse_id,
                  import_price as moving_cost
                from
                  history_import_price_warehouse
                where
                  company_id = 10438
                  and pt = 38
                  AND '2023-09-12 16:27:25.321' >= start
                  AND '2023-09-12 16:27:25.321' <= end_time
                  AND warehouse_id in (31462)
              ) r on t.sys_item_id = r.sys_item_id
              AND t.sys_sku_id = r.sys_sku_id
              and t.warehouse_id = r.warehouse_id
          ) x
        group by
          sys_item_id,
          sys_sku_id
      ) mt on t.sys_item_id = mt.sys_item_id
      AND t.sys_sku_id = mt.sys_sku_id
      LEFT JOIN (
        SELECT
          sys_item_id,
          sys_sku_id,
          array_to_string(array_agg(supplier_name), ',') AS supplier_name,
          array_to_string(array_agg(supplier_item_outer_id), ',') AS supplier_outer_id,
          array_to_string(array_agg(supplier_id), ',') as supplier_id
        FROM
          item_supplier_bridge_38
        WHERE
          company_id = 10438
          and enable_status = 1
        GROUP BY
          sys_item_id,
          sys_sku_id
      ) isb ON t.sys_item_id = isb.sys_item_id
      AND t.sys_sku_id = isb.sys_sku_id
  ) tb
WHERE
  1 = 1
ORDER BY
  sys_item_id,
  sys_sku_id
LIMIT
  50 OFFSET 0