package com.study.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

/**
 * @program: ListStudyDemo
 * @ClassName PahnRef
 * @description:当GC一但发现了虚引用对象，将会将PhantomReference对象插入ReferenceQueue队列，而此时PhantomReference所指向的对象并没有被GC回收，而是要等到ReferenceQueue被你真正的处理后才会被回收
 * @author: sjf
 * @create: 2020-05-06 15:36
 * @Version 1.0
 **/
public class PahnRef {
    public static void main(String[] args) {
        ReferenceQueue<Object> queue = new ReferenceQueue<>();
        PhantomReference<String> pr = new PhantomReference<>(new String("hello"), queue);
        System.out.println(pr.get());
    }
}
