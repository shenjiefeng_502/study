package com.study.gc;

import java.lang.ref.SoftReference;

/**
 * @program: ListStudyDemo
 * @ClassName SoftRef
 * @description:软引用在JVM报告内存不足的时候才会被GC回收，否则不会回收
 * @author: sjf
 * @create: 2020-05-06 15:01
 * @Version 1.0
 **/
public class SoftRef {
    public static void main(String[] args) {
        Object o = new Object();
        SoftReference reference = new SoftReference<>(o);
        //当结束对这个o实例的强引用,
        o=null;
        Object o1 = reference.get();
        System.out.println(o1==null);
    }
}
