package com.study.gc;

import java.lang.ref.WeakReference;

/**
 * @program: ListStudyDemo
 * @ClassName WeakRef
 * @description:弱引用对象,下次回收一定被回收
 * @author: sjf
 * @create: 2020-05-06 14:53
 * @Version 1.0
 **/
class Person {
    public String name;
    public int age;

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class WeakRef {
    public static void main(String[] args) {
        WeakReference<Person> reference = new WeakReference<>(new Person("zhangsan", 100));
        System.out.println(reference.get());
        System.gc();
        System.out.println(reference.get());
    }
}
