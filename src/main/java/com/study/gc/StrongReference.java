package com.study.gc;

/**
 * @program: ListStudyDemo
 * @ClassName StrongReference
 * @description:强引用如果内存不够会抛出异常不会回收new的对象
 * @author: sjf
 * @create: 2020-05-06 14:58
 * @Version 1.0
 **/
public class StrongReference {
    public void run(){
        //强引用
        Object o = new Object();
        Object[] array = new Object[10000000];
    }

    public static void main(String[] args) {
        new StrongReference().run();
    }
}
