package com.study.gc;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/19  14:49
 * @description: gc调优大对象直接分配到老年代
 */
public class PretenrueSizeThresholdTest {
    public static void main(String[] args) {
        //-XX:PretenureSizeThreshold=100000000
        byte[] bytes = new byte[900 * 1024 * 1024];
        for (MemoryPoolMXBean memoryPoolMXBean : ManagementFactory.getMemoryPoolMXBeans()) {
            System.out.println(memoryPoolMXBean.getName() + " 总量：" + memoryPoolMXBean.getUsage().getCommitted() + " 使用内存：" + memoryPoolMXBean.getUsage().getUsed());
        }
    }
}
