package com.study.gc;

/**
 * @program: ListStudyDemo
 * @ClassName ReferenceCountingGC
 * @description:
 * @author: sjf
 * @create: 2020-05-06 13:32
 * @Version 1.0
 **/
public class ReferenceCountingGC {
    private static final int MB = 1024 * 1024;

    private byte[]size=new byte[2*MB];

//    public static void main(String[] args) {
//        ReferenceCountingGC gc = new ReferenceCountingGC();
//        ReferenceCountingGC gc1 = new ReferenceCountingGC();
//        gc.instance=gc1;
//    }
}
