package com.study.excel.poi;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Word util
 *
 * @author Sollgo
 * @version 1.0
 */
public class WordUtils {

    /**
     * 创建doc文件
     * Create doc
     *
     * @param dataMap     the data map 替换参数
     * @param ftlPath     the ftl path ftl文件路径
     * @param ftlFileName the ftl file name ftl文件名
     * @param docFileUrl  the doc file url  生成的doc文件路径
     * @author Sollgo
     */
    public static void createDoc(Map<String, String> dataMap, String ftlPath, String ftlFileName, String docFileUrl) {
        try {

            //Configuration 用于读取ftl文件
            Configuration configuration = new Configuration(new Version("2.3.0"));
            configuration.setDefaultEncoding("utf-8");

            /**
             * 以下是两种指定ftl文件所在目录路径的方式，注意这两种方式都是
             * 指定ftl文件所在目录的路径，而不是ftl文件的路径
             */
            //指定路径的第一种方式（根据某个类的相对路径指定）
//                configuration.setClassForTemplateLoading(this.getClass(), "");

            //指定路径的第二种方式
            configuration.setDirectoryForTemplateLoading(new File(ftlPath));

            //输出文档路径及名称
            File outFile = new File(docFileUrl);
            outFile.getParentFile().mkdirs();
            //以utf-8的编码读取ftl文件
            Template template = configuration.getTemplate(ftlFileName, "utf-8");
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"), 10240);
            template.process(dataMap, out);
            out.close();

        } catch (Exception e) {
//            SxLogger.error("创建word文件失败",e);
//            throw new SXException("创建word文件失败");
        }
    }

    public static class User {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public User() {
        }

    }

    public static void main(String[] args) {
        ArrayList<User> users = new ArrayList<>();
        User user = new User();
        user.setId("12312");
        user.setName("a");
        User userb = new User();
        userb.setId("12313");
        userb.setName("b");
        User userc = new User();
        userc.setId("12314");
        userc.setName("c");
        User userd = new User();
        userd.setId("12315");
        userd.setName("d");
        users.add(user);

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("invoiceCompany", "杭州xx科技有限公司");
        dataMap.put("in" +
                "voiceDutyParagraph", "9144030071526726XG");
        dataMap.put("invoiceCompanyAddress", "杭州市滨江区浦沿街道AAA园区A幢666");
        dataMap.put("invoicePhone", "18756302020");
        dataMap.put("invoiceBankName", "杭州银行");
        dataMap.put("invoiceBankAccount", "287812793812739123123");
        dataMap.put("money", "200元");
        dataMap.put("image", getImageBase("/Users/shenjiefeng/IdeaProjects/untitled/src/main/resources/method-draw.png"));
        //WordUtil.createDoc(dataMap,filePathConfig.getFtlPath(),filePathConfig.getConstructionInvoiceFtlFileName(),filePath);
        createDoc(dataMap, "/Users/shenjiefeng/IdeaProjects/untitled/src/main/resources/", "tp2.ftl", "/Users/shenjiefeng/IdeaProjects/untitled/src/main/resources/图片文字.doc");
    }

    /**
     * Gets image base.
     *
     * @param src the src
     * @return the image base
     * @author zujiaqi
     * @date 2021-10-08 16:12:08
     */
    public static String getImageBase(String src) {
        if (src == null || src == "") {
            return "";
        }
        File file = new File(src);
        if (!file.exists()) {
            return "";
        }
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }


}
