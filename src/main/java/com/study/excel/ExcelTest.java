package com.study.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;

import java.util.ArrayList;
import java.util.List;

public class ExcelTest {
    public static void main(String[] args) {

        //实现excel写的操作
        //1 设置写入文件夹地址和excel文件名称
        String pathname = "/Users/shenjiefeng/Downloads/";
        String filename = pathname + "国庆工作安排.xls";
        // 2 调用easyexcel里面的方法实现写操作
        // write方法两个参数：第一个参数文件路径名称，第二个参数实体类class
//        EasyExcel.write(filename, ExcelEntity.class).sheet("学生列表").doWrite(getData());


        //实现excel读操作
        ExcelReaderSheetBuilder sheet = EasyExcel.read(filename, EnergyData.class, new ExcelListener()).sheet();
        sheet.doRead();
    }

    //创建方法返回list集合
    private static List<ExcelEntity> getData() {
        List<ExcelEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ExcelEntity data = new ExcelEntity();
//            data.setSno(i);
//            data.setSname("lucy" + i);
            list.add(data);
        }
        return list;
    }
}
