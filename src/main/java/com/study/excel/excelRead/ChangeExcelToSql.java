package com.study.excel.excelRead;
//
//
//import com.alibaba.fastjson.JSON;
//import excel.EnergyData;
//import excelRead.EnergyData;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.InputStreamReader;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.stream.Collectors;
//
//public class ChangeExcelToSql {
//    /**
//     * Do 1
//     *
//     * @param excelVO
//     * @return result the result
//     * @author sjf
//     * @date 2021-08-19 14:22:19
//     */
//    public Result do1(ImportExcelVO excelVO) {
//        String path = "/Users/shenjiefeng/IdeaProjects/shuxi-cb/shuxi-ent/src/main/java/com/shuxi/ent/service/excelRead.txt";
//        File file = new File(path);
//        ConcurrentHashMap<String, EntInfo> map = new ConcurrentHashMap<>(160);
//        List<IndustryInfo> infoList = industryInfoService.list(new QueryWrapper<IndustryInfo>().lambda().select(IndustryInfo::getIndustryCode, IndustryInfo::getIndustryName, IndustryInfo::getIndustryType));
//        List<TownStreet> list = streetService.list(new QueryWrapper<TownStreet>().lambda().eq(TownStreet::getRegionId, "402881837043405301704346123B0B80"));
//        Map<String, String> areaCodeMap = list.stream().collect(Collectors.toMap(TownStreet::getTownStreetName, TownStreet::getAreaCode));
//        Map<String, IndustryInfo> industryMap = new HashMap<>();
//        infoList.forEach(info -> {
//            if (industryMap.get(info.getIndustryName()) == null) {
//                industryMap.put(info.getIndustryName(), info);
//            }
//        });
//
//        try {
//            //构造一个BufferedReader类来读取文件
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
//            String s = null;
//            //使用readLine方法，一次读一行
//            while ((s = br.readLine()) != null) {
//                EnergyData data = JSON.parseObject(s, EnergyData.class);
//                if (map.get(data.getEntName()) == null) {
//                    EntInfo entInfo = new EntInfo();
//                    entInfo.setEntId(UuidUtil.getUUID());
//                    entInfo.setIndustryName(data.getIndustry());
//                    getIndustry(industryMap, entInfo);
//                    entInfo.setSocCode(data.getSocCode());
//                    entInfo.setEntName(data.getEntName());
//                    entInfo.setAreaCode(getAreaCode(data, areaCodeMap));
//                    map.put(entInfo.getEntName(), entInfo);
//                }
//            }
//            br.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Collection<EntInfo> values = map.values();
//        saveBatch(values);
//        return null;
//    }
//
//
//    public static void main(String[] args) {
//        String path = "/Users/shenjiefeng/IdeaProjects/untitled/src/main/java/excelRead/excelRead/能源数据（2020年）.xls";
//        File file = new File(path);
//        StringBuilder result = new StringBuilder();
//        ConcurrentHashMap<String, EnergyData> map = new ConcurrentHashMap<>();
//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));//构造一个BufferedReader类来读取文件
//
//            String s = null;
//            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
//                EnergyData data = JSON.parseObject(s, EnergyData.class);
//                map.put(data.getEntName(), data);
//            }
//            br.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Collection<EnergyData> values = map.values();
//        values.forEach(data-> System.out.println(data));
//
//    }
//}
