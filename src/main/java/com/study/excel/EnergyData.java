package com.study.excel;

import lombok.Data;

/**
 * @description: 能源数据
 * @program: easyexcel
 * @author: ly
 * @create: 2021-08-16 14:15
 * @version: 1.0
 **/
@Data
public class EnergyData {

    /**
     * 区县
     */
    private String district;




}
