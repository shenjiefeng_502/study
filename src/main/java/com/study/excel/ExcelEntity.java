package com.study.excel;

import lombok.Data;

import java.util.ArrayList;


@Data
public class ExcelEntity {
    private Integer years;

    public static void main(String[] args) {
        ArrayList<ExcelEntity> list = new ArrayList<>();
        ExcelEntity entity = new ExcelEntity();
        entity.setYears(9);
        list.add(entity);
        ExcelEntity entity1 = new ExcelEntity();
        entity1.setYears(2);
        list.add(entity1);
        list.sort((a,b)->a.getYears().compareTo(b.getYears()));
        System.out.println(list);
    }
}
