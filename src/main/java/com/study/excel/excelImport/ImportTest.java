package com.study.excel.excelImport;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

public class ImportTest {
    public static void main(String[] args) {
        String list = "[\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 321.33305,\n" +
                "    \"cecottav\": 0.04456,\n" +
                "    \"cecottave\": 0.06899,\n" +
                "    \"cecpttov\": 0.01183,\n" +
                "    \"cecpttove\": 0.01832,\n" +
                "    \"ecec\": 138.80772,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D4\",\n" +
                "    \"entName\": \"浙江东信机械有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1927.59,\n" +
                "    \"cecottav\": 1.21214,\n" +
                "    \"cecottave\": 2.80101,\n" +
                "    \"cecpttov\": 0.14679,\n" +
                "    \"cecpttove\": 0.3392,\n" +
                "    \"ecec\": 778.164,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D5\",\n" +
                "    \"entName\": \"新昌县坚固传动科技有限公司\",\n" +
                "    \"industryName\": \"锻件及粉末冶金制品制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 512.5701,\n" +
                "    \"cecottav\": 0.15036,\n" +
                "    \"cecottave\": 0.34026,\n" +
                "    \"cecpttov\": 0.03361,\n" +
                "    \"cecpttove\": 0.07605,\n" +
                "    \"ecec\": 210.31746,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D6\",\n" +
                "    \"entName\": \"新昌县擎光电子科技有限公司\",\n" +
                "    \"industryName\": \"纺织专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 7031.41307,\n" +
                "    \"cecottav\": 0.51496,\n" +
                "    \"cecottave\": 0.65712,\n" +
                "    \"cecpttov\": 0.17256,\n" +
                "    \"cecpttove\": 0.2202,\n" +
                "    \"ecec\": 3605.07343,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D7\",\n" +
                "    \"entName\": \"浙江文源智能科技有限公司\",\n" +
                "    \"industryName\": \"摩托车零部件及配件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1077.03019,\n" +
                "    \"cecottav\": 0.04426,\n" +
                "    \"cecottave\": 0.07267,\n" +
                "    \"cecpttov\": 0.00909,\n" +
                "    \"cecpttove\": 0.01493,\n" +
                "    \"ecec\": 590.99573,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D8\",\n" +
                "    \"entName\": \"浙江博泰纺织有限公司\",\n" +
                "    \"industryName\": \"其他针织或钩针编织服装制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 7230.28588,\n" +
                "    \"cecottav\": 0.73211,\n" +
                "    \"cecottave\": 1.6782,\n" +
                "    \"cecpttov\": 0.08866,\n" +
                "    \"cecpttove\": 0.20323,\n" +
                "    \"ecec\": 2923.29078,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D9\",\n" +
                "    \"entName\": \"万丰镁瑞丁新材料科技有限公司\",\n" +
                "    \"industryName\": \"有色金属铸造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 3813.55861,\n" +
                "    \"cecottav\": 0.19401,\n" +
                "    \"cecottave\": 0.4317,\n" +
                "    \"cecpttov\": 0.05508,\n" +
                "    \"cecpttove\": 0.12256,\n" +
                "    \"ecec\": 1547.859,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DA\",\n" +
                "    \"entName\": \"浙江品诺机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2302.90725,\n" +
                "    \"cecottav\": 0.13411,\n" +
                "    \"cecottave\": 0.3099,\n" +
                "    \"cecpttov\": 0.03807,\n" +
                "    \"cecpttove\": 0.08798,\n" +
                "    \"ecec\": 929.67085,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DB\",\n" +
                "    \"entName\": \"新昌新天龙纽尚精密轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 575.86309,\n" +
                "    \"cecottav\": 0.02627,\n" +
                "    \"cecottave\": 0.05558,\n" +
                "    \"cecpttov\": 0.00735,\n" +
                "    \"cecpttove\": 0.01554,\n" +
                "    \"ecec\": 235.72476,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DC\",\n" +
                "    \"entName\": \"浙江陀曼精密机械有限公司\",\n" +
                "    \"industryName\": \"金属切削机床制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 220.899,\n" +
                "    \"cecottav\": 0.03323,\n" +
                "    \"cecottave\": 0.07679,\n" +
                "    \"cecpttov\": 0.01078,\n" +
                "    \"cecpttove\": 0.02491,\n" +
                "    \"ecec\": 89.1754,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DD\",\n" +
                "    \"entName\": \"新昌县开心纺织有限公司\",\n" +
                "    \"industryName\": \"棉纺纱加工\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2678.928,\n" +
                "    \"cecottav\": 1.19026,\n" +
                "    \"cecottave\": 2.75049,\n" +
                "    \"cecpttov\": 0.25138,\n" +
                "    \"cecpttove\": 0.5809,\n" +
                "    \"ecec\": 1081.4688,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DE\",\n" +
                "    \"entName\": \"新昌县泰兴纺织有限公司\",\n" +
                "    \"industryName\": \"化纤织造加工\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 703.5,\n" +
                "    \"cecottav\": 0.19381,\n" +
                "    \"cecottave\": 0.44786,\n" +
                "    \"cecpttov\": 0.05656,\n" +
                "    \"cecpttove\": 0.13071,\n" +
                "    \"ecec\": 284,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00DF\",\n" +
                "    \"entName\": \"浙江比泽尔制冷机电有限公司\",\n" +
                "    \"industryName\": \"建筑装饰用石开采\",\n" +
                "    \"industryType\": \"MINE_ENTERPRISE\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 253.53707,\n" +
                "    \"cecottav\": 0.12108,\n" +
                "    \"cecottave\": 0.23775,\n" +
                "    \"cecpttov\": 0.03016,\n" +
                "    \"cecpttove\": 0.05922,\n" +
                "    \"ecec\": 104.7209,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E0\",\n" +
                "    \"entName\": \"浙江翊蓝铝业有限公司\",\n" +
                "    \"industryName\": \"汽车零部件及配件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 712.6455,\n" +
                "    \"cecottav\": 0.1283,\n" +
                "    \"cecottave\": 0.29647,\n" +
                "    \"cecpttov\": 0.03642,\n" +
                "    \"cecpttove\": 0.08417,\n" +
                "    \"ecec\": 287.6943,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E1\",\n" +
                "    \"entName\": \"新昌县华鼎机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 661.29,\n" +
                "    \"cecottav\": 0.18002,\n" +
                "    \"cecottave\": 0.41599,\n" +
                "    \"cecpttov\": 0.05111,\n" +
                "    \"cecpttove\": 0.1181,\n" +
                "    \"ecec\": 266.964,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E2\",\n" +
                "    \"entName\": \"新昌县彼扬机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 77.30019,\n" +
                "    \"cecottav\": 0.20665,\n" +
                "    \"cecottave\": 0.33518,\n" +
                "    \"cecpttov\": 0.03416,\n" +
                "    \"cecpttove\": 0.05541,\n" +
                "    \"ecec\": 33.31523,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E3\",\n" +
                "    \"entName\": \"浙江倍特储粮设备科技有限公司\",\n" +
                "    \"industryName\": \"金属门窗制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1343.685,\n" +
                "    \"cecottav\": 0.16768,\n" +
                "    \"cecottave\": 0.38747,\n" +
                "    \"cecpttov\": 0.0476,\n" +
                "    \"cecpttove\": 0.11,\n" +
                "    \"ecec\": 542.441,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E4\",\n" +
                "    \"entName\": \"新昌县普佑机电科技有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 755.78483,\n" +
                "    \"cecottav\": 0.17688,\n" +
                "    \"cecottave\": 0.38045,\n" +
                "    \"cecpttov\": 0.05022,\n" +
                "    \"cecpttove\": 0.10801,\n" +
                "    \"ecec\": 308.50917,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E5\",\n" +
                "    \"entName\": \"新昌县安鑫轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 408.03,\n" +
                "    \"cecottav\": 0.14016,\n" +
                "    \"cecottave\": 0.32388,\n" +
                "    \"cecpttov\": 0.03979,\n" +
                "    \"cecpttove\": 0.09195,\n" +
                "    \"ecec\": 164.718,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E6\",\n" +
                "    \"entName\": \"新昌县利利红轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 245.2471,\n" +
                "    \"cecottav\": 0.09396,\n" +
                "    \"cecottave\": 0.14459,\n" +
                "    \"cecpttov\": 0.03213,\n" +
                "    \"cecpttove\": 0.04945,\n" +
                "    \"ecec\": 114.00058,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E7\",\n" +
                "    \"entName\": \"万丰飞机工业有限公司\",\n" +
                "    \"industryName\": \"飞机制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 3.6582,\n" +
                "    \"cecottav\": 0.0004,\n" +
                "    \"cecottave\": 0.00092,\n" +
                "    \"cecpttov\": 0.00013,\n" +
                "    \"cecpttove\": 0.0003,\n" +
                "    \"ecec\": 1.47772,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E8\",\n" +
                "    \"entName\": \"浙江陀曼智造科技有限公司\",\n" +
                "    \"industryName\": \"工业自动控制系统装置制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 471.97815,\n" +
                "    \"cecottav\": 0.15323,\n" +
                "    \"cecottave\": 0.3541,\n" +
                "    \"cecpttov\": 0.0435,\n" +
                "    \"cecpttove\": 0.10053,\n" +
                "    \"ecec\": 190.53199,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00E9\",\n" +
                "    \"entName\": \"浙江省新昌县三星轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2778.825,\n" +
                "    \"cecottav\": 0.42557,\n" +
                "    \"cecottave\": 0.9834,\n" +
                "    \"cecpttov\": 0.12082,\n" +
                "    \"cecpttove\": 0.27919,\n" +
                "    \"ecec\": 1121.805,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00EA\",\n" +
                "    \"entName\": \"新昌县大雄锻造有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 3397.905,\n" +
                "    \"cecottav\": 0.26841,\n" +
                "    \"cecottave\": 0.62025,\n" +
                "    \"cecpttov\": 0.0762,\n" +
                "    \"cecpttove\": 0.17609,\n" +
                "    \"ecec\": 1371.723,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00EB\",\n" +
                "    \"entName\": \"新昌县红利机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 13143.11553,\n" +
                "    \"cecottav\": 0.08172,\n" +
                "    \"cecottave\": 0.18818,\n" +
                "    \"cecpttov\": 0.0232,\n" +
                "    \"cecpttove\": 0.05343,\n" +
                "    \"ecec\": 5307.824,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00EC\",\n" +
                "    \"entName\": \"斯凯孚(新昌)轴承与精密技术有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 306.52935,\n" +
                "    \"cecottav\": 0.19794,\n" +
                "    \"cecottave\": 0.34269,\n" +
                "    \"cecpttov\": 0.04948,\n" +
                "    \"cecpttove\": 0.08567,\n" +
                "    \"ecec\": 129.674,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00ED\",\n" +
                "    \"entName\": \"新昌县泰普莱机电有限公司\",\n" +
                "    \"industryName\": \"制冷、空调设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 249.6018,\n" +
                "    \"cecottav\": 0.14934,\n" +
                "    \"cecottave\": 0.34512,\n" +
                "    \"cecpttov\": 0.02936,\n" +
                "    \"cecpttove\": 0.06785,\n" +
                "    \"ecec\": 100.75828,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00EE\",\n" +
                "    \"entName\": \"新昌县中邦塑粉科技有限公司\",\n" +
                "    \"industryName\": \"涂料制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1402.6383,\n" +
                "    \"cecottav\": 0.23983,\n" +
                "    \"cecottave\": 0.55421,\n" +
                "    \"cecpttov\": 0.08056,\n" +
                "    \"cecpttove\": 0.18616,\n" +
                "    \"ecec\": 566.24118,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00EF\",\n" +
                "    \"entName\": \"新昌晶辉玻璃制品有限公司\",\n" +
                "    \"industryName\": \"其他玻璃制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 714.72142,\n" +
                "    \"cecottav\": 0.21171,\n" +
                "    \"cecottave\": 0.47689,\n" +
                "    \"cecpttov\": 0.0601,\n" +
                "    \"cecpttove\": 0.13539,\n" +
                "    \"ecec\": 289.34057,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B6\",\n" +
                "    \"entName\": \"新昌县合力连杆制造有限公司\",\n" +
                "    \"industryName\": \"其他传动部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 63.315,\n" +
                "    \"cecottav\": 0.02932,\n" +
                "    \"cecottave\": 0.06776,\n" +
                "    \"cecpttov\": 0.00684,\n" +
                "    \"cecpttove\": 0.0158,\n" +
                "    \"ecec\": 25.559,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B7\",\n" +
                "    \"entName\": \"新昌县海博科技股份有限公司\",\n" +
                "    \"industryName\": \"其他专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 178.19655,\n" +
                "    \"cecottav\": 0.03183,\n" +
                "    \"cecottave\": 0.07356,\n" +
                "    \"cecpttov\": 0.01064,\n" +
                "    \"cecpttove\": 0.02459,\n" +
                "    \"ecec\": 71.93663,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B8\",\n" +
                "    \"entName\": \"浙江天雄工业技术有限公司\",\n" +
                "    \"industryName\": \"增材制造装备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 73.09365,\n" +
                "    \"cecottav\": 0.0298,\n" +
                "    \"cecottave\": 0.06887,\n" +
                "    \"cecpttov\": 0.00449,\n" +
                "    \"cecpttove\": 0.01037,\n" +
                "    \"ecec\": 29.50829,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B9\",\n" +
                "    \"entName\": \"新昌县百德电子有限公司\",\n" +
                "    \"industryName\": \"其他输配电及控制设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 252.9786,\n" +
                "    \"cecottav\": 0.07909,\n" +
                "    \"cecottave\": 0.18277,\n" +
                "    \"cecpttov\": 0.02245,\n" +
                "    \"cecpttove\": 0.05189,\n" +
                "    \"ecec\": 102.12156,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BA\",\n" +
                "    \"entName\": \"新昌县华明轴承有限公司\",\n" +
                "    \"industryName\": \"制冷、空调设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 110.27512,\n" +
                "    \"cecottav\": 0.03761,\n" +
                "    \"cecottave\": 0.07158,\n" +
                "    \"cecpttov\": 0.00938,\n" +
                "    \"cecpttove\": 0.01785,\n" +
                "    \"ecec\": 48.26842,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BB\",\n" +
                "    \"entName\": \"新昌县科宇机械有限公司\",\n" +
                "    \"industryName\": \"内燃机及配件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1226.03379,\n" +
                "    \"cecottav\": 0.19927,\n" +
                "    \"cecottave\": 0.40478,\n" +
                "    \"cecpttov\": 0.05657,\n" +
                "    \"cecpttove\": 0.11492,\n" +
                "    \"ecec\": 504.529,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BC\",\n" +
                "    \"entName\": \"新昌县林泉轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 856.61525,\n" +
                "    \"cecottav\": 0.16447,\n" +
                "    \"cecottave\": 0.37139,\n" +
                "    \"cecpttov\": 0.04669,\n" +
                "    \"cecpttove\": 0.10544,\n" +
                "    \"ecec\": 346.98,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BD\",\n" +
                "    \"entName\": \"新昌县翔宇轴承厂\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1144.24275,\n" +
                "    \"cecottav\": 0.12256,\n" +
                "    \"cecottave\": 0.27176,\n" +
                "    \"cecpttov\": 0.03479,\n" +
                "    \"cecpttove\": 0.07715,\n" +
                "    \"ecec\": 477.25915,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BE\",\n" +
                "    \"entName\": \"浙江润意机械股份有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1273.335,\n" +
                "    \"cecottav\": 0.16136,\n" +
                "    \"cecottave\": 0.37288,\n" +
                "    \"cecpttov\": 0.04581,\n" +
                "    \"cecpttove\": 0.10586,\n" +
                "    \"ecec\": 514.041,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00BF\",\n" +
                "    \"entName\": \"浙江诚本轴承滚子有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1514.01614,\n" +
                "    \"cecottav\": 0.28753,\n" +
                "    \"cecottave\": 0.66187,\n" +
                "    \"cecpttov\": 0.10064,\n" +
                "    \"cecpttove\": 0.23165,\n" +
                "    \"ecec\": 611.49948,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C0\",\n" +
                "    \"entName\": \"浙江新昌康平胶囊有限公司\",\n" +
                "    \"industryName\": \"卫生材料及医药用品制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 476.87747,\n" +
                "    \"cecottav\": 0.16052,\n" +
                "    \"cecottave\": 0.26543,\n" +
                "    \"cecpttov\": 0.04557,\n" +
                "    \"cecpttove\": 0.07536,\n" +
                "    \"ecec\": 203.8,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C1\",\n" +
                "    \"entName\": \"新昌金涛轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 844.2,\n" +
                "    \"cecottav\": 0.05407,\n" +
                "    \"cecottave\": 0.12494,\n" +
                "    \"cecpttov\": 0.01852,\n" +
                "    \"cecpttove\": 0.0428,\n" +
                "    \"ecec\": 340.8,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C2\",\n" +
                "    \"entName\": \"新昌县宏宇制冷有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1483.2594,\n" +
                "    \"cecottav\": 0.25378,\n" +
                "    \"cecottave\": 0.58644,\n" +
                "    \"cecpttov\": 0.07205,\n" +
                "    \"cecpttove\": 0.16649,\n" +
                "    \"ecec\": 598.78324,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C3\",\n" +
                "    \"entName\": \"新昌县锋泰机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 183.82455,\n" +
                "    \"cecottav\": 0.05968,\n" +
                "    \"cecottave\": 0.13792,\n" +
                "    \"cecpttov\": 0.01584,\n" +
                "    \"cecpttove\": 0.03662,\n" +
                "    \"ecec\": 74.20543,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C4\",\n" +
                "    \"entName\": \"新昌县众力弹簧厂\",\n" +
                "    \"industryName\": \"弹簧制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 585.0306,\n" +
                "    \"cecottav\": 0.08693,\n" +
                "    \"cecottave\": 0.20088,\n" +
                "    \"cecpttov\": 0.02204,\n" +
                "    \"cecpttove\": 0.05094,\n" +
                "    \"ecec\": 236.17076,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C5\",\n" +
                "    \"entName\": \"浙江元金印刷有限公司\",\n" +
                "    \"industryName\": \"包装装潢及其他印刷\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 229.80494,\n" +
                "    \"cecottav\": 0.06821,\n" +
                "    \"cecottave\": 0.11661,\n" +
                "    \"cecpttov\": 0.01524,\n" +
                "    \"cecpttove\": 0.02606,\n" +
                "    \"ecec\": 97.96164,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C6\",\n" +
                "    \"entName\": \"浙江晟东印染机械有限公司\",\n" +
                "    \"industryName\": \"纺织专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2106.81655,\n" +
                "    \"cecottav\": 0.68795,\n" +
                "    \"cecottave\": 1.2573,\n" +
                "    \"cecpttov\": 0.21719,\n" +
                "    \"cecpttove\": 0.39693,\n" +
                "    \"ecec\": 801.63663,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C7\",\n" +
                "    \"entName\": \"浙江绿健胶囊有限公司\",\n" +
                "    \"industryName\": \"药用辅料及包装材料\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 582.09052,\n" +
                "    \"cecottav\": 0.15448,\n" +
                "    \"cecottave\": 0.33267,\n" +
                "    \"cecpttov\": 0.04101,\n" +
                "    \"cecpttove\": 0.08832,\n" +
                "    \"ecec\": 237.599,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C8\",\n" +
                "    \"entName\": \"浙江大齐机械有限公司\",\n" +
                "    \"industryName\": \"紧固件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 361.47342,\n" +
                "    \"cecottav\": 0.11071,\n" +
                "    \"cecottave\": 0.23174,\n" +
                "    \"cecpttov\": 0.02939,\n" +
                "    \"cecpttove\": 0.06153,\n" +
                "    \"ecec\": 148.058,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00C9\",\n" +
                "    \"entName\": \"新昌县润达机械有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 330.43395,\n" +
                "    \"cecottav\": 0.12725,\n" +
                "    \"cecottave\": 0.29405,\n" +
                "    \"cecpttov\": 0.03613,\n" +
                "    \"cecpttove\": 0.08348,\n" +
                "    \"ecec\": 133.39867,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CA\",\n" +
                "    \"entName\": \"新昌县环亚特轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 333.79069,\n" +
                "    \"cecottav\": 0.08678,\n" +
                "    \"cecottave\": 0.19171,\n" +
                "    \"cecpttov\": 0.02492,\n" +
                "    \"cecpttove\": 0.05506,\n" +
                "    \"ecec\": 135.6871,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CB\",\n" +
                "    \"entName\": \"浙江祥和阀门有限公司\",\n" +
                "    \"industryName\": \"阀门和旋塞制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1858.5638,\n" +
                "    \"cecottav\": 0.21495,\n" +
                "    \"cecottave\": 0.48906,\n" +
                "    \"cecpttov\": 0.06786,\n" +
                "    \"cecpttove\": 0.1544,\n" +
                "    \"ecec\": 751.77705,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CC\",\n" +
                "    \"entName\": \"浙江宏辉胶丸有限公司\",\n" +
                "    \"industryName\": \"药用辅料及包装材料\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1159.368,\n" +
                "    \"cecottav\": 0.12177,\n" +
                "    \"cecottave\": 0.28138,\n" +
                "    \"cecpttov\": 0.03457,\n" +
                "    \"cecpttove\": 0.07988,\n" +
                "    \"ecec\": 468.0328,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CD\",\n" +
                "    \"entName\": \"新昌县海顺轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 479.05313,\n" +
                "    \"cecottav\": 0.131,\n" +
                "    \"cecottave\": 0.27093,\n" +
                "    \"cecpttov\": 0.03719,\n" +
                "    \"cecpttove\": 0.07692,\n" +
                "    \"ecec\": 196.41467,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CE\",\n" +
                "    \"entName\": \"新昌县迅达机械有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 340.62741,\n" +
                "    \"cecottav\": 0.08082,\n" +
                "    \"cecottave\": 0.17327,\n" +
                "    \"cecpttov\": 0.02295,\n" +
                "    \"cecpttove\": 0.04919,\n" +
                "    \"ecec\": 138.98433,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00CF\",\n" +
                "    \"entName\": \"新昌县纽伦轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 135.6348,\n" +
                "    \"cecottav\": 0.05612,\n" +
                "    \"cecottave\": 0.12967,\n" +
                "    \"cecpttov\": 0.01398,\n" +
                "    \"cecpttove\": 0.0323,\n" +
                "    \"ecec\": 54.76008,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D0\",\n" +
                "    \"entName\": \"浙江新昌同一汽车部件有限公司\",\n" +
                "    \"industryName\": \"汽车零部件及配件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 790.77011,\n" +
                "    \"cecottav\": 0.11023,\n" +
                "    \"cecottave\": 0.22411,\n" +
                "    \"cecpttov\": 0.03129,\n" +
                "    \"cecpttove\": 0.06363,\n" +
                "    \"ecec\": 324.53807,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D1\",\n" +
                "    \"entName\": \"绍兴熔岩机械有限公司\",\n" +
                "    \"industryName\": \"汽车零部件及配件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 143.31851,\n" +
                "    \"cecottav\": 0.03507,\n" +
                "    \"cecottave\": 0.06842,\n" +
                "    \"cecpttov\": 0.00784,\n" +
                "    \"cecpttove\": 0.01529,\n" +
                "    \"ecec\": 59.35356,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D2\",\n" +
                "    \"entName\": \"浙江凯成智能设备股份有限公司\",\n" +
                "    \"industryName\": \"纺织专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2148.43746,\n" +
                "    \"cecottav\": 0.78879,\n" +
                "    \"cecottave\": 1.78624,\n" +
                "    \"cecpttov\": 0.22394,\n" +
                "    \"cecpttove\": 0.50711,\n" +
                "    \"ecec\": 869.2443,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00D3\",\n" +
                "    \"entName\": \"新昌县海拓轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1360.35305,\n" +
                "    \"cecottav\": 0.07555,\n" +
                "    \"cecottave\": 0.17371,\n" +
                "    \"cecpttov\": 0.02145,\n" +
                "    \"cecpttove\": 0.04932,\n" +
                "    \"ecec\": 549.46414,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E0098\",\n" +
                "    \"entName\": \"浙江金泰实业发展有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1648.40087,\n" +
                "    \"cecottav\": 0.07539,\n" +
                "    \"cecottave\": 0.17066,\n" +
                "    \"cecpttov\": 0.0214,\n" +
                "    \"cecpttove\": 0.04845,\n" +
                "    \"ecec\": 667.2132,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E0099\",\n" +
                "    \"entName\": \"新昌县超海轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 696.465,\n" +
                "    \"cecottav\": 0.03391,\n" +
                "    \"cecottave\": 0.07837,\n" +
                "    \"cecpttov\": 0.00963,\n" +
                "    \"cecpttove\": 0.02225,\n" +
                "    \"ecec\": 281.159,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009A\",\n" +
                "    \"entName\": \"浙江新昌三雄轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1983.30909,\n" +
                "    \"cecottav\": 0.03966,\n" +
                "    \"cecottave\": 0.08049,\n" +
                "    \"cecpttov\": 0.01053,\n" +
                "    \"cecpttove\": 0.02137,\n" +
                "    \"ecec\": 825.32279,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009B\",\n" +
                "    \"entName\": \"浙江同星科技股份有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 16894.54506,\n" +
                "    \"cecottav\": 0.12352,\n" +
                "    \"cecottave\": 0.19253,\n" +
                "    \"cecpttov\": 0.03447,\n" +
                "    \"cecpttove\": 0.05374,\n" +
                "    \"ecec\": 6143.91366,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009C\",\n" +
                "    \"entName\": \"浙江京新药业股份有限公司\",\n" +
                "    \"industryName\": \"化学药品制剂制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 209.29125,\n" +
                "    \"cecottav\": 0.08698,\n" +
                "    \"cecottave\": 0.201,\n" +
                "    \"cecpttov\": 0.01049,\n" +
                "    \"cecpttove\": 0.02424,\n" +
                "    \"ecec\": 84.48725,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009D\",\n" +
                "    \"entName\": \"浙江金朗博药业有限公司\",\n" +
                "    \"industryName\": \"食品及饲料添加剂制造\",\n" +
                "    \"industryType\": \"FOOD\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 0,\n" +
                "    \"cecottav\": 0,\n" +
                "    \"cecottave\": 0,\n" +
                "    \"cecpttov\": 0,\n" +
                "    \"cecpttove\": 0,\n" +
                "    \"ecec\": 0,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009E\",\n" +
                "    \"entName\": \"浙江新昌皮尔轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 4212.38836,\n" +
                "    \"cecottav\": 0.55802,\n" +
                "    \"cecottave\": 0.76751,\n" +
                "    \"cecpttov\": 0.0673,\n" +
                "    \"cecpttove\": 0.09256,\n" +
                "    \"ecec\": 1475.55,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E009F\",\n" +
                "    \"entName\": \"新昌和宝生物科技有限公司\",\n" +
                "    \"industryName\": \"食品及饲料添加剂制造\",\n" +
                "    \"industryType\": \"FOOD\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 351.75,\n" +
                "    \"cecottav\": 1.25392,\n" +
                "    \"cecottave\": 2.89758,\n" +
                "    \"cecpttov\": 0.17618,\n" +
                "    \"cecpttove\": 0.40711,\n" +
                "    \"ecec\": 142,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A0\",\n" +
                "    \"entName\": \"新昌县金声铜业有限公司\",\n" +
                "    \"industryName\": \"铜压延加工\",\n" +
                "    \"industryType\": \"OTHER_NONFERROUS_METALS\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1228.74201,\n" +
                "    \"cecottav\": 0.22639,\n" +
                "    \"cecottave\": 0.4181,\n" +
                "    \"cecpttov\": 0.03181,\n" +
                "    \"cecpttove\": 0.05874,\n" +
                "    \"ecec\": 516.477,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A1\",\n" +
                "    \"entName\": \"浙江日佳铜业科技有限公司\",\n" +
                "    \"industryName\": \"铜压延加工\",\n" +
                "    \"industryType\": \"OTHER_NONFERROUS_METALS\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 114.88155,\n" +
                "    \"cecottav\": 0.0565,\n" +
                "    \"cecottave\": 0.13056,\n" +
                "    \"cecpttov\": 0.01384,\n" +
                "    \"cecpttove\": 0.03199,\n" +
                "    \"ecec\": 46.37763,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A2\",\n" +
                "    \"entName\": \"新昌县巨臣卫浴有限公司\",\n" +
                "    \"industryName\": \"建筑装饰及水暖管道零件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 4175.31852,\n" +
                "    \"cecottav\": 0.49543,\n" +
                "    \"cecottave\": 1.06646,\n" +
                "    \"cecpttov\": 0.14065,\n" +
                "    \"cecpttove\": 0.30277,\n" +
                "    \"ecec\": 1701.55494,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A3\",\n" +
                "    \"entName\": \"新昌沛斯轴承配件有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 1724.841,\n" +
                "    \"cecottav\": 0.67077,\n" +
                "    \"cecottave\": 1.19133,\n" +
                "    \"cecpttov\": 0.07694,\n" +
                "    \"cecpttove\": 0.13665,\n" +
                "    \"ecec\": 651.1026,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A4\",\n" +
                "    \"entName\": \"浙江可明生物医药有限公司\",\n" +
                "    \"industryName\": \"食品及饲料添加剂制造\",\n" +
                "    \"industryType\": \"FOOD\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 505.89041,\n" +
                "    \"cecottav\": 0.1965,\n" +
                "    \"cecottave\": 0.42617,\n" +
                "    \"cecpttov\": 0.05217,\n" +
                "    \"cecpttove\": 0.11315,\n" +
                "    \"ecec\": 206.259,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A5\",\n" +
                "    \"entName\": \"浙江新昌大型螺帽有限公司\",\n" +
                "    \"industryName\": \"紧固件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 206.2662,\n" +
                "    \"cecottav\": 0.21047,\n" +
                "    \"cecottave\": 0.48639,\n" +
                "    \"cecpttov\": 0.02414,\n" +
                "    \"cecpttove\": 0.05579,\n" +
                "    \"ecec\": 83.26452,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A6\",\n" +
                "    \"entName\": \"浙江新昌天然保健品有限公司\",\n" +
                "    \"industryName\": \"保健食品制造\",\n" +
                "    \"industryType\": \"FOOD\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 440.74275,\n" +
                "    \"cecottav\": 0.14571,\n" +
                "    \"cecottave\": 0.3367,\n" +
                "    \"cecpttov\": 0.04137,\n" +
                "    \"cecpttove\": 0.09559,\n" +
                "    \"ecec\": 177.92915,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A7\",\n" +
                "    \"entName\": \"新昌县万利轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 96.30915,\n" +
                "    \"cecottav\": 0.03973,\n" +
                "    \"cecottave\": 0.0918,\n" +
                "    \"cecpttov\": 0.01141,\n" +
                "    \"cecpttove\": 0.02636,\n" +
                "    \"ecec\": 38.88459,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A8\",\n" +
                "    \"entName\": \"浙江利永达制冷机械有限公司\",\n" +
                "    \"industryName\": \"阀门和旋塞制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 63.24465,\n" +
                "    \"cecottav\": 0.01511,\n" +
                "    \"cecottave\": 0.03493,\n" +
                "    \"cecpttov\": 0.00494,\n" +
                "    \"cecpttove\": 0.01141,\n" +
                "    \"ecec\": 25.53289,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00A9\",\n" +
                "    \"entName\": \"新昌县合创机械有限公司\",\n" +
                "    \"industryName\": \"实验分析仪器制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 385.59,\n" +
                "    \"cecottav\": 0.05194,\n" +
                "    \"cecottave\": 0.08726,\n" +
                "    \"cecpttov\": 0.01162,\n" +
                "    \"cecpttove\": 0.01953,\n" +
                "    \"ecec\": 143.266,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AA\",\n" +
                "    \"entName\": \"新昌公盛材料有限公司\",\n" +
                "    \"industryName\": \"化学试剂和助剂制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 787.4093,\n" +
                "    \"cecottav\": 0.10009,\n" +
                "    \"cecottave\": 0.18549,\n" +
                "    \"cecpttov\": 0.02658,\n" +
                "    \"cecpttove\": 0.04925,\n" +
                "    \"ecec\": 335.936,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AB\",\n" +
                "    \"entName\": \"浙江佛城制冷有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 445.25207,\n" +
                "    \"cecottav\": 0.08008,\n" +
                "    \"cecottave\": 0.17481,\n" +
                "    \"cecpttov\": 0.02274,\n" +
                "    \"cecpttove\": 0.04963,\n" +
                "    \"ecec\": 181.351,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AC\",\n" +
                "    \"entName\": \"新昌县通用机械有限公司\",\n" +
                "    \"industryName\": \"其他传动部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 176.4378,\n" +
                "    \"cecottav\": 0.06013,\n" +
                "    \"cecottave\": 0.13896,\n" +
                "    \"cecpttov\": 0.00845,\n" +
                "    \"cecpttove\": 0.01952,\n" +
                "    \"ecec\": 71.22388,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AD\",\n" +
                "    \"entName\": \"新昌县荣进机械有限公司\",\n" +
                "    \"industryName\": \"其他有色金属压延加工\",\n" +
                "    \"industryType\": \"OTHER_NONFERROUS_METALS\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 92.08815,\n" +
                "    \"cecottav\": 0.0182,\n" +
                "    \"cecottave\": 0.04206,\n" +
                "    \"cecpttov\": 0.00483,\n" +
                "    \"cecpttove\": 0.01117,\n" +
                "    \"ecec\": 37.17799,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AE\",\n" +
                "    \"entName\": \"浙江康利德科技股份有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 445.99125,\n" +
                "    \"cecottav\": 0.11285,\n" +
                "    \"cecottave\": 0.23388,\n" +
                "    \"cecpttov\": 0.03204,\n" +
                "    \"cecpttove\": 0.0664,\n" +
                "    \"ecec\": 183.669,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00AF\",\n" +
                "    \"entName\": \"浙江嘉阳轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 160.245,\n" +
                "    \"cecottav\": 0.07656,\n" +
                "    \"cecottave\": 0.11705,\n" +
                "    \"cecpttov\": 0.01942,\n" +
                "    \"cecpttove\": 0.02968,\n" +
                "    \"ecec\": 68.12893,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B0\",\n" +
                "    \"entName\": \"新昌县国泰纸箱有限公司\",\n" +
                "    \"industryName\": \"包装装潢及其他印刷\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 3931.158,\n" +
                "    \"cecottav\": 0.42336,\n" +
                "    \"cecottave\": 0.97831,\n" +
                "    \"cecpttov\": 0.12019,\n" +
                "    \"cecpttove\": 0.27774,\n" +
                "    \"ecec\": 1586.9968,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B1\",\n" +
                "    \"entName\": \"新昌县天擎轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 657.42845,\n" +
                "    \"cecottav\": 0.05352,\n" +
                "    \"cecottave\": 0.12062,\n" +
                "    \"cecpttov\": 0.01421,\n" +
                "    \"cecpttove\": 0.03203,\n" +
                "    \"ecec\": 266.412,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B2\",\n" +
                "    \"entName\": \"新昌县丰亿电器有限公司\",\n" +
                "    \"industryName\": \"其他通用零部件制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 515.59362,\n" +
                "    \"cecottav\": 0.16016,\n" +
                "    \"cecottave\": 0.35011,\n" +
                "    \"cecpttov\": 0.04547,\n" +
                "    \"cecpttove\": 0.0994,\n" +
                "    \"ecec\": 214.9764,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B3\",\n" +
                "    \"entName\": \"新昌雪莲花轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 2328.13889,\n" +
                "    \"cecottav\": 0.15105,\n" +
                "    \"cecottave\": 0.34749,\n" +
                "    \"cecpttov\": 0.04288,\n" +
                "    \"cecpttove\": 0.09865,\n" +
                "    \"ecec\": 940.42,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B4\",\n" +
                "    \"entName\": \"新昌县隆豪轴承有限公司\",\n" +
                "    \"industryName\": \"滚动轴承制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 51.4152,\n" +
                "    \"cecottav\": 0.02478,\n" +
                "    \"cecottave\": 0.03375,\n" +
                "    \"cecpttov\": 0.00554,\n" +
                "    \"cecpttove\": 0.00754,\n" +
                "    \"ecec\": 23.25624,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922E00B5\",\n" +
                "    \"entName\": \"新昌县锦兴机械有限公司\",\n" +
                "    \"industryName\": \"纺织专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 0,\n" +
                "    \"cecottav\": 0,\n" +
                "    \"cecottave\": 0,\n" +
                "    \"cecpttov\": 0,\n" +
                "    \"cecpttove\": 0,\n" +
                "    \"ecec\": 0,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007A\",\n" +
                "    \"entName\": \"浙江先锋彩印包装有限公司\",\n" +
                "    \"industryName\": \"包装装潢及其他印刷\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 55.5765,\n" +
                "    \"cecottav\": 0.01149,\n" +
                "    \"cecottave\": 0.02656,\n" +
                "    \"cecpttov\": 0.00257,\n" +
                "    \"cecpttove\": 0.00594,\n" +
                "    \"ecec\": 22.4369,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007B\",\n" +
                "    \"entName\": \"浙江盛星智能装备有限公司\",\n" +
                "    \"industryName\": \"纺织专用设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 0,\n" +
                "    \"cecottav\": 0.89801,\n" +
                "    \"cecottave\": 2.07513,\n" +
                "    \"cecpttov\": 0.17888,\n" +
                "    \"cecpttove\": 0.41337,\n" +
                "    \"ecec\": 668.31052,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007C\",\n" +
                "    \"entName\": \"浙江极盾新材料科技有限公司\",\n" +
                "    \"industryName\": \"铝冶炼\",\n" +
                "    \"industryType\": \"NONFERROUS_METALS\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 339.24512,\n" +
                "    \"cecottav\": 0.09125,\n" +
                "    \"cecottave\": 0.1358,\n" +
                "    \"cecpttov\": 0.02703,\n" +
                "    \"cecpttove\": 0.04023,\n" +
                "    \"ecec\": 150.01605,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007D\",\n" +
                "    \"entName\": \"浙江迈德维智能科技有限公司\",\n" +
                "    \"industryName\": \"其他智能消费设备制造\",\n" +
                "    \"industryType\": \"ELECTRONIC_EQUIPMENT\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 283.6512,\n" +
                "    \"cecottav\": 0.14988,\n" +
                "    \"cecottave\": 0.34635,\n" +
                "    \"cecpttov\": 0.04519,\n" +
                "    \"cecpttove\": 0.10443,\n" +
                "    \"ecec\": 114.50552,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007E\",\n" +
                "    \"entName\": \"浙江耿坚电子科技有限公司\",\n" +
                "    \"industryName\": \"敏感元件及传感器制造\",\n" +
                "    \"industryType\": \"ELECTRONIC_EQUIPMENT\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 295.6107,\n" +
                "    \"cecottav\": 0.10018,\n" +
                "    \"cecottave\": 0.2315,\n" +
                "    \"cecpttov\": 0.00975,\n" +
                "    \"cecpttove\": 0.02252,\n" +
                "    \"ecec\": 119.33422,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D007F\",\n" +
                "    \"entName\": \"浙江诚茂控股集团有限公司\",\n" +
                "    \"industryName\": \"精制茶加工\",\n" +
                "    \"industryType\": \"FOOD\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 48.4008,\n" +
                "    \"cecottav\": 0.03942,\n" +
                "    \"cecottave\": 0.09107,\n" +
                "    \"cecpttov\": 0.00708,\n" +
                "    \"cecpttove\": 0.01635,\n" +
                "    \"ecec\": 19.54368,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0080\",\n" +
                "    \"entName\": \"新昌县新都混凝土有限公司\",\n" +
                "    \"industryName\": \"水泥制品制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 890.44988,\n" +
                "    \"cecottav\": 0.64159,\n" +
                "    \"cecottave\": 1.00507,\n" +
                "    \"cecpttov\": 0.17907,\n" +
                "    \"cecpttove\": 0.28052,\n" +
                "    \"ecec\": 415.83685,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0081\",\n" +
                "    \"entName\": \"浙江中同药业有限公司\",\n" +
                "    \"industryName\": \"化学药品制剂制造\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 29.19525,\n" +
                "    \"cecottav\": 0.01729,\n" +
                "    \"cecottave\": 0.03996,\n" +
                "    \"cecpttov\": 0.00199,\n" +
                "    \"cecpttove\": 0.00461,\n" +
                "    \"ecec\": 11.78565,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0082\",\n" +
                "    \"entName\": \"新昌县锐德制冷设备有限公司\",\n" +
                "    \"industryName\": \"钢压延加工\",\n" +
                "    \"industryType\": \"OTHER\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 21.31605,\n" +
                "    \"cecottav\": 0.02142,\n" +
                "    \"cecottave\": 0.04954,\n" +
                "    \"cecpttov\": 0.00495,\n" +
                "    \"cecpttove\": 0.01145,\n" +
                "    \"ecec\": 8.60133,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0083\",\n" +
                "    \"entName\": \"奔野万丰农业装备有限公司\",\n" +
                "    \"industryName\": \"其他农、林、牧、渔业机械制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 154.0665,\n" +
                "    \"cecottav\": 0.04323,\n" +
                "    \"cecottave\": 0.09988,\n" +
                "    \"cecpttov\": 0.01233,\n" +
                "    \"cecpttove\": 0.0285,\n" +
                "    \"ecec\": 62.2009,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0084\",\n" +
                "    \"entName\": \"新昌县普霖斯暖通科技有限公司\",\n" +
                "    \"industryName\": \"金属压力容器制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  },\n" +
                "  {\n" +
                "    \"carbonEmissionTotal\": 98.49,\n" +
                "    \"cecottav\": 0.04916,\n" +
                "    \"cecottave\": 0.11359,\n" +
                "    \"cecpttov\": 0.01229,\n" +
                "    \"cecpttove\": 0.0284,\n" +
                "    \"ecec\": 39.764,\n" +
                "    \"entId\": \"FF8080817BBF1089017BBF10922D0085\",\n" +
                "    \"entName\": \"新昌县剡源换热器有限公司\",\n" +
                "    \"industryName\": \"制冷、空调设备制造\",\n" +
                "    \"industryType\": \"MACHINE_MANUFACTURING\",\n" +
                "    \"keyEnt\": null,\n" +
                "    \"poecipc\": 0,\n" +
                "    \"totalEnergyCost\": 0,\n" +
                "    \"totalProductionCost\": 0\n" +
                "  }\n" +
                "]";
        JSONArray jsonArray = JSONObject.parseArray(list);
        System.out.println(jsonArray);
        List<ExcelImportData> excelImportData = JSON.parseArray(list, ExcelImportData.class);
        System.out.println(excelImportData);
        String fileName = "/Users/shenjiefeng/IdeaProjects/untitled/src/main/java/excel/excelImport/" + "simpleWrite" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, ExcelImportData.class)
                .sheet("模板")
                .doWrite(excelImportData);

    }
}
