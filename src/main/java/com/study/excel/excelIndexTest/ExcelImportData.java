package com.study.excel.excelIndexTest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ContentRowHeight(20)
@HeadRowHeight(40)
@ColumnWidth(35)
public class ExcelImportData {
    private static final BigDecimal initValue = BigDecimal.ZERO;

    /**
     * 企业名称
     */
    @ExcelProperty(value = "企业名称", index = 0)
    private String entName;
    /**
     * 子级行业中文名称
     */
    @ExcelProperty(value = "所属行业", index = 1)
    private String industryName;
    /** 碳排放总量 */
    @ExcelProperty(value = "碳排放总量", index = 2)
    private BigDecimal carbonEmissionTotal = initValue;
    /** 等价综合能耗 */
    @ExcelProperty(value = "综合能耗", index = 3)
    private BigDecimal ecec = initValue;
    /** 万元产值综合能耗（当量） */
    @ExcelProperty(value = "万元产值综合能耗", index = 4)
    private BigDecimal cecpttov = initValue;
    /** 万元增加值综合能耗（当量） */
    @ExcelProperty(value = "万元增加值综合能耗", index = 5)
    private BigDecimal cecottav = initValue;
    /** 万元增加值综合能耗（等价） */
    @ExcelProperty(value = "万元增加值综合能耗", index = 6)
    private BigDecimal cecottave = initValue;
    /** 能源总费用 */
    @ExcelProperty(value = "能源总费用", index = 7)
    private BigDecimal totalEnergyCost = initValue;
    /** 生产总成本 */
    @ExcelProperty(value = "生产总成本", index = 8)
    private BigDecimal totalProductionCost = initValue;
    /** 能源费用占生产成本比重 */
    @ExcelProperty(value = "能源费用占生产成本比重", index = 9)
    private BigDecimal poecipc = initValue;
}
