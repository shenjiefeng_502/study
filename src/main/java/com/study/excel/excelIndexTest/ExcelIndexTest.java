package com.study.excel.excelIndexTest;


import com.study.excel.easyUtil.BaseExcelListenerTest;
import com.study.excel.easyUtil.EasyExcelUtil;
import com.study.excel.easyUtil.ExcelSaveService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

public class ExcelIndexTest implements ExcelSaveService<ExcelImportData> {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("/Users/shenjiefeng/IdeaProjects/untitled/src/main/java/excel/excelIndexTest/simpleWrite1634092114995.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        BaseExcelListenerTest<ExcelImportData> listenerTest = new BaseExcelListenerTest<>();
        BaseExcelListenerTest readListener = new BaseExcelListenerTest();
//        readListener.setExcelSaveService(this);
        EasyExcelUtil.readExcel(fileInputStream, ExcelImportData.class, readListener, 1);
    }

    @Override
    public void saveData(List<ExcelImportData> list, Map<String, Object> map) {

    }
}
