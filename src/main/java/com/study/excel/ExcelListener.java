package com.study.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;

import java.util.Map;

public class ExcelListener extends AnalysisEventListener<EnergyData> {
    //一行一行读取excel内容
    //读取表头内容
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
    }

    @Override
    public void invoke(EnergyData energyData, AnalysisContext analysisContext) {
        System.out.println(JSON.toJSONString(energyData));
    }

    //读取完成之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }
}