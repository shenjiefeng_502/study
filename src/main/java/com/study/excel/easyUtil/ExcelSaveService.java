package com.study.excel.easyUtil;


import java.util.List;
import java.util.Map;

public interface ExcelSaveService<T> {
    void saveData(List<T> list, Map<String, Object> map);
}
