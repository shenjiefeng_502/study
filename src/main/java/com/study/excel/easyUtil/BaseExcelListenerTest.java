package com.study.excel.easyUtil;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.util.NumberUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 模板的读取类
 *
 * @author sjf
 * @version 1.0
 * @date 2021-08-17 19:28
 */
public class BaseExcelListenerTest<T> extends AnalysisEventListener<T> {
    /**
     * 每隔300条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 300;
    @Getter
    List<T> list = new ArrayList<>();

    private ExcelSaveService<T> excelSaveService;

    private Map<String, Object> map;

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public void setExcelSaveService(ExcelSaveService<T> excelSaveService) {
        this.excelSaveService = excelSaveService;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(T data, AnalysisContext context) {
        try {
//            SxLogger.info("解析到一条数据:" + JSON.toJSONString(data));
            list.add(data);
        } catch (Exception e) {
            throw e;
//            throw new SXException("解析excel失败" + e);
        } finally {
            // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
            if (list.size() >= BATCH_COUNT) {
                saveData(map);
                // 存储完成清理 list
                list.clear();
            }
        }


    }
    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData(map);
//        SxLogger.info("所有数据解析完成！");
    }

    /**
     * 入库,继承该类后实现该方法即可
     */
    public void saveData(Map<String, Object> map) {
        List<T> list = this.getList();
        excelSaveService.saveData(list, map);
    }

}
