package com.study.excel.easyUtil;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.write.metadata.WriteSheet;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Easy excel util
 *
 * @author ly
 * @version 1.0
 * @date 2021-08-30 13:33:36
 * @description: excel工具类
 * @program: shuxi-cb
 * @author: ly
 * @create: 2021-08-18 10:25
 * @version: 1.0
 */
public class EasyExcelUtil {

    /**
     * 分段读取(每读取300行则清理一边数据 避免了OOM)
     * 读取excel
     *
     * @param <T>          the type parameter
     * @param inputStream  导入的文件流
     * @param model        生成的类
     * @param readListener 监听类
     * @param rowNumber    首行数据行数
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> void readExcel(InputStream inputStream, Class<T> model, ReadListener readListener,int rowNumber){
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(inputStream, model, readListener).sheet()
                // 这里可以设置1，因为头就是一行。如果多行头，可以设置其他值。不传入也可以，因为默认会根据DemoData 来解析，他没有指定头，也就是默认1行
                .headRowNumber(rowNumber).doRead();
    }

    /**
     * 分段读取(每读取300行则清理一边数据 避免了OOM)
     * 读取excel 默认从第一行读取
     *
     * @param <T>          the type parameter
     * @param inputStream  导入的文件流
     * @param model        生成的类
     * @param readListener 监听类
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> void readExcel(InputStream inputStream, Class<T> model, ReadListener readListener){
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(inputStream, model, readListener).sheet()
                // 这里可以设置1，因为头就是一行。如果多行头，可以设置其他值。不传入也可以，因为默认会根据DemoData 来解析，他没有指定头，也就是默认1行
                .doRead();
    }

    /**
     * 分段读取(每读取300行则清理一边数据 避免了OOM)
     * 读取excel 默认从第一行读取
     *
     * @param <T>          the type parameter
     * @param inputStream  导入的文件流
     * @param readListener 监听类
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> void readExcel(InputStream inputStream, ReadListener readListener){
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(inputStream, readListener).sheet()
                // 这里可以设置1，因为头就是一行。如果多行头，可以设置其他值。不传入也可以，因为默认会根据DemoData 来解析，他没有指定头，也就是默认1行
                .doRead();
    }

    /**
     * 同步的返回，不推荐使用，如果数据量大会把数据放到内存里面(有可能引发OOM)
     * 读取excel
     *
     * @param <T>       the type parameter
     * @param file      导入的文件流
     * @param model     生成的类
     * @param rowNumber 首行数据行数
     * @return 对象数组 the list
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> List<T> synchronousRead(InputStream file, Class<T> model,int rowNumber) {
        return EasyExcel.read(file).head(model).sheet().headRowNumber(rowNumber).doReadSync();
    }

    /**
     * 同步的返回，不推荐使用，如果数据量大会把数据放到内存里面(有可能引发OOM)
     * 读取excel
     *
     * @param <T>   the type parameter
     * @param file  导入的文件流
     * @param model 生成的类
     * @return 对象数组 the list
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> List<T> synchronousRead(InputStream file, Class<T> model) {
        return EasyExcel.read(file).head(model).sheet().doReadSync();
    }

    /**
     * 分段读取(每读取300行则清理一边数据 避免了OOM)
     * 读取excel
     *
     * @param <T>          the type parameter
     * @param inputStream  导入的文件流
     * @param model        生成的类
     * @param readListener 监听类
     * @param rowNumber    首行数据行数
     * @author ly
     * @date 2021-08-30 13:33:37
     */
    public static<T> void readExcel(InputStream inputStream, Class<T> model, BaseExcelListenerTest readListener, int rowNumber){
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(inputStream, model, readListener).sheet()
                // 这里可以设置1，因为头就是一行。如果多行头，可以设置其他值。不传入也可以，因为默认会根据DemoData 来解析，他没有指定头，也就是默认1行
                .headRowNumber(rowNumber).doRead();
    }

    /**
     * 最简单的写
     *
     * @param response
     * @param list
     * @param cls list中元素的类class
     * @return void
     * @MethodName: simpleWrite
     * @auth: sjf
     * @date 2022/5/7 16:34
     */
    public static void simpleWrite(HttpServletResponse response, List list, Class cls) throws IOException {
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        //定义工具表对象
        WriteSheet sheet = EasyExcel.writerSheet(0, "sheet1").head(cls).build();
        //往excel文件中写入数据
        excelWriter.write(list, sheet);
        excelWriter.finish();
    }

}
