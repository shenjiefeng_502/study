package com.study.io.chat;

import java.io.*;
import java.net.Socket;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/29  11:18
 * @description: 客户端 向固定的端口8888发送信息,客户端端口不固定
 */
public class SocketClient {
    public static void main(String[] args) {
        final String host = "127.0.0.1";
        final int port = 8888;
        //创建Socket
        try (Socket socket = new Socket(host, port)) {
            //接收消息
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(socket.getInputStream())
            );
            //发送消息
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())
            );
            //获取用户输入的消息
            BufferedReader userReader = new BufferedReader(
                    new InputStreamReader(System.in)
            );
            String msg = null;
            //循环客户端就可以一直输入消息,不然执行完会自动释放资源,断开连接
            while (true) {
                String input = userReader.readLine();
                //写入客户端要发送的消息.因为服务端用readList获取消息.其以\n为终点,所以要在消息最后加上\n
                writer.write(input + "\n");
                writer.flush();
                msg = reader.readLine();
                System.out.println(msg);
                //如果客户端输入quit就可以退出循环
                if (input.equals("quit")){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
