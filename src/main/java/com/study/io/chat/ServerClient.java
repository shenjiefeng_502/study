package com.study.io.chat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/29  11:26
 * @description: 服务端 new一个socket接收8888端口穿过来的信息
 */
public class ServerClient {
    public static void main(String[] args) {
        final int port = 8888;
        try (final ServerSocket serverSocket = new ServerSocket(8888)) {
            System.out.println("ServerSocket Start, The Port is:" + port);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Client[" + socket.getPort() + "]Online");
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                String msg = null;
                while ((msg = reader.readLine()) != null) {
                    System.out.println("Client[" + socket.getPort() + "]:" + msg);
                    writer.write("Server:" + msg + "\n");
                    writer.flush();
                    if (msg.equals("quit")){
                        System.out.println("Client[" + socket.getPort() + "]:Offline");
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
