package com.study.io.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/5  11:11
 * @description:客户端输入线程
 */
public class UserInputHandler implements Runnable {
    private ChatClient client;

    public UserInputHandler(ChatClient chatClient) {
        this.client = chatClient;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(System.in));
            while (true) {
                String input = reader.readLine();
                client.sendToServer(input);
                if(input.equals("quit")){
                    break;
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
