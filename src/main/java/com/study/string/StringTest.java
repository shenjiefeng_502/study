package com.study.string;

/**
 * @program: ListStudyDemo
 * @ClassName StringTest
 * @description:
 * @author: sjf
 * @create: 2020-05-06 12:41
 * @Version 1.0
 **/
public class StringTest {
    public static void main(String[] args) {
        //该对象只生成了一个实例:jdk执行了字符串折叠
        String s="1"+"2"+"3";
        //创建了两个对象,一个常量池对象,一个string对象
        String s2=new String("hello");
        String s1="hello";
        //对象地址不同
        //常量池只有一个"hello",但有两个string对象,两个对象地址不同
        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));
    }
}
