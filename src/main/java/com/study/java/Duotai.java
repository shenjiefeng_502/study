package com.study.java;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/8/31  15:23
 * @description:
 */
public class Duotai {
    public static class Fruit {
        int num;
        public void eat(){
            System.out.println("eat Fruit");
        }
    }
    public static class Apple extends Fruit{
        @Override
        public void eat() {
            super.num = 10;
            System.out.println("eat " + num + " Apple");
        }

    }
    public static void main(String[] args) {
        Fruit fruit = new Apple();
        fruit.eat();
    }
}
