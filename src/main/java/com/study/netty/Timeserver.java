package com.study.netty;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * TimeServer监听的是4040，通过构造函数创建ServerSocket，
 * 如果端口未被占用则表示监听成功，然后通过无限循环来监听客户端的连接，如果没有客户端接入，则主线程阻塞在ServerSocket的accept操作上
 *
 * 启动TimeServer，通过jvisualvm打印的线程堆栈可以发现它是阻塞在accept操作上的
 * 该工具在：JAVA_HOME/bin/jvisualvm.exe，打开后默认监听本机所有JVM程序….
 */
public class Timeserver {
    public static void main(String[] args) {
        int port = 4040;
        System.out.println("start server......" + port);
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                new TimeServerHandler(serverSocket.accept()).run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
