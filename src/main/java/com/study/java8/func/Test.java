package com.study.java8.func;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2021/8/4  14:44
 * @description: sd
 */
public class Test {
    private static Map<String, Consumer<String>> FUNC_MAP = new ConcurrentHashMap<>();
    private static String MAN = "man";
    private static String WOMAN = "woman";

    static {
        FUNC_MAP.put(MAN, person -> {
            System.out.println(person + "应该去男厕所");
        });
        FUNC_MAP.put(WOMAN, person -> {
            System.out.println(person + "应该去女厕所");
        });
    }


    public static void main(String[] args) {
        Person p = new Person();
        p.setGender(MAN);
        p.setName("张三");
        ArrayList<Person> list = new ArrayList<>();
        list.add(p);
        Person p2 = new Person();
        p2.setGender(WOMAN);
        p.setGender(WOMAN);
        list.add(p);
        p2.setName("张三他老婆");
        System.out.println(list);
//        String gender = p.getGender();
//        Consumer<String> stringConsumer = FUNC_MAP.get(gender);
//        stringConsumer.accept(p.name);
//        FUNC_MAP.get(p2.getGender()).accept(p2.name);

    }
}

class Person {
    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    private String gender;

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}

