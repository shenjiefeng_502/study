package com.study.java8.newdate;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/27  14:24
 * @description:
 */
public class NewDateExample {
    public static void main(String[] args) {
        Date oldDate = new Date();
        LocalDate newDate = LocalDate.now();
        //新旧年月日
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ods = sdf.format(oldDate);
        System.out.println(String.format("date format : %s", ods));
        System.out.println(newDate);
        System.out.println(String.format("date format : %s", newDate));

        //新旧时间
        //format HH:mm:ss
        SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
        String ots = sdft.format(oldDate);
        System.out.println(String.format("time format : %s", ots));
        //format HH:mm:ss
        LocalTime newTIme = LocalTime.now().withNano(0);
        System.out.println(String.format("time format : %s", newTIme));

        //新旧全时间
        //format yyyy-MM-dd HH:mm:ss
        //format yyyy-MM-dd HH:mm:ss
        SimpleDateFormat sdfdt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String datetime = sdfdt.format(oldDate);
        System.out.println(String.format("dateTime format : %s", datetime));

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateTimeStr = dateTime.format(dateTimeFormatter);
        System.out.println(String.format("dateTime format : %s", dateTimeStr));
    }
}
