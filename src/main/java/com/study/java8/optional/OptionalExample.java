package com.study.java8.optional;

import java.util.Optional;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/27  13:32
 * @description:
 */
public class OptionalExample {
    public static void main(String[] args) {
        Integer val1 = null;
        Integer val2 = new Integer(10);
        // Optional.ofNullable(val1); 允许传递为null参数
        Optional<Integer> a = Optional.ofNullable(val1);

        //Optional.of 如果传递的蚕食是null，抛出异常NPE
        Optional<Integer> b = Optional.of(val2);
        System.out.println(sum(a, b));
        a.orElseGet(() -> getDefaultValue());
    }

    public static Integer getDefaultValue() {  //远程方法调用
        return 2;
    }

    private static int sum(Optional<Integer> a, Optional<Integer> b) {
        System.out.println("判断参数是否存在： " + a.isPresent());
        System.out.println("判断参数是否存在： " + b.isPresent());
        //orElse如果值存在，返回它，否则返回默认值
        Integer val = a.orElse(new Integer(0));
        //get 获取值，值需要存在
        Integer integer = b.get();
        return val + integer;
    }
}
