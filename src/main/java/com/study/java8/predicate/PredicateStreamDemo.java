package com.study.java8.predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  13:58
 * @description:在Stream中使用Predicate
 */
public class PredicateStreamDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Vijay");
        list.add("Ramesh");
        list.add("Mahesh");

        Predicate<String> isNameEndsWithSh = s -> s.endsWith("sh");

        list.stream().filter(isNameEndsWithSh)
                .forEach(s -> System.out.println(s));
    }

}
