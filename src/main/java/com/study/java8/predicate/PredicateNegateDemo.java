package com.study.java8.predicate;

import java.util.function.Predicate;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  11:24
 * @description:
 */
public class PredicateNegateDemo {
    public static void main(String[] args) {
        Predicate<Integer> isNumberMatched = n -> n > 10 && n < 20;
        //With negate()
        Boolean result = isNumberMatched.negate().test(15);
        //false
        System.out.println(result);

        //Without negate()
        result = isNumberMatched.test(15);
        //true
        System.out.println(result);

        Predicate<String> isValidName = s -> s.length() > 5 && s.length() < 15;
        //false
        System.out.println(isValidName.negate().test("Krishna"));

        Predicate<Integer> isLessThan50 = n -> n < 50;
        //true
        System.out.println(isLessThan50.negate().test(60));

        Predicate<Integer> isGreaterThan20 = n -> n > 20;
        //false
        System.out.println(isGreaterThan20.negate().test(30));

        result = isLessThan50.and(isGreaterThan20).negate().test(25);
        //false
        System.out.println(result);
    }
}
