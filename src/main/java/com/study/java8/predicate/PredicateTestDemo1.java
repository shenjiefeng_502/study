package com.study.java8.predicate;


import java.util.function.Predicate;
/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  10:41
 * @description:test是Predicate的功能方法。它在给定的参数上评估这个谓词。
 */
public class PredicateTestDemo1 {
    public static void main(String[] args) {
        // Is username valid
        Predicate<String> isUserNameValid = u -> u != null && u.length() > 5 && u.length() < 10;
        System.out.println(isUserNameValid.test("Mahesh")); //true

        // Is password valid
        Predicate<String> isPasswordValid = p -> p != null && p.length() > 8 && p.length() < 15;
        System.out.println(isPasswordValid.test("Mahesh123")); //true

        // Word match
        Predicate<String> isWordMatched = s -> s.startsWith("Mr.");
        System.out.println(isWordMatched.test("Mr. Mahesh")); //true

        //Odd numbers
        Predicate<Integer> isEven = n -> n % 2 == 0;
        for(int i = 0 ; i < 5 ; i++) {
            System.out.println("Is "+ i + " even: " + isEven.test(i));
        }
    }
}
