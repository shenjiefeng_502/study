package com.study.java8.predicate;

import java.util.function.Predicate;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  13:22
 * @description:
 */
public class PredicateIsEqualDemo {
    public static void main(String[] args) {
        Predicate<String> hello = Predicate.isEqual("hello");
        System.out.println(hello.test("hello"));
        System.out.println(hello.test("hi"));
        System.out.println("------------------");
        Student student1 = new Student("s", 26, "male");
        Student student2 = new Student("s", 27, "male");
        Student student3 = new Student("m", 27, "male");
        Predicate<Student> equal = Predicate.isEqual(student1);
        System.out.println(equal.test(student1));
        System.out.println(equal.test(student2));
        System.out.println(equal.test(student3));
        
    }
}
