package com.study.java8.predicate;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  10:45
 * @description:
 */
public class PredicateTestDemo2 {
    public static void main(String[] args) {
        Predicate<Student> maleStudent = s -> s.getAge() >= 20 && "male".equals(s.getGender());
        Predicate<Student> femaleStudent = s -> s.getAge() > 18 && "female".equals(s.getGender());

        Function<Student, String> maleStyle = s -> "Hi, You are male and age " + s.getAge();
        Function<Student, String> femaleStyle = s -> "Hi, You are female and age " + s.getAge();

        Student s1 = new Student("Gauri", 20, "female");
        if (maleStudent.test(s1)) {
            System.out.println(s1.customShow(maleStyle));
        } else if (femaleStudent.test(s1)) {
            System.out.println(s1.customShow(femaleStyle));
        }
    }
}
