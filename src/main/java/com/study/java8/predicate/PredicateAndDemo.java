package com.study.java8.predicate;

import java.util.function.Predicate;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/18  10:57
 * @description:and是Predicate的默认方法，它返回一个组成的谓词， 表示这个谓词和其他谓词的短路逻辑AND。在评估组成的谓词时，
 * 如果这个谓词是假的，那么其他谓词将不会被评估。在错误的情况下，如果此谓词抛出错误，那么其他谓词将不会被评估。
 */
public class PredicateAndDemo {
    public static void main(String[] args) {
        Predicate<Student> isMaleStudent = s -> s.getAge() >= 20 && "male".equals(s.getGender());
        Predicate<Student> isFemaleStudent = s -> s.getAge() > 18 && "female".equals(s.getGender());
        Predicate<Student> isStudentPassed = s -> s.getMarks() >= 33;
        Predicate<String> testLength = s -> s.length() > 0;
        // Testing if male student passed.
        Student student1 = new Student("Mahesh", 22, "male", 30);
        Boolean result = isMaleStudent.and(isStudentPassed).test(student1);
//        System.out.println(result); //false
        System.out.println(testLength.test(""));
        // Testing if female student passed.
        Student student2 = new Student("Gauri", 19, "female", 40);
        result = isFemaleStudent.and(isStudentPassed).test(student2);
//        System.out.println(result); //true
    }
}
