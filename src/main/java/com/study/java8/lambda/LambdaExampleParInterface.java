package com.study.java8.lambda;


import java.util.List;
import java.util.Map;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/6/6  14:50
 * @description: 带参数的lambda函数
 */
@FunctionalInterface
public interface LambdaExampleParInterface<T> {
    
    void saveData(Map<String,Object> map, List<T> list);
}
