package com.study.java8.lambda;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  16:52
 * @description:
 */
public interface LambdaExample {
    public static void main(String[] args) {
        //Runnable接口
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("The runable now is using!");
            }
        }).start();
        new Thread(() -> System.out.println("It's a lambda function!")).start();


        //Comparator接口
        List<Integer> integers = Arrays.asList(1, 2, 3);
        Collections.sort(integers, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        //lambda
        Collections.sort(integers, (Integer o1, Integer o2) -> o1 - o2);
        //分解开
        Comparator<Integer> comparator = (Integer o1, Integer o2) -> o1 - o2;
        Collections.sort(integers, comparator);

        //Listener接口
        JButton button = new JButton();
        button.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                e.getItem();
            }
        });
        //lambda
        button.addItemListener(e -> e.getItem());

        //自定义实现method
        show(new LambdaInterface() {
            @Override
            public void f() {
                System.out.println("自定义实现");
            }
        });
        show(() -> System.out.println("自定义接口实现"));
    }

    public static void show(LambdaInterface lambdaInterface) {
        lambdaInterface.f();
    }
}
