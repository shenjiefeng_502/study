package com.study.java8.lambda;

import com.alibaba.fastjson.JSON;
import org.apache.poi.ss.formula.functions.T;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/6/6  15:39
 * @description:带参数的lambda函数
 */
public class LambdaParExample {
    private LambdaExampleParInterface parInterface;

    public void saveData(Map<String,Object> map, List<Integer> list) {
        this.parInterface = new LambdaExampleParInterface<Integer>() {
            @Override
            public void saveData(Map<String,Object> map, List<Integer> list) {
                System.out.println(JSON.toJSONString(map));
                System.out.println(JSON.toJSONString(list));
            }
        };
        parInterface.saveData(map,list);
    }

    public static void main(String[] args) {
        LambdaParExample lambdaExample = new LambdaParExample();
        HashMap<String, Object> map = new HashMap<>();
        map.put("1",1);
        List<Integer> list = Arrays.asList(1, 2);
        lambdaExample.saveData(map,list);
    }
}
