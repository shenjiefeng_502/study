package com.study.java8.lambda;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/26  09:48
 * @description:
 */
@FunctionalInterface
public interface LambdaInterface {
    void f();
}

