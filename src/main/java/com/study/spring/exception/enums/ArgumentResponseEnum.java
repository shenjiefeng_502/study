package com.study.spring.exception.enums;

import com.study.spring.exception.asserts.BusinessExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ArgumentResponseEnum implements BusinessExceptionAssert {
    /**
     * VALID_ERROR
     */
    VALID_ERROR(2001, "VALID ERROR");

    /**
     * 返回码
     */
    private int code;
    /**
     * 返回消息
     */
    private String message;
}
