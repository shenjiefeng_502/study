package com.study.spring.exception.enums;

public interface IResponseEnum {

    int getCode();
    String getMessage();
}
