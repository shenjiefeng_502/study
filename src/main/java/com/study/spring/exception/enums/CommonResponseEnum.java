package com.study.spring.exception.enums;

import com.study.spring.exception.asserts.BusinessExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/22  15:30
 * @description:
 */
@Getter
@AllArgsConstructor
public enum CommonResponseEnum implements BusinessExceptionAssert {
    /**
     * SERVER_ERROR
     */
    SERVER_ERROR(1001, "SERVER ERROR");

    /**
     * 返回码
     */
    private int code;
    /**
     * 返回消息
     */
    private String message;
}
