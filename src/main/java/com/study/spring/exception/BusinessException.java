package com.study.spring.exception;

import com.study.spring.exception.enums.IResponseEnum;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/22  15:04
 * @description:
 */
public class BusinessException extends BaseException {
    private static final long serialVersionUID = 1L;

    public BusinessException(IResponseEnum responseEnum, Object[] args, String message) {
        super(responseEnum, args, message);
    }

    public BusinessException(IResponseEnum responseEnum, Object[] args, String message, Throwable cause) {
        super(responseEnum, args, message, cause);
    }
}
