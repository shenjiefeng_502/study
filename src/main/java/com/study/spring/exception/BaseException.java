package com.study.spring.exception;

import com.study.spring.exception.enums.IResponseEnum;
import lombok.Data;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/22  15:22
 * @description:
 */
@Data
public class BaseException extends Exception{
    private IResponseEnum responseEnum;
    private String message;
    public BaseException(IResponseEnum responseEnum, Object[] args, String message) {
        
    }

    public BaseException(IResponseEnum responseEnum, Object[] args, String message, Throwable cause) {
        
    }
}
