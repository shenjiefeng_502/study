package com.study.spring.exception.asserts;

import com.study.spring.exception.BaseException;
import com.study.spring.exception.BusinessException;
import com.study.spring.exception.enums.IResponseEnum;

import java.text.MessageFormat;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/22  15:04
 * @description:
 */
public interface BusinessExceptionAssert  extends IResponseEnum, Assert {
    @Override
    default BaseException newException(Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this, args, msg);
    }

    @Override
    default BaseException newException(Throwable t, Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this, args, msg, t);
    }

}
