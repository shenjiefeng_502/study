package com.study.spring.aspect;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/19  14:23
 * @description:
 */
//@Aspect
//@Component
public class SjfAspect {
    @Before("execution(public void com.study.spring.service.spring.UserService.test())")
    public void sjfBefore(Joinpoint joinpoint){
        System.out.println("sjfBefore");
    }
}
