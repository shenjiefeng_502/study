package com.study.spring.service.spring;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  16:49
 * @description: 用户服务
 */
@Data
@Component
public class UserService implements InitializingBean {
    @Autowired
    private OrderService orderService;
    private KafkaProperties.Admin admin;

    @PostConstruct
    public void a() {
        this.admin = new KafkaProperties.Admin();
        admin.setClientId("1");
    }

    public void test() {
        System.out.println(orderService);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        
    }
}
