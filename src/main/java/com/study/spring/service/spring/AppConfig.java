package com.study.spring.service.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  17:00
 * @description:
 */
@ComponentScan("com.study")
@Configuration
@EnableTransactionManagement
public class AppConfig {
    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource1());
        return transactionManager;
    }

    private DataSource dataSource1() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:mysql://101.35.248.103:3306/bill?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true&zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true");
        dataSource.setUsername("bill");
        dataSource.setPassword("wMGX7arfwcDfm8TT");
        return dataSource;
    }

    private DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:mysql://101.35.248.103:3306/bill?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true&zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true");
        dataSource.setUsername("bill");
        dataSource.setPassword("wMGX7arfwcDfm8TT");
        return dataSource;
    }
}
