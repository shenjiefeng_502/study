package com.study.spring.service.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  16:58
 * @description:
 */
@Component
public class OrderService {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Transactional
    public void test(){
        jdbcTemplate.execute("INSERT into jdbc_test VALUES(\"111\")");
        throw new NullPointerException();
    }
}
