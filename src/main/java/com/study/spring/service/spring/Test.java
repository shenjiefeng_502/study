package com.study.spring.service.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  16:59
 * @description:spring 面试70问
 */
public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext application = new AnnotationConfigApplicationContext(AppConfig.class);
        UserService userService = (UserService) application.getBean("userService");
        UserService userService1 = (UserService) application.getBean("userService");
        UserService userService2 = (UserService) application.getBean("userService");
        userService.test();
    }
}
