package com.study.spring.service.spring_jdbc_transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  16:58
 * @description:
 */
@Component
public class SpringTransactionalService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public void test() {
        jdbcTemplate.execute("INSERT into jdbc_test VALUES(\"111\")");
        //普通对象
        a();
    }

    /**
     * 这个方法是普通对象在调用这个方法，所以不会进行aop进行事务切面
     *
     * @param
     * @return void
     * @MethodName: a
     * @auth: sjf
     * @date 2022/9/27 14:44
     */
    @Transactional(propagation = Propagation.NEVER)
    private void a() {
        jdbcTemplate.execute("INSERT into jdbc_test VALUES(\"111\")");
    }
}
