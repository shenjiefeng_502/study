package com.study.spring.service.spring_jdbc_transactional;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/14  16:59
 * @description:spring 面试70问
 */
public class SpringTransactionalTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext application = new AnnotationConfigApplicationContext();
        application.scan("com.study.spring.service.spring_jdbc_transactional");
        application.refresh();
        SpringTransactionalService springTransactionalService = (SpringTransactionalService) application.getBean("springTransactionalService");
        springTransactionalService.test();
    }
}
