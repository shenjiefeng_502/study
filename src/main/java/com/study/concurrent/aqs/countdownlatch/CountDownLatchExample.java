package com.study.concurrent.aqs.countdownlatch;


import com.study.concurrent.aqs.sync.SjfCountDownLatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/24  10:16
 * @description:CountDownLatch 的两种典型用法
 */
public class CountDownLatchExample {
    private static final int threadCount = 500;

    /**
     * @param args
     * @return void
     * @MethodName: main
     * @auth: sjf
     * @date 2022/5/24 10:44
     */
    public static void main(String[] args) throws InterruptedException {
        //主线程开始前等待n个线程执行完毕
        extracted();
        //实现多个线程开始执行任务的最大并行性
        extracted2();
    }

    /**
     * 主线程等待其他线程全部完毕继续
     *
     * @param
     * @return void
     * @MethodName: extracted
     * @auth: sjf
     * @date 2022/5/24 10:51
     */
    private static void extracted() throws InterruptedException {
        // 创建一个具有固定线程数量的线程池对象（如果这里线程池的线程数量给太少的话你会发现执行的很慢）
        ExecutorService threadPool = Executors.newFixedThreadPool(300);
        final SjfCountDownLatch countDownLatch = new SjfCountDownLatch(threadCount);
        for (int i = 0; i < threadCount; i++) {
            final int threadnum = i;
            threadPool.execute(() -> {
                try {
                    test(threadnum);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //表示一个请求已经被完成
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        threadPool.shutdown();
        System.out.println("finish");
    }

    /**
     * 实现多个线程开始执行任务的最大并行性
     *
     * @param
     * @return void
     * @MethodName: extracted2
     * @auth: sjf
     * @date 2022/5/24 10:52
     */
    private static void extracted2() throws InterruptedException {
        // 创建一个具有固定线程数量的线程池对象（如果这里线程池的线程数量给太少的话你会发现执行的很慢）
        ExecutorService threadPool = Executors.newFixedThreadPool(300);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < 300; i++) {
            final int threadnum = i;
            threadPool.execute(() -> {
                try {
                    countDownLatch.await();
                    test(threadnum);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        countDownLatch.countDown();
        threadPool.shutdown();
        System.out.println("finish");
    }

    private static void test(int threadnum) throws InterruptedException {
        Thread.sleep(50);// 模拟请求的耗时操作
        System.out.println("threadnum:" + threadnum);
        Thread.sleep(50);// 模拟请求的耗时操作
    }

}
