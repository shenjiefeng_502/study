package com.study.concurrent.aqs.sync;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/23  16:14
 * @description:共享资源式锁实现
 */
public class SjfSharedSync {
    private final MySharedSync mySharedSync;

    public SjfSharedSync(int permits) {
        this.mySharedSync = new MySharedSync(permits);
    }
    public void acquire(int permits){
        if (permits<0){
            throw new IllegalArgumentException();
        }
        mySharedSync.acquireShared(permits);
    }
    public void release(int permits){
        mySharedSync.releaseShared(permits);
    }

    final class MySharedSync extends AbstractQueuedSynchronizer {
        //初始化父类时的入参作为state数量
        public MySharedSync(int permits) {
            setState(permits);
        }

        /**
         * 尝试抢锁，返回负数表示数量不足，0或正整数表示锁的剩余数量
         *
         * @param arg
         * @return
         */
        @Override
        protected int tryAcquireShared(int arg) {
            //自旋
            for (; ; ) {
                int available = getState();
                int remaining = available - arg;
                //小于0时返回的是负数，表示锁不足
                //交换成功返回的表示锁剩余数量
                if (remaining < 0 || compareAndSetState(available, remaining)) {
                    return remaining;
                }
            }
        }

        @Override
        protected boolean tryReleaseShared(int arg) {
            for (; ; ) {
                int current = getState();
                int next = current + arg;
                if (next < current) {
                    throw new Error("Maximum permit count exceeded");
                }
                if (compareAndSetState(current, next)) {
                    return true;
                }
            }
        }
    }

}
