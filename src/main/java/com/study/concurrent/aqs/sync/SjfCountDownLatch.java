package com.study.concurrent.aqs.sync;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/24  11:11
 * @description:
 */
public class SjfCountDownLatch {
    private final SjfCountDownLatchSync sjfCountDownLatchSync;

    public SjfCountDownLatch(Integer state) {
        if (state < 0) {
            throw new IllegalArgumentException("count < 0");
        }
        this.sjfCountDownLatchSync = new SjfCountDownLatchSync(state);
    }

    public void countDown() {
        sjfCountDownLatchSync.releaseShared(1);
    }

    public void await() throws InterruptedException {
        sjfCountDownLatchSync.acquireSharedInterruptibly(1);
    }

    private class SjfCountDownLatchSync extends AbstractQueuedSynchronizer {

        public SjfCountDownLatchSync(int state) {
            setState(state);
        }


        int getCount() {
            return getState();
        }

        protected int tryAcquireShared(int acquires) {
            return (getState() == 0) ? 1 : -1;
        }

        protected boolean tryReleaseShared(int releases) {
            // Decrement count; signal when transition to zero
            for (; ; ) {
                int c = getState();
                if (c == 0)
                    return false;
                int nextc = c - 1;
                if (compareAndSetState(c, nextc))
                    return nextc == 0;
            }
        }
    }

}
