package com.study.concurrent.aqs.sync;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/23  16:14
 * @description:独占资源式锁实现
 */
public class SjfExclusiveSync extends AbstractQueuedSynchronizer {
    @Override
    public boolean tryAcquire(int arg) {
        //获取当前线程
        Thread t = Thread.currentThread();
        //拿到状态
        int state = getState();
        if (state == 0) {
            if (compareAndSetState(0, arg)) {
                setExclusiveOwnerThread(t);
                return true;
                //重入设计
            } else if (getExclusiveOwnerThread() == t) {
                setState(state + 1);
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean tryRelease(int arg) {
        //  锁的获取和释放是一一对应的，调用次方法的线程一定是当前的线程
        if (Thread.currentThread() != getExclusiveOwnerThread()) {
            throw new RuntimeException();
        }

        int state = getState() - arg;

        boolean flag = false;

        if (state == 0) {
            setExclusiveOwnerThread(null);
            flag = true;
        }
        setState(state);
        return flag;

    }

}
