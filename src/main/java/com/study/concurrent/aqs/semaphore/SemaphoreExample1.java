package com.study.concurrent.aqs.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/4/22  15:28
 * @description: 信号量
 */
public class SemaphoreExample1 {
    // 请求的数量
    private static final int threadCount = 550;

    /**
     * Semaphore
     * 执行 acquire() 方法阻塞，直到有一个许可证可以获得然后拿走一个许可证；
     * 每个 release 方法增加一个许可证，这可能会释放一个阻塞的 acquire() 方法。
     * 然而，其实并没有实际的许可证这个对象，Semaphore 只是维持了一个可获得许可证的数量。
     * Semaphore 经常用于限制获取某种资源的线程数量。
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        // 创建一个具有固定线程数量的线程池对象（如果这里线程池的线程数量给太少的话你会发现执行的很慢）
        ExecutorService threadPool = Executors.newFixedThreadPool(300);
        // 一次只能允许执行的线程数量。
        /**
         * Semaphore有公平模式和非公平模式两种构造方式
         */
        final Semaphore semaphore = new Semaphore(20);
        //默认为非公平模式.可以切换为true
        final Semaphore unFairSemaphore = new Semaphore(20, false);

        for (int i = 0; i < threadCount; i++) {
            final int threadnum = i;
            // Lambda 表达式的运用
            threadPool.execute(() -> {
                try {
                    //该方法如果获取不到许可就立即返回 false。
                    //可以一次获取拿取和释放多个令牌，虽然没什么意义
                    if (threadnum % 2 == 0) {
                        semaphore.tryAcquire(2);
                        // 获取2个许可，所以可运行线程数量为20/2=10
                        semaphore.acquire(2);
                        test(threadnum);
                        // 释放2个许可
                        semaphore.release(2);
                    } else {
                        semaphore.tryAcquire();
                        // 获取一个许可，所以可运行线程数量为20/1=20
                        semaphore.acquire();
                        test(threadnum);
                        // 释放一个许可
                        semaphore.release();
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            });
        }
        threadPool.shutdown();
        System.out.println("finish");
    }

    public static void test(int threadnum) throws InterruptedException {
        Thread.sleep(1000);// 模拟请求的耗时操作
        System.out.println("threadnum:" + threadnum);
        Thread.sleep(1000);// 模拟请求的耗时操作
    }
}
