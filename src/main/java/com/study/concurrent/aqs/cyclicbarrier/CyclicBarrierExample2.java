package com.study.concurrent.aqs.cyclicbarrier;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/24  14:33
 * @description:循环栅栏（高级构造方式）
 */
public class CyclicBarrierExample2 {

    public static final int threadCount = 100;
    //高级构造方式，可以放入Runnable
    public static final CyclicBarrier cyclicBarrier = new CyclicBarrier(5, () -> {
        System.out.println("------当线程数达到之后，优先执行------");
    });

    /**
     * 更高级的构造函数 CyclicBarrier(int parties, Runnable barrierAction)，
     * 用于在线程到达屏障时，优先执行 barrierAction，
     * @MethodName: main
     * @param args
     * @return void
     * @auth: sjf
     * @date  2022/5/24 14:50
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        for (int i = 0; i < threadCount; i++) {
            final int threadNum = i;
            Thread.sleep(1000);
            threadPool.execute(() -> {
                try {
                    test(threadNum);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        threadPool.shutdown();
    }

    private static void test(int threadNum) {
        System.out.println("threadnum:" + threadNum + "is ready");
        try {
            /**等待60秒，保证子线程完全执行结束*/
            cyclicBarrier.await(60, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("-----CyclicBarrierException------");
        }
        System.out.println("threadnum:" + threadNum + "is finish");
    }
}
