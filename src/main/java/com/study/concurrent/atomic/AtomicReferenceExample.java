package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  11:17
 * @description:引用类型原子类
 */
public class AtomicReferenceExample {

    public static void main(String[] args) {
        AtomicReference<Person> ar = new AtomicReference<>();
        Person sjf = new Person("sjf", 20);
        ar.set(sjf);
        Person oldSjf = new Person("sjf", 21);
        ar.compareAndSet(sjf, oldSjf);
        System.out.println(ar.get().getName());
        System.out.println(ar.get().getAge());
    }

    static class Person {
        private String name;
        private int age;

        public Person(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
