package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  10:04
 * @description:
 */
public class AtomicLongExample {
    public static void main(String[] args) {
        AtomicLong atomicLong = new AtomicLong(1);
        long tempVal = atomicLong.get();
        tempVal = atomicLong.getAndSet(3L);
        System.out.println(tempVal);
        tempVal = atomicLong.get();
        System.out.println(tempVal);
        tempVal = atomicLong.getAndIncrement();
        System.out.println(tempVal);
        tempVal = atomicLong.getAndAdd(5);
        System.out.println(tempVal);

    }
}
