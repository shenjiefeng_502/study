package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/24  16:53
 * @description:
 */
public class AtomicIntegerExample {
    public static void main(String[] args) {
        AtomicInteger integer = new AtomicInteger(0);
        int temvalue = 0;
        temvalue = integer.getAndSet(3);
        System.out.println(temvalue);
        temvalue = integer.get();
        System.out.println(temvalue);
        temvalue = integer.getAndIncrement();
        System.out.println(temvalue);
        temvalue = integer.getAndAdd(5);
        System.out.println(temvalue);
    }
}
