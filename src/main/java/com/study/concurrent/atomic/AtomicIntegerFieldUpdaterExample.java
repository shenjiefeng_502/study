package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  14:15
 * @description:对象属性修改原子类
 */
public class AtomicIntegerFieldUpdaterExample {
    public static void main(String[] args) {
        AtomicIntegerFieldUpdater<User> a = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");
        User java = new User("java", 22);
        System.out.println(a.getAndIncrement(java));
        System.out.println(a.get(java));
    }
}

class User {
    private String name;
    public volatile int age;

    public User(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
