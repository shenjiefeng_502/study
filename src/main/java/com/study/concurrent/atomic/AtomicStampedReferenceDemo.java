package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  13:19
 * @description:原子更新带有版本号的引用类型
 */
public class AtomicStampedReferenceDemo {
    public static void main(String[] args) {
        //实例化、取当前值和stamp值
        final Integer initRef = 0, initialStamp = 0;
        AtomicStampedReference<Integer> asr = new AtomicStampedReference<>(initRef, initialStamp);
        System.out.println("currentVal= " + asr.getReference() + ", currentStamp = " + asr.getStamp());
        final Integer newReference = 666, newStamp = 999;
        //cas
        final boolean casResult = asr.compareAndSet(initRef, newReference, initialStamp, newStamp);
        System.out.println("currentValue=" + asr.getReference()
                + ", currentStamp=" + asr.getStamp()
                + ", casResult=" + casResult);
        //获取当前的值和当前是stamp值
        int[] arr = new int[1];
        final Integer currentValue = asr.get(arr);
        final int currentStamp = arr[0];
        System.out.println("currentValue = " + currentValue + ", currentStamp = " + currentStamp);

        //单独设置stamp值
        final boolean attemptStampResult = asr.attemptStamp(newReference, 88);
        System.out.println("currentValue=" + asr.getReference()
                + ", currentStamp=" + asr.getStamp()
                + ", attemptStampResult=" + attemptStampResult);
        // 重新设置当前值和 stamp 值
        asr.set(initRef, initialStamp);
        System.out.println("currentValue=" + asr.getReference() + ", currentStamp=" + asr.getStamp());

        // [不推荐使用，除非搞清楚注释的意思了] weak compare and set
        // 困惑！weakCompareAndSet 这个方法最终还是调用 compareAndSet 方法。[版本: jdk-8u191]
        // 但是注释上写着 "May fail spuriously and does not provide ordering guarantees,
        // so is only rarely an appropriate alternative to compareAndSet."
        // todo 感觉有可能是 jvm 通过方法名在 native 方法里面做了转发
        final boolean wCasResult = asr.weakCompareAndSet(initRef, newReference, initialStamp, newStamp);
        System.out.println("currentValue=" + asr.getReference()
                + ", currentStamp=" + asr.getStamp()
                + ", wCasResult=" + wCasResult);
    }
}
