package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/25  11:08
 * @description:
 */
public class AtomicIntegerArrayExample {
    public static void main(String[] args) {
        int temVal = 0;
        int[] ints = {1, 2, 3, 4, 5, 6};
        AtomicIntegerArray array = new AtomicIntegerArray(ints);
        for (int i = 0; i < ints.length; i++) {
            System.out.println(array.get(i));
        }
        temVal = array.getAndSet(0, 2);
        System.out.println("temVal:" + temVal + "; i:" + array);
        temVal = array.getAndIncrement(0);
        System.out.println("temVal:" + temVal + "; i:" + array);
        temVal = array.getAndAdd(0, 5);
        System.out.println("temVal:" + temVal + "; i:" + array);
    }
}
