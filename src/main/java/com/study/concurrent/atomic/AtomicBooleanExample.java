package com.study.concurrent.atomic;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/5/24  16:53
 * @description:
 */
public class AtomicBooleanExample {
    public static void main(String[] args) {
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        boolean temvalue;
        temvalue = atomicBoolean.getAndSet(false);
        System.out.println(temvalue);
        temvalue = atomicBoolean.get();
        System.out.println(temvalue);
        atomicBoolean.lazySet(true);
        temvalue = atomicBoolean.get();
        System.out.println(temvalue);
        System.out.println(temvalue);
    }
}
