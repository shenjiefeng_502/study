package com.study.concurrent.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/8/18  16:09
 * @description:用于异步编程
 */
public class CompletableFutureTest {
    /**
     * 线程池
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = new CompletableFuture<>();
        //手动完成
        future.complete("s");
        //获取结果get()方法会一直阻塞到Future完成。
        String s = future.get();
        //异步无返回计算
        CompletableFuture<Void> async = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                System.out.println("异步无返回计算");
            }
        });
        async.get();
        //异步无返回lambda
        CompletableFuture<Void> lambdaAsync = CompletableFuture.runAsync(() -> {
            System.out.println("异步无返回lambda");
        });
        lambdaAsync.get();
        //异步带返回计算
        CompletableFuture<String> supplyAsync = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                return "res";
            }
        });
        String result = supplyAsync.get();
        System.out.println(result);
        //异步带返回lambda
        CompletableFuture<String> lambdaSupplyAsync = CompletableFuture.supplyAsync(() -> {
            return "res";
        });
        String lambdaRes = lambdaSupplyAsync.get();
        System.out.println(lambdaRes);
        //链式异步连续调用
        CompletableFuture<String> welcomeText = CompletableFuture.supplyAsync(() -> {
            return "sjf";
        }).thenApply((name) -> {
            return "hello " + name;
        }).thenApply(greeting -> {
            return greeting + " ,welcome";
        });
        System.out.println(welcomeText.get());
    }
}
