package com.study.jvm;

import com.study.excel.poi.WordUtils;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/7  13:52
 * @description: 测试javap
 */
public class Math {
    public static final int initData = 666;
    public static WordUtils.User user = new WordUtils.User();

    public int compute() {
        int a = 1;
        int b = 2;
        int c = (a + b) * 10;
        return c;
    }

    public static void main(String[] args) {
        Math math = new Math();
        int compute = math.compute();
        System.out.println("end");
    }
}
