package com.study.jvm;

import java.util.ArrayList;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/7  15:31
 * @description: 使用java自带工具堆内存分析
 */
public class HeapTest {
    byte[] a = new byte[1024 * 100]; //100kb

    public static void main(String[] args) throws InterruptedException {
        ArrayList<HeapTest> list = new ArrayList<>();
        while (true) {
            list.add(new HeapTest());
            Thread.sleep(5);
        }
    }
}
