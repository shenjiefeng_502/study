package com.study.mybatis.bean;

import lombok.Data;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/7/29  10:42
 * @description: yonghu
 */
@Data
public class User {
    private String userId;
    private String username;
    private String mobile;
    private String password;
    private String createTime;
}
