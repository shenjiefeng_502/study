package com.study.mybatis.bean;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/8/1  10:21
 * @description:
 */
@Data
public class Blog {
    private String name;
    private User user;
    private List<String> list;
    private Map<String, String> map;

}
