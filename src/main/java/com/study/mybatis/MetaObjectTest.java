package com.study.mybatis;

import com.study.mybatis.bean.Blog;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.property.PropertyTokenizer;
import org.apache.ibatis.reflection.wrapper.BeanWrapper;
import org.apache.ibatis.session.Configuration;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/8/1  10:20
 * @description:
 */
public class MetaObjectTest {
    @Test
    public void test() {
        Blog blog = new Blog();
        Configuration configuration = new Configuration();
        MetaObject metaObject = configuration.newMetaObject(blog);
        //直接操作属性
        metaObject.setValue("name", "blogname");
        //自动创建属性对象，操作属性对象的属性
        metaObject.setValue("user.username", "username");
        //自动查找属性名，支持下划线驼峰功能 true则转驼峰
        System.out.println(metaObject.findProperty("user.mobile", true));
        //数组属性不能自动创建
        ArrayList<Object> value = new ArrayList<>();
        value.add("1");
        metaObject.setValue("list", value);
        metaObject.setValue("list[0]", "12");
        System.out.println(metaObject.getValue("list[0]"));
        metaObject.setValue("map", new HashMap<>());
        metaObject.setValue("map[red]", "red");
        System.out.println(metaObject.getValue("map[red]"));
    }

    @Test
    public void beanWrapperTest() {
        Object blog = new Blog();
        Configuration configuration = new Configuration();
        MetaObject metaObject = configuration.newMetaObject(blog);
        //直接操作属性
        metaObject.setValue("name", "blogname");
        //自动创建属性对象，操作属性对象的属性
        metaObject.setValue("user.username", "username");
        //自动查找属性名，支持下划线驼峰功能 true则转驼峰
        System.out.println(metaObject.findProperty("user.mobile", true));
        //数组属性不能自动创建
        ArrayList<Object> value = new ArrayList<>();
        value.add("1");
        metaObject.setValue("list", value);
        metaObject.setValue("list[0]", "12");
        System.out.println(metaObject.getValue("list[0]"));
        metaObject.setValue("map", new HashMap<>());
        metaObject.setValue("map[red]", "red");
        System.out.println(metaObject.getValue("map[red]"));
        BeanWrapper beanWrapper = new BeanWrapper(metaObject, blog);
        beanWrapper.get(new PropertyTokenizer("list[0]"));
        beanWrapper.get(new PropertyTokenizer("list")); 
    }
}
