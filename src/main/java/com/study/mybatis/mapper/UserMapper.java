package com.study.mybatis.mapper;

import com.study.mybatis.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/7/29  10:46
 * @description:
 */
@Mapper
public interface UserMapper {
    @Select(" select * from tb_user where user_id = #{userId}")
    User selectUser(@Param("userId") Integer id);
}
