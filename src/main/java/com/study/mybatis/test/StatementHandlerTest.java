package com.study.mybatis.test;

import com.alibaba.fastjson.JSON;
import com.study.mybatis.bean.User;
import org.apache.ibatis.executor.SimpleExecutor;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.jdbc.JdbcTransaction;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/7/29  10:05
 * @description: jdbc处理器
 */
public class StatementHandlerTest {
    private final String url = "jdbc:mysql://101.35.248.103:3306/api_xcx?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true&zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true";
    private final String userName = "api_xcx";
    private final String password = "py8dfBNLBRRzbJEp";
    private Configuration configuration;
    private Transaction tran;

    @Before
    private void init() throws IOException, SQLException {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        SqlSessionFactoryBuilder factoryBuilder = new SqlSessionFactoryBuilder();
        String resource = "/mybatis-config.xml";
        InputStream inputStream = StatementHandlerTest.class.getResourceAsStream(resource);
        SqlSessionFactory factory = factoryBuilder.build(inputStream);
        configuration = factory.getConfiguration();
        configuration.addMappers("com.study.mybatis.mapper");
        Connection connection = DriverManager.getConnection(url, userName, password);
        tran = new JdbcTransaction(connection);
    }

    @Test
    public void simpleStatementHandlerTest() throws SQLException, IOException {    
        SimpleExecutor executor = new SimpleExecutor(configuration, tran);
        MappedStatement ms=configuration.getMappedStatement("com.study.mybatis.mapper.UserMapper.selectUser");
        List<User> objects = executor.doQuery(ms, 1, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER, ms.getBoundSql(1));
        Object o = objects.get(0);
        System.out.println(JSON.toJSONString(o));
    }
}
