package com.study.thread;

import java.util.concurrent.Callable;

/**
 * @program: ListStudyDemo
 * @ClassName FutureCallable
 * @description:
 * @author: sjf
 * @create: 2020-05-11 14:51
 * @Version 1.0
 **/
public class FutureCallable implements Callable<Integer> {
    //给定泛型确定返回类型
    @Override
    public Integer call() throws Exception {
        System.out.println("实现callable接口实现线程并返回返回值");
        return 1;
    }
}
