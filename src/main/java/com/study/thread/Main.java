package com.study.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @program: ListStudyDemo
 * @ClassName Main
 * @description:
 * @author: sjf
 * @create: 2020-05-11 14:52
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        Thread1 thread1 = new Thread1();
        new Thread(thread1).start();
        RunnableThread runnableThread = new RunnableThread();
        new Thread(runnableThread).start();
        FutureCallable futureCallable = new FutureCallable();
        FutureTask<Integer> futureTask = new FutureTask<>(futureCallable);
        new Thread(futureTask,"有返回值的线程").start();
        //实质上还是以Callable对象来创建并启动线程
        try {
            Integer integer = futureTask.get();
            System.out.println(integer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
