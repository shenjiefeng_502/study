package com.study.thread;

public class ThreadLocalDemo {
    private static ThreadLocal<Integer> seqNum = new ThreadLocal<Integer>() {
        @Override
        public Integer initialValue() {
            return 0;
        }
    };

    public ThreadLocal<Integer> getThreadLocal() {
        return seqNum;
    }

    public int getNext() {
        getThreadLocal().set(getThreadLocal().get() + 1);
        return getThreadLocal().get();
    }

    private static class TestClient extends Thread {
        private ThreadLocalDemo sn;

        public TestClient(ThreadLocalDemo sn) {
            this.sn = sn;
        }

        @Override
        public void run() {
            for (int i = 0; i < 3; i++) {
                System.out.println(Thread.currentThread().getName() + "--" + sn.getNext());
            }
        }
    }

    public static void main(String[] args) {
        ThreadLocalDemo demo1 = new ThreadLocalDemo();
        TestClient t1 = new TestClient(demo1);
        TestClient t2 = new TestClient(demo1);
        TestClient t3 = new TestClient(demo1);
        t1.start();
        t2.start();
        t3.start();
    }
}
