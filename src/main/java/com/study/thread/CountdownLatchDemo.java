package com.study.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static java.lang.Thread.sleep;

public class CountdownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        Executor executor =  Executors.newFixedThreadPool(2);
        CountDownLatch latch = new CountDownLatch(2);
        executor.execute(()->{
            try {
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("子线程执行完成");
            latch.countDown();
        });
        System.out.println("主线程执行完成");
        latch.countDown();
        latch.await();
        System.out.println("计数完成");
    }
}
