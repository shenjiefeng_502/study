package com.study.thread;

public class NoVIsibilty {
    private static boolean ready;
    private static int num;
    private static class ReaderThread extends Thread{

        @Override
        public void run(){
            while (!ready){
                Thread.yield();
            }
            System.out.println(num);
        }

        public static void main(String[] args) {
            ReaderThread readerThread = new ReaderThread();
            readerThread.start();
            num=42;
            ready=true;
        }
    }

}
