package com.study.thread;

/**
 * @program: ListStudyDemo
 * @ClassName RunnableThread
 * @description:
 * @author: sjf
 * @create: 2020-05-11 14:50
 * @Version 1.0
 **/
public class RunnableThread implements Runnable {
    @Override
    public void run() {
        System.out.println("实现Runnable接口实现线程");
    }
}
