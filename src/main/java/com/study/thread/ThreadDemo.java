package com.study.thread;

public class ThreadDemo implements Runnable{

    public static void main(String[] args) throws InterruptedException {
        ThreadDemo threadDemo = new ThreadDemo();
        Thread thread = new Thread(threadDemo, "demo");
        System.out.println("thread start");
        thread.start();
        Thread.sleep(3000);
        System.out.println("Interrupting thread");
        thread.interrupt();
        System.out.println("线程是否中断: "+thread.isInterrupted());
        Thread.sleep(3000);
        System.out.println("stop application");
    }
    @Override
    public void run() {
        boolean b = false;
        do {
            long currentTimeMillis = System.currentTimeMillis();
            while ((System.currentTimeMillis()-currentTimeMillis) <1000L){
            }
            System.out.println("My thread exiting under request ....");
            if (Thread.currentThread().isInterrupted()){
                break;
            }
        }while (!b);
    }
}
