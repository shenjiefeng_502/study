package com.study.thread;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput) // 吞吐量
@OutputTimeUnit(TimeUnit.MILLISECONDS) // 结果所使用的时间单位
@State(Scope.Thread) // 每个测试线程分配一个实例
@Fork(2) // Fork进行的数目
@Warmup(iterations = 4) // 先预热4轮
@Measurement(iterations = 10) // 进行10轮测试
public class BenchMark {

    @Param({"85"}) // 定义四个参数，之后会分别对这四个参数进行测试
    private int n;

    //空参数map
    private HashMap<Integer, Integer> nullMap;
    //定义初始化大小的map
    private HashMap<Integer, Integer> intMap;

    @Setup(Level.Trial) // 初始化方法，在全部Benchmark运行之前进行
    public void init() {
        nullMap = new HashMap<Integer, Integer>();
        intMap = new HashMap<Integer, Integer>(128);
    }

    @Benchmark
    public void arrayTraverse() {
        for (int i = 0; i < n; i++) {
            nullMap.put(i, i);
        }
    }

    @Benchmark
    public void listTraverse() {
        for (int i = 0; i < n; i++) {
            intMap.put(i, i);
        }
    }

    // 结束方法，在全部Benchmark运行之后进行
    @TearDown(Level.Trial)
    public void arrayRemove() {
        for (int i = 0; i < n; i++) {
            nullMap.remove(i);
            intMap.remove(i);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(BenchMark.class.getSimpleName()).build();
        new Runner(options).run();
    }
}
