package com.study.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2022/9/26  10:29
 * @description:
 */
public class DateUtils {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static String getDataDate(Integer years, Integer months, Integer days) {
        try {
            Date date = sdf.parse(years + "-" + months + "-" + days);
            return sdf.format(date.getTime());
        } catch (Exception e) {

        }
        return "";
    }
}
