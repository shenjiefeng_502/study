package com.study.utils;


import org.springframework.http.HttpStatus;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2023/12/13  17:54
 * @description:
 */
public class HttpStatusException extends Exception {
    private HttpStatus httpStatus;


    private String result;

    public HttpStatusException(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatusException(HttpStatus httpStatus, String result) {
        this.httpStatus = httpStatus;

    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getResult() {
        return result;
    }
}
