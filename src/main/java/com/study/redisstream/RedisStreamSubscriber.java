package com.study.redisstream;

import com.alibaba.excel.util.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RStream;
import org.redisson.api.RedissonClient;
import org.redisson.api.StreamMessageId;
import org.redisson.api.stream.StreamAddArgs;
import org.redisson.api.stream.StreamCreateGroupArgs;
import org.redisson.api.stream.StreamReadGroupArgs;
import org.redisson.client.RedisException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
@Component
public class RedisStreamSubscriber {

    @Autowired
    private RedissonClient redissonClient;

    @PostConstruct
    public void init() {
        HashMap<String, String> map = new HashMap<>();
        map.put("boxId", "1205");
        addEventToStream(RedisConstant.HEARTBEAT_INFO, map);
        createStreamAndConsumerGroup(RedisConstant.HEARTBEAT_INFO, RedisConstant.HEARTBEAT_INFO);
        consumeMessages();
    }

    // 创建流和消费者组，如果它们还不存在
    public void createStreamAndConsumerGroup(String streamName, String groupName) {
        RStream<String, String> stream = redissonClient.getStream(streamName);
        try {
            // 尝试创建消费者组，如果流不存在，它将被自动创建
            stream.createGroup(StreamCreateGroupArgs.name(groupName));
        } catch (RedisException e) {
            // 如果消费者组已经存在，我们将得到一个错误
            if (e.getMessage().contains("BUSYGROUP Consumer Group name already exists")) {
                log.info("Consumer group '{}' already exists for stream '{}'", groupName, streamName);
            } else {
                // 如果是其他错误，记录日志
                log.error("Error creating consumer group '{}' for stream '{}'", groupName, streamName, e);
            }
        }
    }

    public void consumeMessages() {

        while (true) {
            // 使用 StreamReadGroupArgs 构造函数来设置参数
            Map<StreamMessageId, Map<String, String>> messageIdMapMap = readEventsFromStream(
                    RedisConstant.HEARTBEAT_INFO, RedisConstant.HEARTBEAT_INFO, "0");
            if (!CollectionUtils
                    .isEmpty(messageIdMapMap)) {
                Set<Map.Entry<StreamMessageId, Map<String, String>>> entries = messageIdMapMap.entrySet();
                for (Map.Entry<StreamMessageId, Map<String, String>> entry : entries) {
                    StreamMessageId streamMessageId = entry.getKey();
                    Map<String, String> value = entry.getValue();
                    log.info("id0:{}", streamMessageId.getId0());
                    log.info("id1:{}", streamMessageId.getId1());
                    log.info("id:{}", streamMessageId.toString());
                    log.info("value:{}", value.toString());
                    acknowledgeEvent(RedisConstant.HEARTBEAT_INFO, RedisConstant.HEARTBEAT_INFO, streamMessageId);
                }
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        }
    }


    // 添加事件到流
    public StreamMessageId addEventToStream(String streamName, Map<String, String> event) {
        try {
            RStream<String, String> stream = redissonClient.getStream(streamName);
            StreamAddArgs<String, String> entries = StreamAddArgs.entries(event);
            entries.trim().maxLen(1000);
            return stream.add(entries);
        } catch (Exception e) {
            log.error("Error adding event to stream", e);
            return null;
        }
    }

    // 读取事件
    public Map<StreamMessageId, Map<String, String>> readEventsFromStream(String streamName, String groupName, String consumerName) {
        try {
            RStream<String, String> stream = redissonClient.getStream(streamName);
            Map<StreamMessageId, Map<String, String>> messageIdMapMap = stream.readGroup(streamName, groupName, StreamReadGroupArgs.neverDelivered());
            return messageIdMapMap;
        } catch (Exception e) {
            log.error("Error reading events from stream", e);
            return null;
        }
    }

    // 确认事件处理完成
    public void acknowledgeEvent(String streamName, String groupName, StreamMessageId messageId) {
        try {
            RStream<String, String> stream = redissonClient.getStream(streamName);
            stream.ack(groupName, messageId);
        } catch (Exception e) {
            log.error("Error acknowledging event", e);
        }
    }


    // 读取并自动确认事件
    public Map<StreamMessageId, Map<String, String>> readAndAcknowledgeEventsFromStream(String streamName, String groupName, String consumerName) {
        Map<StreamMessageId, Map<String, String>> messages = readEventsFromStream(streamName, groupName, consumerName);
        if (messages != null) {
            messages.keySet().forEach(messageId -> acknowledgeEvent(streamName, groupName, messageId));
        }
        return messages;
    }

    // 允许自定义 StreamAddArgs
    public StreamMessageId addEventToStreams(String streamName, Map<String, String> event) {
        try {
            RStream<String, String> stream = redissonClient.getStream(streamName);
            // 创建 StreamAddArgs 实例并设置条目
            StreamAddArgs<String, String> streamAddArgs = StreamAddArgs.<String, String>entries(event);
            // 添加事件到流
            return stream.add(streamAddArgs);
        } catch (Exception e) {
            log.error("Error adding event to stream", e);
            return null;
        }
    }

    // 允许自定义 StreamReadGroupArgs
    public Map<StreamMessageId, Map<String, String>> readEventsFromStream(String streamName, String groupName, String consumerName, StreamReadGroupArgs readGroupArgs) {
        try {
            RStream<String, String> stream = redissonClient.getStream(streamName);
            return stream.readGroup(groupName, consumerName, readGroupArgs);
        } catch (Exception e) {
            log.error("Error reading events from stream with custom StreamReadGroupArgs", e);
            return null;
        }
    }

}
