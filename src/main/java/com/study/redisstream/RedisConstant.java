package com.study.redisstream;

public class RedisConstant {
    public static final String CHANGE_AVAILABILITY = "changeAvailability";
    public static final String HEARTBEAT_INFO = "ChargeBoxHeartbeat";
    public static final String STATUS_INFO = "statusChange";
    public static final String START_CHARGE_INFO = "startCharge";
    public static final String METER_VALUES_INFO = "insertMeterValues";
    public static final String STOP_CHARGE_INFO = "stopCharge";
    public static final String REMOTE_START_CHARGE = "remoteStartCharge";
    public static final String REMOTE_STOP_CHARGE = "remoteStopCharge";

    public static final String AUTH_INFO = "tagAuth";
}
