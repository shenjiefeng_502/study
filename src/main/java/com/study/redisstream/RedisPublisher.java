package com.study.redisstream;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
public class RedisPublisher {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void sendMessage(String streamKey, String message) {
        log.info("发送stream{}，message{}", streamKey, message);
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("vo",message);
        try {
            stringRedisTemplate.opsForStream().add(streamKey, stringStringHashMap);
        } catch (Exception e) {
            log.info("stream：{}", JSON.toJSONString(e));
        }
    }

    public void publishMessage(String topic, String message) {
        log.info("推送订阅消息;topic:{},message:{}", topic, message);
        stringRedisTemplate.convertAndSend(topic, message);
    }

    public String publishMessageWithResult(String topic, String key, String message) {
        log.info("推送订阅消息;topic:{},message:{}", topic, message);
        stringRedisTemplate.convertAndSend(topic, message);
        return getResult(topic, key);
    }

    private String getResult(String topic, String key) {
        stringRedisTemplate.afterPropertiesSet();
        return null;
    }
}