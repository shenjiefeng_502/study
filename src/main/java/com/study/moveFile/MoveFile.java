package com.study.moveFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Move file
 *
 * @author sjf
 * @version 1.0
 * @date 2021-12-22 15:10:23
 */
public class MoveFile {
    public static void main(String[] args) {
        copy("/Users/shenjiefeng/IdeaProjects/untitled/src/main/java/moveFile", "/Users/shenjiefeng/IdeaProjects/untitled/src/main/java/LeetCode/字符转数组", "pdf");
    }

    /**
     * Copy
     *
     * @param srcPathStr the src path str
     * @param movePath   the move path
     * @param fileType   the file type
     * @author sjf
     * @date 2021-12-23 11:29:49
     */
    private static void copy(String srcPathStr, String movePath, String fileType) {
        //获取源文件的名称
        //目标文件地址
        File file = new File(srcPathStr);
        if (file.isDirectory()) {
            String[] list = file.list();
            for (String path : list) {
                String nowPath = srcPathStr + File.separator + path;
                copy(srcPathStr, nowPath, movePath, fileType);
            }
        } else {
            String fileName = srcPathStr.substring(srcPathStr.lastIndexOf("/") + 1);
            //源文件地址
            String newMovePath = movePath + File.separator + fileName;
            copyFile(srcPathStr, movePath, newMovePath, fileType);
        }
    }


    /**
     * 递归Copy(如果是文件夹则递归移动保留原文件路径)
     * 例如
     * 需要移动的文件夹为  /User/s/file
     * 需要移动的文件为   /User/s/file/java/人月传说.pdf
     * 移动到的文件夹为   /User/s/book
     * 则文件位置为      /User/s/bokk/java/人月传说.pdf
     *
     * @param srcPathStr 需要移动的文件或文件夹
     * @param nowPath    当前相对路径
     * @param movePath   目标路径
     * @param fileType   文件类型可以为空
     * @author sjf
     * @date 2021-12-23 11:19:29
     */
    static void copy(String srcPathStr, String nowPath, String movePath, String fileType) {
        //获取源文件的名称
        //目标文件地址
        File file = new File(nowPath);
        if (file.isDirectory()) {
            String[] list = file.list();
            for (String path : list) {
                String s = nowPath + File.separator + path;
                copy(srcPathStr, s, movePath, fileType);
            }
        } else {
            String newFileName = nowPath.substring(nowPath.lastIndexOf("/") + 1);
            String newFilePath = nowPath.replace(srcPathStr, "").replace(newFileName, "");
            //源文件地址
            String newMovePath = movePath + newFilePath + newFileName;
            String fileSrc = newMovePath.replace(newFileName, "");
            copyFile(nowPath, fileSrc, newMovePath, fileType);
        }
    }

    /**
     * Copy file
     *
     * @param filePath    the file path
     * @param newMovePath the new move path
     * @param fileType
     * @author sjf
     * @date 2021-12-23 11:29:43
     */
    private static void copyFile(String filePath, String fileSrc, String newMovePath, String fileType) {
        String pathFileType = filePath.substring(filePath.lastIndexOf(".") + 1);
        if (fileType != null && fileType != "" && !pathFileType.equals(fileType)) {
            return;
        }
        File movePathF = new File(fileSrc);
        if (!movePathF.exists()) {
            movePathF.mkdirs();
        }
        try {
            //创建输入流对象
            FileInputStream fis = new FileInputStream(filePath);
            //创建输出流对象
            FileOutputStream fos = new FileOutputStream(newMovePath);
            //创建搬运工具
            byte datas[] = new byte[1024 * 8];
            //创建长度
            int len = 0;
            //循环读取数据
            while ((len = fis.read(datas)) != -1) {
                fos.write(datas, 0, len);
            }
            //释放资源
            fis.close();
            //释放资源
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
