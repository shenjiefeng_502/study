package com.study.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @program: project-monitor
 * @ClassName ResultVo
 * @description:
 * @author: sjf
 * @create: 2021-04-21 13:13
 * @Version 1.0
 **/
@Data
public class ResultVo {
    private String jsonrpc;
    private String uiVersion;
    private JSONObject result;
}
