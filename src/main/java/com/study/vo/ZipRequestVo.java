package com.study.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: project-monitor
 * @ClassName ZipRequestVo
 * @description:
 * @author: sjf
 * @create: 2021-04-21 13:29
 * @Version 1.0
 **/
@Data
public class ZipRequestVo {
    private String bucketName;
    private List<String> objects;
}
