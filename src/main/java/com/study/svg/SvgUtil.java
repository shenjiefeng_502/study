package com.study.svg;

import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SvgUtil {
    public static void main(String[] args) throws IOException, TranscoderException {
        String file = "/Users/shenjiefeng/IdeaProjects/shuxi-cb/shuxi-ent/src/main/resources/method-draw-image.svg";
        HashMap<String, String> map = new HashMap<>();
        map.put("svg_9", "test1");
        map.put("svg_10", "test2");
        map.put("svg_11", "test3");
        map.put("svg_12", "test4");
        convertToPngByFile(file, "/Users/shenjiefeng/IdeaProjects/untitled/src/main/resources/method-draw.png", map);
    }

    /**
     * batik通过读取svg文件的方式转png
     *
     * @param filePath    传入读取的svg文件
     * @param pngFilePath 转换后的png图片
     * @param map         更改svg属性的集合 传值规则，id,内容 主要是更改svg子节点的内容。
     * @throws IOException
     * @throws TranscoderException
     */
    public static void convertToPngByFile(String filePath, String pngFilePath, Map<String, String> map)
            throws IOException, TranscoderException {
        File file = new File(pngFilePath);
        FileOutputStream outputStream = null;
        try {
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            convertToPngByFile(filePath, outputStream, map);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void convertToPngByFile(String path, OutputStream outputStream, Map<String, String> map)
            throws TranscoderException, IOException {
        try {
            File file = new File(path);
            String parser = XMLResourceDescriptor.getXMLParserClassName();
            SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parser);
            Document doc = f.createDocument(file.toURI().toString());
            Element element = doc.getDocumentElement();
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> next = iterator.next();
                Element e = doc.getElementById(next.getKey());
                Node firstChild = e.getFirstChild();
                firstChild.setNodeValue(next.getValue());
            }
            PNGTranscoder t = new PNGTranscoder();
            TranscoderInput input = new TranscoderInput(doc);
            TranscoderOutput output = new TranscoderOutput(outputStream);
            t.transcode(input, output);
            outputStream.flush();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
