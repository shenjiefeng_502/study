package com.study.redisson.rabbit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.study.redisson.AvailabilityType;
import com.study.redisson.RedisConstant;
import com.study.redisson.RedisMessageUnrepeatable;
import com.study.redisson.RedisPublisher;
import com.study.redisson.rabbit.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class RedissonMessageSubscriber {
    @Autowired
    private RedisPublisher redisPublisher;

    @Autowired
    private RedissonClient redissonClient;
    public void send(){
        ChangeAvailabilityVO vo = new ChangeAvailabilityVO();
        ArrayList<String> arrayList = new ArrayList<>();
        vo.setType(AvailabilityType.OPERATIVE.value());
        vo.setChargeBoxList(arrayList);
        redisPublisher.publishMessage(RedisConstant.CHANGE_AVAILABILITY, JSON.toJSONString(vo));
    }

    /**
     * 心跳消息
     */
    @PostConstruct
    public void ChargeBoxHeartbeat() {
        RedisMessageUnrepeatable redisMq = new RedisMessageUnrepeatable(redissonClient, "ChargeBoxHeartbeat");
        redisMq.addListener(String.class, new MessageListener<String>() {
                    @Override
                    public void onMessage(CharSequence channel, String message) {
                        log.info(message);
                        ChargeBoxHeartbeatVO chargeBoxHeartbeatVO = JSON.parseObject(message, ChargeBoxHeartbeatVO.class ,
                                Feature.SupportNonPublicField,
                                Feature.DisableCircularReferenceDetect);
                    }
        });
    }

    /**
     * 状态变更消息
     */
    @PostConstruct
    public void statusChange() {
        RedisMessageUnrepeatable redisMq = new RedisMessageUnrepeatable(redissonClient, "statusChange");
        redisMq.addListener(String.class, new MessageListener<String>() {
            @Override
            public void onMessage(CharSequence channel, String message) {
                log.info(message);
                StatusChangeVO statusChangeVO = JSON.parseObject(message, StatusChangeVO.class);
            }
        });
    }

    /**
     * 启动充电消息
     */
    @PostConstruct
    public void startCharge() {
        RedisMessageUnrepeatable redisMq = new RedisMessageUnrepeatable(redissonClient, "startCharge");
        redisMq.addListener(String.class, new MessageListener<String>() {
            @Override
            public void onMessage(CharSequence channel, String message) {
                // 处理接收到的消息
                log.info(message);
                StartChargeVO startChargeVO = JSON.parseObject(message, StartChargeVO.class);
            }
        });
    }

    /**
     * 停止充电消息
     */
    @PostConstruct
    public void stopCharge() {
        RedisMessageUnrepeatable redisMq = new RedisMessageUnrepeatable(redissonClient, "stopCharge");
        redisMq.addListener(String.class, new MessageListener<String>() {
            @Override
            public void onMessage(CharSequence channel, String message) {
                // 处理接收到的消息
                log.info(message);
                StopChargeVO stopChargeVO = JSON.parseObject(message, StopChargeVO.class);
            }
        });
    }

    /**
     * 充电数据消息
     */
    @PostConstruct
    public void insertMeterValues() {
        RedisMessageUnrepeatable redisMq = new RedisMessageUnrepeatable(redissonClient, "insertMeterValues");
        redisMq.addListener(String.class, new MessageListener<String>() {
            @Override
            public void onMessage(CharSequence channel, String message) {
                // 处理接收到的消息
                log.info(message);
                List<MeterValueVO> meterValueVO = JSON.parseArray(message, MeterValueVO.class);
            }
        });
    }
}
