package com.study.redisson.rabbit.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ChangeAvailabilityVO implements Serializable {
    private static final long serialVersionUID = -8244105134858362284L;
    private String type;
    private List<String> chargeBoxList;
}
