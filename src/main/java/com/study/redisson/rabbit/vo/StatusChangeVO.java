package com.study.redisson.rabbit.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class StatusChangeVO implements Serializable {
    private static final long serialVersionUID = 4159993917703039334L;

    private String chargeBoxId;
    private int connectorId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String timestamp;
    private String status;
    private String errorCode;

    // Only in OCPP1.5
    private String errorInfo, vendorId, vendorErrorCode;
}
