package com.study.redisson.rabbit.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class ChargeBoxHeartbeatVO implements Serializable {

    private static final long serialVersionUID = -4984281927680926911L;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String heartbeatTimestamp;
    private String chargeBoxId;
}
