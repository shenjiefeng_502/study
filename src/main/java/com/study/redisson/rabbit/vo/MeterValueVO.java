package com.study.redisson.rabbit.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MeterValueVO implements Serializable {

    private static final long serialVersionUID = 6101773003453462232L;
    private Integer connectorPk;

    private Integer transactionPk;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date valueTimestamp;

    private String value;

    /**
     * 读取内容
     */
    private String readingContext;

    private String format;

    /**
     * 被测变量
     */
    private String measurand;

    private String location;

    private String unit;

    /**
     * 阶段
     */
    private String phase;
}
