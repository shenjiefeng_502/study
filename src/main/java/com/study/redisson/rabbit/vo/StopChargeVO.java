package com.study.redisson.rabbit.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class StopChargeVO implements Serializable {
    private static final long serialVersionUID = 2808921532607893320L;
    private String chargeBoxId;
    private int transactionId;
    private String stopTimestamp;
    private String stopMeterValue;
    private String stopReason;

    private String eventTimestamp;
}
