package com.study.redisson.rabbit.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class StartChargeVO implements Serializable {
    private static final long serialVersionUID = -8823360030385249328L;
    private String chargeBoxId;
    private int connectorId;
    private String idTag;
    private String startTimestamp;
    private String startMeterValue;

    // Only in OCPP1.5
    private Integer reservationId;
    private Integer transactionId;

    // this came after splitting transaction table into two tables (start and stop)
    private String eventTimestamp;

    public boolean isSetReservationId() {
        return reservationId != null;
    }
}
