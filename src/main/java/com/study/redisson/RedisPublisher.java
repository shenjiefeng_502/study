package com.study.redisson;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RedisPublisher {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void publishMessage(String topic, String message) {
        log.info("推送订阅消息;topic:{},message:{}", topic, message);
        stringRedisTemplate.convertAndSend(topic, message);
    }
}