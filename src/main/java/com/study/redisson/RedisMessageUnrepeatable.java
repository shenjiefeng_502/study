package com.study.redisson;

import io.micrometer.core.lang.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 1.在 redis 中为每一个消息队列维护一个 topicIndex, 表示当前消费的消息序列。
 * 2.在每一个消费者中维护一个 currentIndex, 表示当前消费者已消费到的消息序列, 创建时与 topicIndex 同步。
 * 3.redis 会为每一个消费者都推送每一条消息, 当消费者拿到当前消息时, 会将本地的 currentIndex 和 currentIndex 加一后的结果与 redis 中的 topicIndex 进行 CAS。
 * 3.1如果成功的将替换 topicIndex 的值则表示能够消费当前数据，随后执行具体消费逻辑。
 * 3.2如果未能替换 topicIndex , 则表示消息序列为 currentIndex 的消息已被其它的消费者消费, 则跳过当前消息处理下一条消息。
 * 3.3无论是否消费到消息, currentIndex 都将加一。
 */
@Slf4j
public class RedisMessageUnrepeatable {
    private static final String INDEX_SUFFIX = "_unrepeatable";

    private static final String RW_SUFFIX = "_rwLock";

    private final RedissonClient client;

    // 主题名
    private final String topic;


    private final RTopic rTopic;

    private final RAtomicLong topicIndex;

    private final RReadWriteLock rwLock;

    public RedisMessageUnrepeatable(@NonNull RedissonClient client, @NonNull String topic) {
        this.client = client;
        this.topic = topic;
        this.rTopic = client.getTopic(topic);
        this.topicIndex = client.getAtomicLong(topic + INDEX_SUFFIX);
        rwLock = client.getReadWriteLock(topic + RW_SUFFIX);
    }

    public <M> void addListener(Class<M> type, MessageListener<M> listener) {
        // 获取写锁, 保证在注册新的消费者的时候 topicIndex 不会被修改.
        // 若在注册的时候 topicIndex 在改变, 可能导致与 currentIndex 不一致,
        // 从而导致 currentIndex 永远比 topicIndex 小, 造成该消费者永远消费不到消息.
        rwLock.writeLock().lock();
        try {
            final AtomicLong currentIndex = new AtomicLong(topicIndex.get());
            rTopic.addListener(type, (charSequence, s) -> {
                // 获取读锁, 读锁是共享锁.
                rwLock.readLock().lock();
                try {
                    if (!topicIndex.compareAndSet(currentIndex.get(),
                            currentIndex.incrementAndGet())) {
                        log.info("消费失败");
                        return;
                    }
                    log.info("消费成功");
                    listener.onMessage(charSequence, s);
                }finally {
                    rwLock.readLock().unlock();
                }
            });
        }finally {
            rwLock.writeLock().unlock();
        }
    }

    public RedissonClient getClient() {
        return client;
    }

    public String getTopic() {
        return topic;
    }
}
