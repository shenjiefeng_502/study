package com.study;

import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * @author : sjf
 * @version 1.0
 * @Date :  2021/8/5  10:37
 * @description: test
 */
@Data
public class JUCTest {
    //线程处理数
    private static final int pageSize = 100;
    //线程数
    private static final int threadSize = 10;

    private Consumer<Object> consumer = null;

    public JUCTest(Consumer consumer) {
        this.consumer = consumer;
    }

    static void doJob() {
        AtomicInteger pageIndex = new AtomicInteger(0);
        do {

        }while (true);
        //计算分页数
        //提交线程并运行

    }
}
