package com.study.minio;

import io.minio.Digest;
import io.minio.MinioClient;
import io.minio.Time;
import io.minio.credentials.Credentials;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.http.Method;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;

/**
 * @program: ListStudyDemo
 * @ClassName HttpFileTest
 * @description:
 * @author: sjf
 * @create: 2021-04-20 15:57
 * @Version 1.0
 **/
public class HttpFileTest extends MinioClient {
    private String userAgent = DEFAULT_USER_AGENT;
    private static final String DEFAULT_USER_AGENT =
            "MinIO ("
                    + System.getProperty("os.name")
                    + "; "
                    + System.getProperty("os.arch")
                    + ") minio-java/"
                    + MinioProperties.INSTANCE.getVersion();

    protected HttpFileTest(MinioClient client) {
        super(client);
    }
    @Override
    /** Create HTTP request for given paramaters. */
    protected Request createRequest(
            HttpUrl url, Method method, Headers headers, Object body, int length, Credentials creds)
            throws InsufficientDataException, InternalException, IOException, NoSuchAlgorithmException {
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);

        if (headers != null) requestBuilder.headers(headers);
        requestBuilder.header("Host", getHostHeader(url));
        // Disable default gzip compression by okhttp library.
        requestBuilder.header("Accept-Encoding", "identity");
        requestBuilder.header("User-Agent", this.userAgent);

        String sha256Hash = null;
        String md5Hash = null;
        if (creds != null) {
            if (url.isHttps()) {
                // Fix issue #415: No need to compute sha256 if endpoint scheme is HTTPS.
                sha256Hash = "UNSIGNED-PAYLOAD";
                if (body != null) {
                    md5Hash = Digest.md5Hash(body, length);
                }
            } else {
                Object data = body;
                int len = length;
                if (data == null) {
                    data = new byte[0];
                    len = 0;
                }

                String[] hashes = Digest.sha256Md5Hashes(data, len);
                sha256Hash = hashes[0];
                md5Hash = hashes[1];
            }
        } else {
            // Fix issue #567: Compute MD5 hash only for anonymous access.
            if (body != null) {
                md5Hash = Digest.md5Hash(body, length);
            }
        }

        if (md5Hash != null) {
            requestBuilder.header("Content-MD5", md5Hash);
        }

        if (sha256Hash != null) {
            requestBuilder.header("x-amz-content-sha256", sha256Hash);
        }

        if (creds != null && creds.sessionToken() != null) {
            requestBuilder.header("X-Amz-Security-Token", creds.sessionToken());
        }

        ZonedDateTime date = ZonedDateTime.now();
        requestBuilder.header("x-amz-date", date.format(Time.AMZ_DATE_FORMAT));

        RequestBody requestBody = null;
        if (body != null) {
            String contentType = (headers != null) ? headers.get("Content-Type") : null;
            if (body instanceof RandomAccessFile) {
                requestBody = new HttpRequestBody((RandomAccessFile) body, length, contentType);
            } else if (body instanceof BufferedInputStream) {
                requestBody = new HttpRequestBody((BufferedInputStream) body, length, contentType);
            } else {
                requestBody = new HttpRequestBody((byte[]) body, length, contentType);
            }
        }

        requestBuilder.method(method.toString(), requestBody);
        return requestBuilder.build();
    }

    private String getHostHeader(HttpUrl url) {
        // ignore port when port and service matches i.e HTTP -> 80, HTTPS -> 443
        if ((url.scheme().equals("http") && url.port() == 80)
                || (url.scheme().equals("https") && url.port() == 443)) {
            return url.host();
        }
        return url.host() + ":" + url.port();
    }
}
