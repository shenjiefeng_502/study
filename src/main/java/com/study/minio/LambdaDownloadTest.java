package com.study.minio;

import com.alibaba.fastjson.JSONObject;
import com.study.vo.ResultVo;
import com.study.vo.ZipRequestVo;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

/**
 * @program: ListStudyDemo
 * @ClassName LambdaDownloadTest
 * @description:
 * @author: sjf
 * @create: 2021-04-21 15:00
 * @Version 1.0
 **/
public class LambdaDownloadTest {
//    public static void main(String[] args) {
//        String bookObj="{\n" +
//                "\t\"jsonrpc\": \"2.0\",\n" +
//                "\t\"result\": {\n" +
//                "\t\t\"objects\": [{\n" +
//                "\t\t\t\"name\": \"Spring面试专题及答案 (1).pdf\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T06:59:45.066400117Z\",\n" +
//                "\t\t\t\"size\": 1471217,\n" +
//                "\t\t\t\"contentType\": \"application/pdf\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"Spring面试专题及答案.pdf\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T06:59:42.971333327Z\",\n" +
//                "\t\t\t\"size\": 1471217,\n" +
//                "\t\t\t\"contentType\": \"application/pdf\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"[编码：隐匿在计算机软硬件背后的语言].(Code).Charles.Petzold.中文文字版(jb51.net).pdf\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T07:07:09.791559029Z\",\n" +
//                "\t\t\t\"size\": 7499905,\n" +
//                "\t\t\t\"contentType\": \"application/pdf\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"owen-spencer-ZLGYg0OgbxQ-unsplash.jpg\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T07:20:13.993510678Z\",\n" +
//                "\t\t\t\"size\": 1131789,\n" +
//                "\t\t\t\"contentType\": \"image/jpeg\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"vlado-paunovic-iBG594vhR1k-unsplash.jpg\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T07:20:28.63597661Z\",\n" +
//                "\t\t\t\"size\": 845891,\n" +
//                "\t\t\t\"contentType\": \"image/jpeg\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"大公司最爱问的97道面试题_wrapper.pdf\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T06:59:32.939013499Z\",\n" +
//                "\t\t\t\"size\": 2139806,\n" +
//                "\t\t\t\"contentType\": \"application/pdf\"\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"name\": \"算法刷题LeetCode中文版.pdf\",\n" +
//                "\t\t\t\"lastModified\": \"2021-04-20T06:59:10.435296085Z\",\n" +
//                "\t\t\t\"size\": 1067911,\n" +
//                "\t\t\t\"contentType\": \"application/pdf\"\n" +
//                "\t\t}],\n" +
//                "\t\t\"writable\": true,\n" +
//                "\t\t\"uiVersion\": \"2021-04-18T19:26:29Z\"\n" +
//                "\t},\n" +
//                "\t\"id\": 1\n" +
//                "}";
//        ArrayList<String> bookS = new ArrayList<>();
//        JSONObject jsonObject = JSONObject.parseObject(bookObj);
//        JSONObject result = (JSONObject) jsonObject.get("result");
//        JSONArray jsonArray = JSONObject.parseArray(result.get("objects").toString());
//        Iterator<Object> iterator = jsonArray.iterator();
//        while (iterator.hasNext()){
//            Object next = iterator.next();
//            String s = next.toString();
//            JSONObject jsonObject1 = JSONObject.parseObject(s);
//            Object name = jsonObject1.get("name");
//            bookS.add(name.toString());
//        }
//        System.out.println(JSON.toJSONString(bookS));
//    }
public static void main(String[] args) throws UnsupportedEncodingException {
    String json = DoPostTest.doPost("http://182.254.220.54:9000/minio/webrpc", "{\"id\":1,\"jsonrpc\":\"2.0\",\"params\":{},\"method\":\"web.CreateURLToken\"}");
    System.out.println(json);
    ResultVo resultVo = JSONObject.parseObject(json, ResultVo.class);
    String token = (String) resultVo.getResult().get("token");
    ZipRequestVo zipRequestVo = new ZipRequestVo();
    zipRequestVo.setBucketName("my-bucketname");
    List<String> objs = Arrays.asList("Spring面试专题及答案 (1).pdf", "Spring面试专题及答案.pdf", "大公司最爱问的97道面试题_wrapper.pdf", "\n" +
            "算法刷题LeetCode中文版.pdf");
    zipRequestVo.setObjects(objs);
    String body = JSONObject.toJSONString(zipRequestVo);
//        String body = "{\"bucketName\":\"my-bucketname\",\"prefix\":\"\",\"objects\":[\"vlado-paunovic-iBG594vhR1k-unsplash.jpg\",\"owen-spencer-ZLGYg0OgbxQ-unsplash.jpg\"]}";
    String post = DoPostTest.doFilePost("http://182.254.220.54:9000/minio/zip?token=" + token, body);
//        System.out.println(post);
}
}
