package com.study.minio;

import io.minio.*;
import io.minio.errors.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: ListStudyDemo
 * @ClassName MinIOTest
 * @description:
 * @author: sjf
 * @create: 2021-04-19 16:00
 * @Version 1.0
 **/
public class MinIOTest {

    public static void main(String[] args) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint("http://182.254.220.54:9000")
                        .credentials("AKIAIOSFODNN7EXAMPLE", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY")
                        .build();
        GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder().bucket("my-bucketname").object("vlado-paunovic-iBG594vhR1k-unsplash.jpg").build());
    }

    public static String createBucket(MinioClient minioClient) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException,
            NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        minioClient.makeBucket(
                MakeBucketArgs.builder()
                        .bucket("my-bucketname")
                        .build());

        return null;
    }

    public static String compose(MinioClient minioClient) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        List<ComposeSource> sourceObjectList = new ArrayList<ComposeSource>();
        sourceObjectList.add(
                ComposeSource.builder().bucket("my-bucketname").object("[编码：隐匿在计算机软硬件背后的语言].(Code).Charles.Petzold.中文文字版(jb51.net).pdf").build());
        sourceObjectList.add(
                ComposeSource.builder().bucket("my-bucketname").object("Spring面试专题及答案.pdf").build());
        sourceObjectList.add(
                ComposeSource.builder().bucket("my-bucketname").object("算法刷题LeetCode中文版.pdf").build());
        sourceObjectList.add(
                ComposeSource.builder().bucket("my-bucketname").object("大公司最爱问的97道面试题_wrapper.pdf").build());

// Create my-bucketname/my-objectname by combining source object list.
        minioClient.composeObject(
                ComposeObjectArgs.builder()
                        .bucket("my-bucketname")
                        .object("my-objectname")
                        .sources(sourceObjectList)
                        .build());

//// Create my-bucketname/my-objectname with user metadata by combining source object
//// list.
//        Map<String, String> userMetadata = new HashMap<>();
//        userMetadata.put("My-Project", "Project One");
//        minioClient.composeObject(
//                ComposeObjectArgs.builder()
//                        .bucket("my-bucketname")
//                        .object("my-objectname")
//                        .sources(sourceObjectList)
//                        .userMetadata(userMetadata)
//                        .build());
        return null;
    }
}
