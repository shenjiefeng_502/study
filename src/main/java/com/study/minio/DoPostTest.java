package com.study.minio;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.entity.ContentType;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * @program: ListStudyDemo
 * @ClassName DoPostTest
 * @description:
 * @author: sjf
 * @create: 2021-04-20 16:21
 * @Version 1.0
 **/
public class DoPostTest {

    public static String doGet(String url) {
        // 输入流
        InputStream is = null;
        BufferedReader br = null;
        String result = null;
        // 创建httpClient实例
        HttpClient httpClient = new HttpClient();
        // 设置http连接主机服务超时时间：15000毫秒
        // 先获取连接管理器对象，再获取参数对象,再进行参数的赋值
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(15000);
        // 创建一个Get方法实例对象
        GetMethod getMethod = new GetMethod(url);
        // 设置get请求超时为60000毫秒
        getMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 60000);
        // 设置请求重试机制，默认重试次数：3次，参数设置为true，重试机制可用，false相反
        getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, true));
        try {
            // 执行Get方法
            int statusCode = httpClient.executeMethod(getMethod);
            // 判断返回码
            if (statusCode != HttpStatus.SC_OK) {
                // 如果状态码返回的不是ok,说明失败了,打印错误信息
                System.err.println("Method faild: " + getMethod.getStatusLine());
            } else {
                // 通过getMethod实例，获取远程的一个输入流
                is = getMethod.getResponseBodyAsStream();
                // 包装输入流
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                StringBuffer sbf = new StringBuffer();
                // 读取封装的输入流
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    sbf.append(temp).append("\r\n");
                }

                result = sbf.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 释放连接
            getMethod.releaseConnection();
        }
        return result;
    }


    public static String doPost(String url, String body) throws UnsupportedEncodingException {
        // 获取输入流
        InputStream is = null;
        BufferedReader br = null;
        String result = null;
        // 创建httpClient实例对象
        HttpClient httpClient = new HttpClient();
        // 设置httpClient连接主机服务器超时时间：15000毫秒
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(15000);
        // 创建post请求方法实例对象
        PostMethod postMethod = new PostMethod(url);
        // 设置post请求超时时间
        postMethod.setRequestHeader("Authorization", " Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NLZXkiOiJBS0lBSU9TRk9ETk43RVhBTVBMRSIsImV4cCI6MTYxOTA4MjQ5Niwic3ViIjoiQUtJQUlPU0ZPRE5ON0VYQU1QTEUifQ.RuF734jq66RDhmW0RN50_HVP7LfvIJdYHn-IKkSrGT_AGH9P5VrOSyVTZKHvBxj4NqWefQFbZ-o1T5DphLZxAA");
        postMethod.setRequestHeader("Content-Type", "application/json");
        postMethod.setRequestHeader("User-Agent", " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36");
        StringRequestEntity stringRequestEntity = new StringRequestEntity(body, ContentType.APPLICATION_JSON.getMimeType(),"utf-8");
        postMethod.setRequestEntity(stringRequestEntity);
        // 执行POST方法
        try {
            int statusCode = httpClient.executeMethod(postMethod);
            // 判断是否成功
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method faild: " + postMethod.getStatusLine());
            }
            // 获取远程返回的数据
            is = postMethod.getResponseBodyAsStream();
            // 封装输入流
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            StringBuffer sbf = new StringBuffer();
            String temp = null;
            while ((temp = br.readLine()) != null) {
                sbf.append(temp).append("\r\n");
            }

            result = sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 释放连接
            postMethod.releaseConnection();
        }
        return result;
    }
    public static String doFilePost(String url, String body) throws UnsupportedEncodingException {
        // 获取输入流
        InputStream is = null;
        BufferedReader br = null;
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        FileChannel fileChannelInput = null;
        FileChannel fileChannelOutput = null;
        String result = null;
        // 创建httpClient实例对象
        HttpClient httpClient = new HttpClient();
        // 设置httpClient连接主机服务器超时时间：15000毫秒
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(15000);
        // 创建post请求方法实例对象
        PostMethod postMethod = new PostMethod(url);
        // 设置post请求超时时间
        postMethod.setRequestHeader("Authorization", " Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NLZXkiOiJBS0lBSU9TRk9ETk43RVhBTVBMRSIsImV4cCI6MTYxOTA3NzQwMywic3ViIjoiQUtJQUlPU0ZPRE5ON0VYQU1QTEUifQ.ST8BX4CdvfG7GhZ0O0iRtal5T2cJioiHB4oJO50HhscxWPqpVpQmumQXGsbFwy37FQhhrf1NLJ-w_MQ5OB7T7Q");
        postMethod.setRequestHeader("Content-Type", "application/json");
        postMethod.setRequestHeader("User-Agent", " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36");
        StringRequestEntity stringRequestEntity = new StringRequestEntity(body, ContentType.APPLICATION_JSON.getMimeType(), "utf-8");
        postMethod.setRequestEntity(stringRequestEntity);
        // 执行POST方法
        try {
            long be = System.currentTimeMillis();
            int statusCode = httpClient.executeMethod(postMethod);
            long currentTimeMillis = System.currentTimeMillis();
            System.out.println("请求执行时间： "+(currentTimeMillis-be)+"mm");
            // 判断是否成功
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method faild: " + postMethod.getStatusLine());
            }
            // 获取远程返回的数据
            is = postMethod.getResponseBodyAsStream();

            // 封装输入流
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            StringBuffer sbf = new StringBuffer();
            String temp = null;
            long begin = System.currentTimeMillis();
            while ((temp = br.readLine()) != null) {
                sbf.append(temp).append("\r\n");
            }
            long end = System.currentTimeMillis();
            System.out.println(" ");
            System.out.println(" ");
            System.out.println(" ");
            System.out.println("执行时间: " + (end - begin) + "mm");
            System.out.println(" ");
            System.out.println(" ");
            System.out.println(" ");
            result = sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 释放连接
            postMethod.releaseConnection();
        }
        return result;
    }
}
