package com.study.minio;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;

/**
 * @program: ListStudyDemo
 * @ClassName HttpRequestBody
 * @description:
 * @author: sjf
 * @create: 2021-04-20 16:06
 * @Version 1.0
 **/
public class HttpRequestBody extends RequestBody {
    private RandomAccessFile file = null;
    private BufferedInputStream stream = null;
    private byte[] bytes = null;
    private int length = -1;
    private String contentType = null;

    HttpRequestBody(final RandomAccessFile file, final int length, final String contentType) {
        this.file = file;
        this.length = length;
        this.contentType = contentType;
    }

    HttpRequestBody(final BufferedInputStream stream, final int length, final String contentType) {
        this.stream = stream;
        this.length = length;
        this.contentType = contentType;
    }

    HttpRequestBody(final byte[] bytes, final int length, final String contentType) {
        this.bytes = bytes;
        this.length = length;
        this.contentType = contentType;
    }

    @Override
    public MediaType contentType() {
        MediaType mediaType = null;

        if (contentType != null) {
            mediaType = MediaType.parse(contentType);
        }
        if (mediaType == null) {
            mediaType = MediaType.parse("application/octet-stream");
        }

        return mediaType;
    }

    @Override
    public long contentLength() {
        return length;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        if (file != null) {
            sink.write(Okio.source(Channels.newInputStream(file.getChannel())), length);
        } else if (stream != null) {
            sink.write(Okio.source(stream), length);
        } else {
            sink.write(bytes, 0, length);
        }
    }
}
