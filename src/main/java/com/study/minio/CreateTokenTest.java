package com.study.minio;

import com.alibaba.fastjson.JSONObject;

/**
 * @program: ListStudyDemo
 * @ClassName CreateTokenTest
 * @description:
 * @author: sjf
 * @create: 2021-04-20 16:28
 * @Version 1.0
 **/
public class CreateTokenTest {

//        public static void main(String[] args) throws UnsupportedEncodingException {
//        //{"id":1,"jsonrpc":"2.0","params":{},"method":"web.CreateURLToken"}
//        String post = DoPostTest.doPost("http://182.254.220.54:9000/minio/webrpc", "{\"id\":1,\"jsonrpc\":\"2.0\",\"params\":{},\"method\":\"web.CreateURLToken\"}");
//        System.out.println(post);
//    }
    public static void main(String[] args) {
        String json="{\"jsonrpc\":\"2.0\",\"result\":{\"token\":\"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9" +
                ".eyJhY2Nlc3NLZXkiOiJBS0lBSU9TRk9ETk43RVhBTVBMRSIsImV4cCI6MTYxODk3NTQxOCwic3ViIjoiQUtJQUlPU0ZPRE5ON0VYQU1QTEUifQ.KOAv_bTpRNWqkUh2w0ViZdxx93Vh46kO5bOyYLqBExLl8AXZZ38CtGj1tNbLqgsPN56HyyqQYN8nUlm7Rq9WLA\",\"uiVersion\":\"2021-04-18T19:26:29Z\"},\"id\":1}";
        JSONObject returnStr = JSONObject.parseObject(json);
        Object result = returnStr.get("result");
        JSONObject object = JSONObject.parseObject(result.toString());
        Object token = object.get("token");
    }
}
