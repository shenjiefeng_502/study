package com.study.minio;

import java.io.UnsupportedEncodingException;

/**
 * @program: ListStudyDemo
 * @ClassName LoginMinIOTest
 * @description:
 * @author: sjf
 * @create: 2021-04-21 11:12
 * @Version 1.0
 **/
public class LoginMinIOTest {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String body="{\"id\":1,\"jsonrpc\":\"2.0\",\"params\":{\"username\":\"AKIAIOSFODNN7EXAMPLE\",\"password\":\"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"},\"method\":\"web.Login\"}";
        String url = "http://182.254.220.54:9000/minio/webrpc";
        String post = DoPostTest.doPost(url, body);
        System.out.println(post);
    }
}
