package com.study.minio;

import java.io.UnsupportedEncodingException;

/**
 * @program: ListStudyDemo
 * @ClassName FileZipTes
 * @description:
 * @author: sjf
 * @create: 2021-04-20 18:13
 * @Version 1.0
 **/
public class FileZipTest {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //{"id":1,"jsonrpc":"2.0","params":{},"method":"web.CreateURLToken"}
        String body = "{\"bucketName\":\"my-bucketname\",\"prefix\":\"\",\"objects\":[\"vlado-paunovic-iBG594vhR1k-unsplash.jpg\",\"owen-spencer-ZLGYg0OgbxQ-unsplash.jpg\"]}";
        String token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NLZXkiOiJBS0lBSU9TRk9ETk43RVhBTVBMRSIsImV4cCI6MTYxODk3MDAzMiwic3ViIjoiQUtJQUlPU0ZPRE5ON0VYQU1QTEUifQ.KWZ5I19NJDZmRTU7vbdHzMRPc_uRUK-uJj4FWWFB-n7bhlvnSCt60eO_xgyUAmwQwIL-xTHfIati8q7P2zrt5w";
        String post = DoPostTest.doPost("http://182.254.220.54:9000/minio/zip?token=" + token, body);
        System.out.println(post);
    }
}
